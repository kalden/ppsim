package ppatch_webInterface;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;


/**
 * Applet for display on the website, to launch the form for changing parameters prior to a
 * simulation run
 * 
 * @author kieran
 *
 */
public class LaunchXMLMod extends Applet
{
	/**
	 * window for the applet
	 */
	JFrame f = new JFrame();
    
	public void init() 
    {
		JButton button = new JButton("Adjust Parameters prior to run");
		setLayout(new BorderLayout());
	    add(button, BorderLayout.CENTER);
	
	    // add button and link to ModifyXML when this is pressed
		button.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent evt)
            {
				try
                {
					ModifyXML xl = new ModifyXML();
					xl.init();
                }
				catch(Exception e)
				{
					e.printStackTrace();
				}
            }
        });
    }
}
