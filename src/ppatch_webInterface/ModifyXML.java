package ppatch_webInterface;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import ppatch_onepatch.PPatchSimUI;
import ppatch_onepatch.SimParameters;

import sim.display.Console;
import sim.display.GUIState;
 
public class ModifyXML extends Applet implements ActionListener
{
	/**
	 * Window for the form to be created within
	 */
	JFrame f = new JFrame();
	
	/**
	 * Arraylist containing all the textboxes, and therefore all the system variables, on the form. 
	 * Processed later when the simulation begins
	 */
	public static ArrayList<JTextField> boxes = new ArrayList<JTextField>();
	
	/**
	 * ArrayList containing the knockout experiments chosen on the console
	 */
	public static ArrayList<JRadioButton> radios = new ArrayList<JRadioButton>();
	
	/**
	 * Arraylist of tabbed panels - one for each parameter group
	 */
	public static ArrayList<JPanel> panels = new ArrayList<JPanel>();
	
	
	/**
	 * Once the form is completed, a simParameters object is needed to process the chosen parameters
	 * and start the simulator
	 */
	public static SimParameters simParams;
	
	/**
	 * Creates the form to set the simulation variables before the simulation starts
	 */
	public void createForm()
	{	
		// tabbed form - the parameters are split into the same groups as in the XML file
		JTabbedPane tabbedPane;
		
		f.setTitle("Simulation Parameters");
	    f.setSize(700,400);
	    
		JPanel topPanel = new JPanel();
		topPanel.setLayout( new BorderLayout() );
		f.getContentPane().add( topPanel );
		
		// SET THE INFORMATION FOR THE WINDOW
		topPanel.add(createInfoPanel(),BorderLayout.NORTH);
		
		// Create a tabbed pane
		tabbedPane = new JTabbedPane();
		
		try
		{
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc;
			
			// Get the parameters from the web file - used to show defaults
			URL url = new URL("http://www-users.cs.york.ac.uk/~kalden/frontiers/parameters-metadata_MASTER.xml");
			InputStream stream = url.openStream();
			doc = docBuilder.parse(stream);
			doc.getDocumentElement ().normalize ();
			
			// FIRSTLY WORK OUT THE NUMBER OF SUBPANELS NEEDED ON THE DISPLAY (1 FOR EACH PARAMETER GROUP)
			/**
			 * Some of the maths looks weird from this point onwards.  When the XML is read in, it is read in
			 * as #text, <NodeName>, #text, <NodeName> etc.  Therefore, we need to skip the #text parts, which
			 * is why the for loop starts at 1 and increases by 2.  This is also why a separate counter is needed,
			 * panelArrayCounter, as the panels stored in the array need to increase by 1
			 */
			
			NodeList n2 = doc.getDocumentElement().getChildNodes();   // ALL CHILD NODES OF XML
			int numberOfGroups = n2.getLength();
			
			int panelArrayCounter=0;
			for(int i=1;i<numberOfGroups-2;i=i+2)
			{
				// CREATE A NEW PANEL IN THE ARRAY
				panels.add(new JPanel(new GridLayout(0,2)));
				
				// NOW GO THROUGH THE GROUP
				// GIVE THE TAB THE SAME NAME AS THE PARAMETER GROUP
				tabbedPane.addTab(n2.item(i).getNodeName().toString(),panels.get(panelArrayCounter));
				
				// NOW COMPLETE THIS TAB WITH THE PARAMETERS
				NodeList n3 = n2.item(i).getChildNodes();                  // GROUP CHILD NODES
				int numberOfParams = n3.getLength();
				
				if(i!=numberOfGroups-4)			// THE GROUP IS NOT THE KNOCKOUTS (NOTE THE 4 DUE TO #TEXTs)
				{
					for(int u=1;u<numberOfParams;u=u+2)
					{
						addValueParameter(n3.item(u),u,panelArrayCounter);
					}
				}
				else
				{
					for(int u=1;u<numberOfParams;u=u+2)
					{
						// USE RADIO BUTTONS INSTEAD OF TEXT BOXES
						addRadioParameter(n3.item(u),u,panelArrayCounter);
					}
				}
				
				panelArrayCounter++;
			
			}
			
			topPanel.add( tabbedPane, BorderLayout.CENTER );
			
			// Add the buttons & listeners			
		    topPanel.add(createControlPanel(), BorderLayout.SOUTH );
		    
		    f.setVisible(true);
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
			
		}
	}
	
	public void addValueParameter(Node param_group,int paramNumber,int visualTabNumber)
	{
		NodeList paramDetails = param_group.getChildNodes();                  // PARAMETER CHILD NODES
		
		// label for the parameter
		JLabel label = new JLabel();
		label.setText(param_group.getNodeName().toString());
		// set tooltip text for this parameter
		label.setToolTipText(paramDetails.item(9).getTextContent());
		panels.get(visualTabNumber).add(label);
		
		// textbox for the parameter - with default value
		JTextField textbox = new JTextField(15);
		textbox.setName(label.getName());
		textbox.setText(paramDetails.item(1).getTextContent());
		// set tooltip text for this box
		textbox.setToolTipText(paramDetails.item(9).getTextContent());
		panels.get(visualTabNumber).add(textbox);
	
		// Add textbox to the Array so the result can be accessed later
		boxes.add(textbox);
		
		// NOW, ATTACH A LISTENER TO THE LABEL
		// WHEN THE LABEL IS PRESSED, A BROWSER PAGE WILL SHOW THE USER HOW THE PARAMETER FITS IN IN THE DOMAIN MODEL DIAGRAM
		// NOT CURRENTLY USED AS NOT YET COMPLETE
		this.linkToDomainInfo(label);
	}
	
	public void addRadioParameter(Node param_group,int paramNumber,int visualTabNumber)
	{
		NodeList paramDetails = param_group.getChildNodes();                  // PARAMETER CHILD NODES
		
		// label for the parameter
		JLabel label = new JLabel();
		label.setText(param_group.getNodeName().toString());
		// set tooltip text for this parameter
		label.setToolTipText(paramDetails.item(9).getTextContent());
		panels.get(visualTabNumber).add(label);
		
		// radio button to turn to true or false
		// check the default in the parameter file - this may be set to be off initially & needs checking
		JRadioButton jradiobuttonTrue,jradiobuttonFalse;
		if(Boolean.parseBoolean(paramDetails.item(1).getTextContent()))
		{
			// default is knockout is true
			jradiobuttonTrue = new JRadioButton("on",true);
			jradiobuttonFalse = new JRadioButton("off");
		}
		else
		{
				// default is knockout is false
			jradiobuttonTrue = new JRadioButton("on");
			jradiobuttonFalse = new JRadioButton("off",true);
		}
		
		// add to a button group
		ButtonGroup bgroup = new ButtonGroup();
		bgroup.add(jradiobuttonTrue);
		bgroup.add(jradiobuttonFalse);
			
		// add the true radio button to the array so these can be read when the parameters are stored
		radios.add(jradiobuttonTrue);

		// create a panel for the radio buttons to go in the main tab panel
		JPanel radioPanel = new JPanel();
		radioPanel.setLayout(new GridLayout(1,2));
		radioPanel.add(jradiobuttonTrue);
		radioPanel.add(jradiobuttonFalse);
		
		panels.get(visualTabNumber).add(radioPanel);
		
		// set the tooltop for the radio button
		label.setToolTipText(paramDetails.item(9).getTextContent());
		
		// NOW, ATTACH A LISTENER TO THE LABEL
		// WHEN THE LABEL IS PRESSED, A BROWSER PAGE WILL SHOW THE USER HOW THE PARAMETER FITS IN IN THE DOMAIN MODEL DIAGRAM
		// NOT CURRENTLY USED AS NOT YET COMPLETE
		this.linkToDomainInfo(label);
		
	}
	
	public JPanel createInfoPanel()
	{
		JLabel blank = new JLabel("");
		JLabel blank2 = new JLabel("");
		JLabel help = new JLabel("Move the mouse over the parameter name to see a description",SwingConstants.CENTER);
		JLabel help2 = new JLabel("Or click on the label to open a web page showing more information on that parameter",SwingConstants.CENTER);
		
		JPanel infoPanel = new JPanel(new GridLayout(4,1));
		infoPanel.add(blank);
		infoPanel.add(help);
		infoPanel.add(help2);
		infoPanel.add(blank2);
		
		return infoPanel;
	}
	
	public JPanel createControlPanel()
	{
		// buttons
		JButton SUBMIT=new JButton("Start Simulation");
		JButton reset = new JButton("Reset to Defaults & Run");
		
		JPanel controlPanel = new JPanel();
		controlPanel.add(SUBMIT);
	    SUBMIT.addActionListener(this);
	    
	    controlPanel.add(reset);
	    reset.addActionListener(this);
	    
	    return controlPanel;
		
	}
	
	/**
	 * Creates a mouse listener on each label, which if clicked opens a browser tab with information about that parameter
	 * 
	 * @param paramLabel	The label to add the listener to
	 */
	public void linkToDomainInfo(final JLabel paramLabel)
	{
		paramLabel.addMouseListener(new MouseAdapter()
		{
			public void mousePressed(MouseEvent me)
			{
				// Now, when a mouse is pressed, open a web browser tab displaying the correct information about this parameter
				try
				{ 
					String link = "http://localhost/frontiersNew/doc/ppatch_Initial/SimParameters.html#"+paramLabel.getText();
					//String link = "file:///home/kieran/MASON/doc/sim/app/ppatch/SimParameters.html#"+ paramLabel.getText();
					//String link = "http://www-users.cs.york.ac.uk/~kalden/frontiersNew/doc/ppatch_Initial/SimParameters.html#"+paramLabel.getText();
					URI uri = new URI(link);
					Desktop desktop = null;
					
					if(Desktop.isDesktopSupported())
					{
						desktop = Desktop.getDesktop();
					}
					
					if(desktop !=null)
						desktop.browse(uri);
				}
				
				catch(Exception ex)
				{
					ex.printStackTrace();
				}	
			}
		});
	}
	
	/**
	 * Determines what happens when the start simulation, or reset parameters button is pressed
	 */
	 public void actionPerformed(ActionEvent ae)
	 {
		 String arg = ae.getActionCommand();
		 
		 if(arg.equals("Start Simulation"))			// with the assumption that the parameters have been changed
		 {
			 //startSimulation();
			 simParams.adjustParameters(this.boxes,this.radios);
			 PPatchSimUI ppatchdisp = new PPatchSimUI(simParams);
		 }
		 else	// Chosen to reset to defaults & run simulation
		 {
			 // clear the boxes, so that the size is zero, and the simulation will read in the defaults from the XML
			 boxes.clear();
			 PPatchSimUI ppatchdisp = new PPatchSimUI(simParams);
		 }
	 }
	
	 /**
	  * Start the applet
	  */
	 public void init() 
     {
		 // Read in the tags & parameters to build the form
		 //simParams = new SimParameters(null,null,null);
		 
		 // create the form
		 ModifyXML setParams = new ModifyXML();
		 this.simParams = new SimParameters();
		 setParams.createForm();
     }
}
