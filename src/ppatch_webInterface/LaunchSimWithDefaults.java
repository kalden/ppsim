package ppatch_webInterface;

import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;

import ppatch_onepatch.PPatchSimUI;
import ppatch_onepatch.SimParameters;

/**
 * Applet for display on the website, to launch the form for changing parameters prior to a
 * simulation run
 * 
 * @author kieran
 *
 */
public class LaunchSimWithDefaults extends Applet
{
	/**
	 * window for the applet
	 */
	JFrame f = new JFrame();
	
	public static SimParameters simParams;
    
	public void init() 
    {
		JButton button = new JButton("Launch Simulation with Defaults");
		setLayout(new BorderLayout());
	    add(button, BorderLayout.CENTER);
	
	    // add button and link to ModifyXML when this is pressed
		button.addActionListener(new ActionListener()
        {
			public void actionPerformed(ActionEvent evt)
            {
				try
                {
					simParams = new SimParameters(null,null,null);
					 
					PPatchSimUI ppatchdisp = new PPatchSimUI(simParams);
					 	 
					//ModifyXML xl = new ModifyXML();
					//xl.init();
                }
				catch(Exception e)
				{
					e.printStackTrace();
				}
            }
        });
    }
}
