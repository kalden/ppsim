package ppatch_post_process;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ProcessBatchData 
{
	public static ArrayList<String> files = new ArrayList<String>();
	public static FileWriter newCSVWriter;
	
	public static int totalNumberOfPatches = 0;
	public static int totalNumberOfPatchesOver400 = 0;
	public static int totalNumberOfPatchesOver25LTi = 0;

	
	public void populateFiles(String runDesc, int numOfRuns)
	{
		files.clear();
		
		for(int i=1;i<=numOfRuns;i++)
		{
			files.add("/home/kieran/Documents/SimRuns/PPCompRuns/"+runDesc+"/RUN"+i+"/patchStats.csv");
		}
		
	}
	


	 public double variance(ArrayList results) 
	 {
		 long n = 0;
		 double mean = 0;
	     double s = 0.0;
	  
	     for(int i=0;i<results.size();i++)
	     {
	    	 double x = (Double)results.get(i);
	    	 n++;
	    	 double delta = x - mean;
	  
	    	 mean += delta / n;
	    	 s += delta * (x - mean);
	  
	     }
	 
	     // if you want to calculate std deviation of a sample change this to (s/(n-1))
	  
	     return (s / results.size());
	  
	 }
	  
	       
	  /**
	  * @param population an array, the population
	  * @return the standard deviation
	  */
	  
	   public double standard_deviation(ArrayList population) 
	   {
		   return Math.sqrt(variance(population));
	   }


	
	public void analyseFileSet(String runDesc)
	{
		
		try
		{
			ArrayList<Double> numPatches = new ArrayList<Double>();
			ArrayList<Double> numPatches400 = new ArrayList<Double>();
			ArrayList<Double> numPatches25  = new ArrayList<Double>();
			
			for(int i=0;i<files.size();i++)
			{
				double numberOfPatches = 0;
				double numOfPatchesOver400 = 0;
				double numOfPatchesOver25LTi = 0;
				
				//csv file containing dat
				String strFile = files.get(i);
				
				//create BufferedReader to read csv file
	
				BufferedReader br = new BufferedReader( new FileReader(strFile));
	
				String strLine = "";
	
				StringTokenizer st = null;
	
				int lineNumber = 0;
	
				//read comma separated file line by line
	
				while( (strLine = br.readLine()) != null)
				{
					if(lineNumber==0)
					{
						// print the headingsnewCSVWriter = new FileWriter("/home/kieran/Documents/SimRuns/PPCompRuns/pt25ChemKO/BatchPatchSummary.csv",true);
						st = new StringTokenizer(strLine, ",");
						//newCSVWriter.append("N,");
					
						while(st.hasMoreTokens())
						{
							st.nextToken();
						}
						lineNumber++;
					}
			
					else
					{
						//break comma separated line using ","
						numberOfPatches++;
						st = new StringTokenizer(strLine, ",");
						//newCSVWriter.append(lineNumber+",");
	
						while(st.hasMoreTokens())
						{
							// FIRST ITEM ON LINE IS THE LTO NUMBER - NOT REALLY NECESSARY
							st.nextToken();
							
							// NEXT ITEM IS THE PATCH AREA
							double patchArea = Double.parseDouble(st.nextToken());
							
							if(patchArea>=400)
							{
								numOfPatchesOver400++;
							}
							
							// NEXT IS THE NUMBER OF LTI IN THE PATCH
							int ltiNum = Integer.parseInt(st.nextToken());
							
							if(ltiNum>=25)
							{
								numOfPatchesOver25LTi++;
							}
							
							
							//newCSVWriter.append(st.nextToken()+",");
						}
						//newCSVWriter.append("\n");
						lineNumber++;
						
						
					}
	
				}
			
				numPatches.add(numberOfPatches);
				numPatches400.add(numOfPatchesOver400);
				numPatches25.add(numOfPatchesOver25LTi);
				
				// add this to the count of the batch
				totalNumberOfPatches += numberOfPatches ;
				totalNumberOfPatchesOver400 += numOfPatchesOver400;
				totalNumberOfPatchesOver25LTi += numOfPatchesOver25LTi;
			
			}
			
			newCSVWriter = new FileWriter("/home/kieran/Documents/SimRuns/PPCompRuns/BatchPatchSummary.csv",true);
			newCSVWriter.append(runDesc+",");
			
					
			newCSVWriter.append(Double.toString(totalNumberOfPatches / files.size())+",");
			// error bar
			newCSVWriter.append(Double.toString(standard_deviation(numPatches)/files.size())+",");
			
			newCSVWriter.append(Double.toString(totalNumberOfPatchesOver400 / files.size())+",");
			// error bar
			newCSVWriter.append(Double.toString(standard_deviation(numPatches400)/files.size())+",");
			
			newCSVWriter.append(Double.toString(totalNumberOfPatchesOver25LTi / files.size())+",");
			// error bar
			newCSVWriter.append(Double.toString(standard_deviation(numPatches25)/files.size())+"\n");
			
			newCSVWriter.close();
			
			totalNumberOfPatches=0 ;
			totalNumberOfPatchesOver400=0;
			totalNumberOfPatchesOver25LTi=0;
		}
		
		catch(Exception e)
		{
			System.out.println("Exception while reading csv file: " + e);
		}
		
	
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		try
		{
			ProcessBatchData batchAnalysis = new ProcessBatchData();
			
			newCSVWriter = new FileWriter("/home/kieran/Documents/SimRuns/PPCompRuns/BatchPatchSummary.csv");
			newCSVWriter.append("RUN DESC,NUMBER OF PATCHES,ERROR BAR,NUMBER OVER 400 MICRONS SQ,ERROR BAR,NUMBER OVER 25 LTI,ERROR BAR\n");
			newCSVWriter.close();
			
			// load the batch of each run & analyse
			batchAnalysis.populateFiles("2pt5",20);
			batchAnalysis.analyseFileSet("2pt5");
			
			batchAnalysis.populateFiles("5",20);
			batchAnalysis.analyseFileSet("5");
			
			batchAnalysis.populateFiles("pt1",20);
			batchAnalysis.analyseFileSet("pt1");
			
			batchAnalysis.populateFiles("pt5",20);
			batchAnalysis.analyseFileSet("pt5");
			
			batchAnalysis.populateFiles("pt25",20);
			batchAnalysis.analyseFileSet("pt25");
			
			batchAnalysis.populateFiles("pt25ChemKO",20);
			batchAnalysis.analyseFileSet("pt25ChemKO");
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
	
	

	}

}
