package ppatch_post_process;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class ProcessCSV 
{
	public static ArrayList<String> files = new ArrayList<String>();
	public static FileWriter newCSVWriter;
	
	public void processFile(String file)
	{
		try
		{
			String strFile = file+".csv";

			newCSVWriter = new FileWriter(file+"_New.csv");
			
			//create BufferedReader to read csv file
			BufferedReader br = new BufferedReader( new FileReader(strFile));

			String strLine = "";
			StringTokenizer st = null;
			int lineNumber = 0;

			//read comma separated file line by line
			while( (strLine = br.readLine()) != null)
			{
				if(lineNumber==0)
				{
					// print the headings
					st = new StringTokenizer(strLine, ",");
					newCSVWriter.append("N,");
				
					while(st.hasMoreTokens())
					{
						newCSVWriter.append(st.nextToken()+",");
					}
					newCSVWriter.append("\n");
					lineNumber++;
				}
		
				else
				{
					//break comma separated line using ","

					st = new StringTokenizer(strLine, ",");
					newCSVWriter.append(lineNumber+",");

					while(st.hasMoreTokens())
					{
						newCSVWriter.append(st.nextToken()+",");
					}
					newCSVWriter.append("\n");
					lineNumber++;
				}

			}
			newCSVWriter.close();


		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		
		
	}
	
	public static void main(String[] args) 
	{
		// NOW RUN FROM THE COMMAND LINE AS A STANDALONE PROGRAM
		
		ProcessCSV pcsv = new ProcessCSV();
		
		String filePath = args[0];
		int numRuns = Integer.parseInt(args[1]);
		
		for(int i=1;i<=numRuns;i++)
		{
			pcsv.processFile(filePath+i+"/trackedCells_Away");
			pcsv.processFile(filePath+i+"/trackedCells_Close");
		}
			
	}
}
