package ppatch_vcam;

/**
 * Models the effect of adhesion factors on LTin/LTi cell behaviour.  An object of this clas is attached to each LTo cell, and the effect on 
 * cell behaviour determined by the level of adhesion factor expressed.
 * 
 * @author kieran
 *
 */
public class VCAM_Adhesion_Effect 
{
	/**
	 * Level of adhesion factor expressed by this LTo cell
	 */
	public double vcamExpressionLevel;
	
	/**
	 * Initialise the level of expression
	 */
	public VCAM_Adhesion_Effect()
	{
		this.vcamExpressionLevel = 0;
	}
	
	/**
	 * Increase the level of expression with each stable contact
	 * 
	 * @param vcamIncrement	The level to increment expression by
	 */
	public void increaseVCAMExpressionLevel(double vcamIncrement)
	{
		this.vcamExpressionLevel = this.vcamExpressionLevel + vcamIncrement;
	}
	
	/**
	 * Determine if the level of expression affects LTin/LTi cell behaviour
	 * 
	 * @param vcamSlope	The slope of VCAM expression - how this rises with each stable contact
	 * @param maxVCAMeffectProbabilityCutoff	The maximum probability adhesion factors hold a cell in place around an LTo
	 * @param randNum	Represents the probability calculated that the adhesion factor has an influence
	 * @return	Boolean determining whether the cell stays put or moves away
	 */
	public boolean examineVCAMEffect(double vcamSlope, double maxVCAMeffectProbabilityCutoff,double randNum)
	{
		double probabilityProlongedAdhesion = vcamSlope * this.vcamExpressionLevel;
    	
    	if(probabilityProlongedAdhesion > maxVCAMeffectProbabilityCutoff)
    	{
    		probabilityProlongedAdhesion = maxVCAMeffectProbabilityCutoff;
    	}
    	
    	if(randNum<probabilityProlongedAdhesion)	// the cell will remain in contact with an LTo within the patch, held by vcam
    	{
    		return true;
    	}
    	else			// the cell will move away
    	{
    		return false;
    	}
		
	}
	
	/**
	 * Returns the vcam expression level of the LTo
	 * @return
	 */
	public double returnVCAMExpressionLevel()
	{
		return this.vcamExpressionLevel;
	}
}
