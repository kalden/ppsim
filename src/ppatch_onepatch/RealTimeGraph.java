package ppatch_onepatch;

import javax.swing.JFrame;

import sim.display.Controller;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;

public class RealTimeGraph extends GUIState
{
	public org.jfree.data.xy.XYSeries series;    // the data series we'll add to
    public sim.util.media.chart.TimeSeriesChartGenerator chart;  // the charting facility
	
    JFrame chartFrame = new JFrame();

    public RealTimeGraph(SimState state)
    {
    	super(state);
    	
    }
    
    public void init(Controller c)
    {
    	super.init(c);
    	
    	chart = new sim.util.media.chart.TimeSeriesChartGenerator();
        chart.setTitle("Put the title of your chart here");
        chart.setXAxisLabel("Time");
        chart.setYAxisLabel("X Axis");
        // perhaps you might move the chart to where you like.
        chartFrame.setVisible(true);
        chartFrame.setSize(400,400);
        chartFrame.add(chart);
        chartFrame.pack();
        c.registerFrame(chartFrame);
       
        // the console automatically moves itself to the right of all
        // of its registered frames -- you might wish to rearrange the
        // location of all the windows, including the console, at this
        // point in time....
    }
   
    
    public void start()
    {
    	super.start();
    	
    	chart.removeAllSeries();
    	series = new org.jfree.data.xy.XYSeries(
    			"Put a unique name for this series here so JFreeChart can hash with it",
    			false);
    	chart.addSeries(series, null);
    	scheduleRepeatingImmediatelyAfter(new Steppable()
        {
    		public void step(SimState state)
    		{
    			// at this stage we're adding data to our chart.  We
    			// 	need an X value and a Y value.  Typically the X
    			// value is the schedule's timestamp.  The Y value
    			// is whatever data you're extracting from your 
    			// simulation.  For purposes of illustration, let's
    			// extract the number of steps from the schedule and
    			// run it through a sin wave.
           
    			double x = state.schedule.getSteps(); 
    			System.out.println(x);
    			double y = Math.sin(state.schedule.getSteps()) * 10;
           
		       // now add the data
		       if (x >= state.schedule.EPOCH && x < state.schedule.AFTER_SIMULATION)
		           {
		           series.add(x, y, true);
		
		           // we're in the model thread right now, so we shouldn't directly
		           // update the chart.  Instead we request an update to occur the next
		           // time that control passes back to the Swing event thread.
		           chart.updateChartLater(state.schedule.getSteps());
		           }
		       }
        });
   }
    
    public void finish()
    {
    	super.finish();

    	chart.update(state.schedule.getSteps(), true);
    	chart.repaint();
    	chart.stopMovie();
    }


    public void quit()
    {
	    super.quit();
	
	    chart.update(state.schedule.getSteps(), true);
	    chart.repaint();
	    chart.stopMovie();
	    if (chartFrame != null)	chartFrame.dispose();
	    chartFrame = null;
    }

    

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
