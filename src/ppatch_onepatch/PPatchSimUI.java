package ppatch_onepatch;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;

import sim.display.Console;
import sim.display.Controller;
import sim.display.Display2D;
import sim.display.GUIState;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.portrayal.Inspector;
import sim.portrayal.continuous.ContinuousPortrayal2D;
import ycil_XMLUtilities.XMLFileUtilities;
import ppatch_onepatch.SimParameters2;


/**
 * The display class for the Peyer's Patch Simulation.  Attaches and monitors a graphical display which 
 * shows the cell behaviour within the tract
 * 
 * @author Kieran Alden
 *
 */
public class PPatchSimUI extends GUIState
{
	public org.jfree.data.xy.XYSeries series;    // the data series we'll add to
    public sim.util.media.chart.TimeSeriesChartGenerator chart;  // the charting facility
    public JFrame chartFrame = new JFrame();
	
	/**
	 * MASON Console which controls the simulation (when run in graphical mode)
	 */
    public static Console c;
    
    /**
	 * If run with a GUI, this forms the Continuous grid onto which the cells will be mapped onto (using x and y coordinates)
	 */
    public static ContinuousPortrayal2D tractPortrayal = new ContinuousPortrayal2D();
    
    /**
	 * If run with a GUI, this is the frame used within the display to contain the intestine tract 
	 */
    public static JFrame displayFrame;   
    
    /**
     * Folder where simulation results should be output to
     */
    public String filePath;
    
    /**
     * Description of this run - used to form the results folder name
     */
    public String runDescription;
    
    /**
     * Full file path to the parameter XML file
     */
    public String paramFilePath;
    
    /**
     * Holds the simulation parameters gathered from the website if the simulator is being run online
     */
    public SimParameters2 webRunSP;
    
	
	/**
	 * Main Class - Begins the simulation in graphical mode
	 * @param args
	 */
	public static void main(String[] args)
	{
		// Set up the simulation display & console
		// Check whether running from the command line or eclipse
		if(args.length > 0)
		{
			// Command line run (arguments should have been supplied!)
			PPatchSimUI ppatchdisp = new PPatchSimUI(args[0],args[1],args[2]);
		}
		else
		{
			// Assume Eclipse Run (though will enter here if command line and no args supplied)
			// Else an exception will be generated
			String runDescription = "Control";
			String paramFilePath = "/home/kieran/Dropbox/WorkingDocs/parameters-master.xml";
			String filePath = "/home/kieran/Desktop/";
			
			PPatchSimUI ppatchdisp = new PPatchSimUI(runDescription,paramFilePath,filePath);
		}
	}
	
	/**
	 * Constructor - attaches the simulation to the graphics class
	 */
	public PPatchSimUI(String runDesc, String paramFileP, String dirPath)
	{		
		super(new PPatchSim(System.currentTimeMillis()));
		
		this.filePath = dirPath;
		this.runDescription = runDesc;
		this.paramFilePath = paramFileP;
		
		c = new Console(this);
		c.setVisible(true);
		
		// set the console to always appear in the bottom right hand corner (not covering model)
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		// Note 30 is taken from calculation so that the console appears above / away from any toolbars
		// on the edge of the screen
		c.setLocation((dim.width-c.getSize().width-30),(dim.height-c.getSize().height-30));
		c.pressPlay();
				
	}
	
	
	/**
	 * Web run constructor - gets the parameters from an XML file stored online, or from the form completed by the user
	 * 
	 * @param sp	The simulation parameter values
	 */
	public PPatchSimUI(SimParameters2 sp)
	{
		super(new PPatchSim(System.currentTimeMillis()));
		this.filePath = null;
		this.runDescription = null;
		this.paramFilePath = null;
		
		//this.webRunSP = sp;
		
		c = new Console(this);
		c.setVisible(true);
		
		// set the console to always appear in the bottom right hand corner (not covering model)
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		// Note 30 is taken from calculation so that the console appears above / away from any toolbars
		// on the edge of the screen
		c.setLocation((dim.width-c.getSize().width-30),(dim.height-c.getSize().height-30));
		c.pressPlay();
	}
	
	public PPatchSimUI(SimState state)
	{
		super(state);
	}
	
	/**
	 * Sets name of the display window
	 * @return Console Window Name
	 */
	public static String getName()
	{
		return "Peyer's Patch Simulation";
	}
	
	/**
	 * Begins the simulation when the start button is pressed
	 */
	public void start()
	{
		super.start();
		setupPortrayals();
		
		// Real time graphing
		// TRIAL REAL TIME GRAPHING
		chart.removeAllSeries();
		    	series = new org.jfree.data.xy.XYSeries(
		    			"Put a unique name for this series here so JFreeChart can hash with it",
		    			false);
		    	chart.addSeries(series, null);
		    	scheduleRepeatingImmediatelyAfter(new Steppable()
		        {
		    		public void step(SimState state)
		    		{
		    			// at this stage we're adding data to our chart.  We
		    			// 	need an X value and a Y value.  Typically the X
		    			// value is the schedule's timestamp.  The Y value
		    			// is whatever data you're extracting from your 
		    			// simulation.  For purposes of illustration, let's
		    			// extract the number of steps from the schedule and
		    			// run it through a sin wave.
		           
		    			double x = state.schedule.getSteps(); 
		    			System.out.println(x);
		    			double y = Math.sin(state.schedule.getSteps()) * 10;
		           
				       // now add the data
				       if (x >= state.schedule.EPOCH && x < state.schedule.AFTER_SIMULATION)
				           {
				           series.add(x, y, true);
				
				           // we're in the model thread right now, so we shouldn't directly
				           // update the chart.  Instead we request an update to occur the next
				           // time that control passes back to the Swing event thread.
				           chart.updateChartLater(state.schedule.getSteps());
				           }
				       }
		        });
				
	}
	
	/**
	 * Reloads the presentation if the stop button is pressed
	 */
	public void load(SimState state)
	{
		super.load(state);
		setupPortrayals();
	}
	
	/**
	 * Determines how each cell will be represented on the tract
	 */
	public void setupPortrayals()
	{
		PPatchSim ppsim = (PPatchSim)state;
		tractPortrayal.setField(ppsim.tract);
		ppsim.display.reset();
		ppsim.display.repaint();
		
	}	
	
	/**
	 * Initialises the display once the simulation starts & creates the relevant window
	 */
	public void init(Controller c)
	{	
		PPatchSim ppsim = (PPatchSim)state;
		
		if(this.webRunSP == null)
		{
			PPatchSim.filePath = this.filePath;
			PPatchSim.runDescription = this.runDescription;
			PPatchSim.paramFilePath = this.paramFilePath;
			XMLFileUtilities.readSettingsFile(paramFilePath);
			PPatchSim.simParams = new SimParameters2();
			
			
			
			
		
		}
		else
		{
			ppsim.simParams = this.webRunSP;
		}
		
		ppsim.simParams.displayFlag = true;
		
		// Set up the display to the dimensions the user entered
		ppsim.display = new Display2D(ppsim.simParams.initialGridLength,ppsim.simParams.initialGridHeight,this,1);
		
		// create the display frame to hold it
		displayFrame = ppsim.display.createFrame();
		
		// initialise all features of the simulation software
		super.init(c);
		
		
		// Set the display frame to be the width of the users screen, plus around 400 (so as much of intestine as possible is shown)
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		displayFrame.setSize(dim.width, 400);
		c.registerFrame(displayFrame);

		// attach all representations of the environment & cells
		ppsim.display.attach(tractPortrayal,"Agents");
		ppsim.display.setBackdrop(Color.black);
		//ppsim.display.setBackdrop(Color.DARK_GRAY);
		displayFrame.setTitle("Intestine Tract Display");
		
		displayFrame.setVisible(true);
		
		
		// NEW REAL TIME CHARTING
		chart = new sim.util.media.chart.TimeSeriesChartGenerator();
        chart.setTitle("Put the title of your chart here");
        chart.setXAxisLabel("Time");
        chart.setYAxisLabel("X Axis");
        // perhaps you might move the chart to where you like.
        chartFrame.setVisible(true);
        chartFrame.setSize(400,400);
        chartFrame.add(chart);
        chartFrame.pack();
        c.registerFrame(chartFrame);
	
	
	}
	
	/**
	 * Destroys the frame when the simulation ends
	 */
	public void quit()
	{
		//super.quit();
		
		//if (displayFrame!=null) displayFrame.dispose();
        //	displayFrame = null;
        //PPatchSim.display = null;

		chart.update(state.schedule.getSteps(), true);
	    chart.repaint();
	    chart.stopMovie();
	    if (chartFrame != null)	chartFrame.dispose();
	    chartFrame = null;
	}
	
	/**
	 * Returns the state - used for inspection of variables within the console
	 */
	public Object getSimulationInspectedObject()
	{
		return state;
	}
	
	 public void finish()
	    {
	    	super.finish();

	    	chart.update(state.schedule.getSteps(), true);
	    	chart.repaint();
	    	chart.stopMovie();
	    }
	
	/**
	 * Set up an inspector with which to use to monitor variables and create graphs within the console
	 */
	public Inspector getInspector()
	{
		Inspector i = super.getInspector();
		i.setVolatile(true);
		return i;
	}
}