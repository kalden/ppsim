package ppatch_onepatch;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.StringTokenizer;
import java.net.URL;
import java.io.File;
import java.io.InputStream;
import org.w3c.dom.*;

import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;


/**
 * Takes care of the parameter side of the simulation.  Reads the parameters in (whether from an XML file or from the web form), 
 * ensures these are stored, and this object can then be passed around the simulation and accessed by the various classes that may 
 * require them
 * 
 * @author kieran
 *
 */
public class SimParametersBackup
{
	
	/***************************************************************************************************
	 * 						SIMULATION CONTROL (NOT PART OF MODEL & STATS) 
	 */

	/**
	 * <a name = "webRun"></a>
	 * <b>Description:<br></b> 
	 * Flag to state whether this is being run from the web front end or not - if so stats generation 
	 * will need to be turned off
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Boolean
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public boolean webRun = false;
	
	/**
	 * <a name = "tags"></a>
	 * <b>Description:<br></b> 
	 * Linked list containing all the tags used in the XML file - used later in creation of the web form
	 * (if this is being used as the front end) so is populated as the tags are read
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * None
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public LinkedHashMap tags = new LinkedHashMap();
	
	/**
	 * <a name = "description"></a>
	 * <b>Description:<br></b> 
	 * Short description of this run, used to create the output folder containing all the data 
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Text String
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 * 
	 */
	public String description;
	
	
	/**
	 * <a name = "filePath"></a>
	 * <b>Description:<br></b> 
	 * FilePath of where the results files should be output to
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Text String
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 * 
	 */
	public String filePath;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 1
	 * 									ENVIRONMENT						 
	 */
	
	/**
	 * <a name = "displayFlag"></a>
	 * <b>Description:<br></b> 
	 * Boolean which notes whether the GUI display is enabled or not - chooses between PPatchSim and PPatchSimUI
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Boolean
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public boolean displayFlag = true;
	
	/**
	 * <a name = "initialGridHeight"></a>
	 * <b>Description:<br></b> 
	 * This value (in pixels) is the circumference of measurement of the intestine tract when the simulation begins. 
	 * The tract is modelled on a 2D plane, as if the intestine had been cut along it's length.  Any cells that leave 
	 * the bottom of the screen will reappear at the top
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels.  1 Pixel = 4 microns
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 * 
	 */
	public static double initialGridHeight;
	
	/**
	 * <a name = "initialGridLength"></a>
	 * <b>Description:<br></b> 
	 * Value (in pixels) representing the length of the tract when the simulation begins.  Any cells that leave the left or right hand 
	 * side of the screen are deemed to be removed from the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels.  1 Pixel = 4 microns
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public static double initialGridLength;
	
	/**
	 * <a name = "upperGridHeight"></a>
	 * <b>Description:<br></b> 
	 * Upper bound height at which the tract should grow to over the simulation - the height increases with each step until this is measure is 
	 * reached at the end of the 48 hours
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels.  1 Pixel = 4 microns
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public static double upperGridHeight;
	
	/**
	 * <a name = "upperGridLength"></a>
	 * <b>Description:<br></b> 
	 * Upper bound length at which the tract should grow to over the simulation - the length increases with each step until this is length is 
	 * reached at the end of the 48 hours
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels.  1 Pixel = 4 microns
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public static double upperGridLength;
	
	/**
	 * <a name = "secondsPerStep"></a>
	 * <b>Description:<br></b> 
	 * The number of seconds of development time represented by 1 step of the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Seconds
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public Integer secondsPerStep;

	/**
	 * <a name = "simulationTime"></a>
	 * <b>Description:<br></b> 
	 * The number of hours the simulation will run (in developmental time)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Seconds
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int simulationTime;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 2
	 * 									INTERACTION						 
	 */
	
	/**
	 * <a name = "thresholdBindProbability"></a>
	 * <b>Description:<br></b> 
	 * The probability that when two cells come into contact, there will be a stable bind and signalling 
	 * will occur
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0, yet less than or equal to 100
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Probability
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double thresholdBindProbability;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 3
	 * 									STROMA PARAMETERS						 
	 */
	
	/**
	 * <a name = "stromalCellDensity"></a>
	 * <b>Description:<br></b> 
	 * Density of stromal cells on the surface of the tract.  If 100%, tract will be complete with tightly packed LTo cells - reducing this spreads
	 * the number of cells that can be potentially active out throughout the tract
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and between 1 and 100
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Percentage
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int stromalCellDensity;
	
	/**
	 * <a name = "percentStromaRETLigands"></a>
	 * <b>Description:<br></b> 
	 * Percentage of cells on the stroma that will express artemin when the simulation begins (i.e. & therefore be active in terms of forming patches)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be between 0 and 100
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Percentage of all cells on the stroma (determined by a density function - see stromaDensity
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double percentStromaRETLigands;
	
	
	/**
	 * <a name = "percentRETLigandNonStromal"></a>
	 * <b>Description:<br></b> 
	 * Percentage of cells on the stroma that express RET Ligand but do not have the capacity to become patches - LTin/LTi cells can bind but no 
	 * signalling occurs
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be between 0 and 100
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Percentage of all cells on the stroma (determined by a density function - see stromaDensity)
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double percentRETLigandNonStromal;
	
	/**
	 * <a name = "numHoursRETLigandLToActive"></a>
	 * <b>Description:<br></b> 
	 * Time (in hours) for which LTo expressing RET ligand is active before being 'removed' from the simulation.  
	 * In reality, the cell is hidden from view and set to not be contactable so the chemokine level can still be calculated
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be above 0 and less than 72
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int numHoursRETLigandLToActive;
	
	/**
	 * <a name = "probabilityLTinLTiDecoyRETLigands"></a>
	 * <b>Description:<br></b> 
	 * Probability that a new LTin/LTi cell will express RET Ligand (and can therefore bind other LTin/LTi cells
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be above 0 and less than 1
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Probability
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double probabilityLTinLTiRETLigands;
	
	
	/**
	 * <a name = "imLToActiveTime"></a>
	 * <b>Description:<br></b> 
	 * Time (in hours) for which an immature cell is active before becoming inactive & being removed from the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be above 0 and less than 48
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int imLToActiveTime;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 4
	 * 										LTIN						 
	 */
	
	/**
	 * <a name = "percentLTinfromFACStain"></a>
	 * <b>Description:<br></b> 
	 * Percentage of LTin cells on the tract at E15.5, taken from FACS stain.  Used to calculate input rate
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be 0 or above, but can be decimal
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * If whole tract was full of cells, this is the percentage of cells that would be LTin
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 * 
	 * 
	 */
	public double percentLTinfromFACStain;
	
	/**
	 * <a name = "lTinInputTime"></a>
	 * <b>Description:<br></b> 
	 * Number of hours before LTin migration ceases.
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be above 0, numeric, and less than 72
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int lTinInputTime;
	
	/**
	 * <a name = "lTinInputRateGraphType"></a>
	 * <b>Description:<br></b> 
	 * Type of curve used to generate the input rate - can be exponential (exp), square root (sqrt) or linear (line)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be exp, sqrt, or line
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Type of line graph
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public String lTinInputRateGraphType;
	
	
	/**
	 * <a name = "lTinInputRateGraphConstant"></a>
	 * <b>Description:<br></b> 
	 * Where the input rate function is exponential or square root, this sets the constant which adjusts the line
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Number to feed into function
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 * 
	 */
	public double lTinInputRateGraphConstant;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 5
	 * 										LTI						 
	 */
	
	/**
	 * <a name = "percentLTifromFACStain"></a>
	 * <b>Description:<br></b> 
	 * Percentage of LTi cells on the tract at E15.5, taken from FACS stain.  Used to calculate input rate
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be 0 or above, but can be decimal
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * If whole tract was full of cells, this is the percentage of cells that would be LTi
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 * 
	 */
	public double percentLTifromFACStain;
	
	/**
	 * <a name = "lTiInputDelayTime"></a>
	 * <b>Description:<br></b> 
	 * Number of hours before LTi migration begins.
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric, above 0, and less than 48
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int lTiInputDelayTime;
	
	/**
	 * <a name = "lTiInputTime"></a>
	 * <b>Description:<br></b> 
	 * Number of hours after LTi migration begins that this ceases.
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric, above 0, and less than 72
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int lTiInputTime;
	
	/**
	 * <a name = "lTiInputRateGraphType"></a>
	 * <b>Description:<br></b> 
	 * Type of curve used to generate the input rate - can be exponential (exp), square root (sqrt) or linear (line)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be exp, sqrt, or line
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Type of line graph
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public String lTiInputRateGraphType;
	
	
	/**
	 * <a name = "lTinInputRateGraphConstant"></a>
	 * <b>Description:<br></b> 
	 * Where the input rate function is exponential or square root, this sets the constant which adjusts the line
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Number to feed into function
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double lTiInputRateGraphConstant;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 6
	 * 										CHEMOKINE						 
	 */
	
	/**
	 * <a name = "chemoThreshold"></a>
	 * <b>Description:<br></b> 
	 * Threshold value at which chemokine will affect the movement of the LTi cell
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be a decimal number between 0 and 1
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Chemokine level is determined by a sigmoid function.  As this can give a result anywhere between 0 and 1, a lower level cut off is needed
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double chemoThreshold;
	
	
	/**
	 * <a name = "chemoUpperLinearAdjust"></a>
	 * <b>Description:<br></b> 
	 * Upper bound to use for the linear adjustment applied to the chemokine sigmoid function.
	 * Upper bound makes the s curve steep. The curve is adjusted from this point to the lower bound to make it more linear
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be a double
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Unit at which sigmoid function stops being adjusted
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double chemoUpperLinearAdjust;
	
	/**
	 * <a name = "chemoLowerLinearAdjust"></a>
	 * <b>Description:<br></b> 
	 * Lower bound to use for the linear adjustment applied to the chemokine sigmoid function.
	 * Lower bound makes the s curve more linear. The curve is adjusted from this point to the lower bound to make it more linear
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be a double
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Unit at which sigmoid function stops being adjusted
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double chemoLowerLinearAdjust;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 7
	 * 										VCAM						 
	 */
	
	/**
	 * <a name = "maxVCAMeffectProbabilityCutoff"></a>
	 * <b>Description:<br></b> 
	 * Ensures the probability of 'sticking' cannot go past a set threshold - ensures some stochasticity
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be a double between 0 and 1
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Probability
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double maxVCAMeffectProbabilityCutoff;

	/**
	 * <a name = "vcamSlope"></a>
	 * <b>Description:<br></b> 
	 * Slope of the linear VCAM line which determines the probability that VCAM will hold the cells in contact with
	 * a forming patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be a double
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Unit at which the linear function rises (i.e. the slope)
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
    public double vcamSlope;
	
	
    /***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 8
	 * 										KNOCKOUTS						 
	 */
	
    /**
	 * <a name = "retLigandKnockOut"></a>
	 * <b>Description:<br></b> 
	 * Knockout flag determining if ART/RET pathway has been knocked out (for experimentation purposes)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public boolean retLigandKnockOut;
	
	/**
	 * <a name = "chemoKnockOut"></a>
	 * <b>Description:<br></b> 
	 * Knockout flag determining if chemokine has been knocked-out (for experimentation purposes)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public boolean chemoKnockOut;
	
	/**
	 * <a name = "vcamKnockOut"></a>
	 * <b>Description:<br></b> 
	 * Knockout flag determining if VCAM has been knocked-out (for experimentation purposes)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public boolean vcamKnockOut;
	
	
	/***************************************************************************************************
	 * 						PARAMETERS READ IN FROM XML FILE - GROUP 9
	 * 										OUTPUT CONTROL	 
	 */
	
	/**
	 * <a name = "cellTrackingEnabled"></a>
	 * <b>Description:<br></b> 
	 * Knockout flag determining if cell tracking is on
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public Boolean cellTrackingEnabled;
	
	/**
	 * <a name = "stepBystepTrackingImages"></a>
	 * <b>Description:<br></b> 
	 * Knockout flag determining if the simulation is taking snapshots (for movie generation)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public Boolean stepBystepTrackingImages;
	
	/**
	 * <a name = "trackingHourRanges"></a>
	 * <b>Description:<br></b> 
	 * Hour ranges at which cell tracking should be performed.
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Should be a string of two hours, separated by -, with the ranges separated by a ,
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public String trackingHourRanges;
	
	
	
	/**
	 * <a name = "twelveHourSnaps"></a>
	 * <b>Description:<br></b> 
	 * Knockout flag determining if the simulation is taking snapshots at 12 hour intervals
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public Boolean twelveHourSnaps;
	
	/**
	 * <a name = "generateLToStats"></a>
	 * <b>Description:<br></b> 
	 * Knockout flag determining if the simulation is producing statistics of LTo data
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be true or false
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public Boolean generateLToStats;
	
	
	/***************************************************************************************************
	 * 						OTHER PARAMETERS USED IN SIMULATION CONTROL AND STATS GENERATION
	 * 											 
	 */
	
	/**
	 * <a name = "lTinCellularity"></a>
	 * <b>Description:<br></b> 
	 *Counter for the number of LTin cells in the tract - used in stats generation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be initialised to zero and increased as each cell is added; reduced as cells leave screen
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int lTinCellularity = 0;
	
	/**
	 * <a name = "lTiCellularity"></a>
	 * <b>Description:<br></b> 
	 *Counter for the number of LTi cells in the tract - used in stats generation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be initialised to zero and increased as each cell is added; reduced as cells leave screen
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int lTiCellularity = 0;
	
	/**
	 * <a name = "lTinInputRate"></a>
	 * <b>Description:<br></b> 
	 * Rate at which LTin cells enter the tract per step.  Adjusted as the simulation progresses.  Having this working rate, rather
	 * than adjusting the initial rate, is useful later for stats etc
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Number of LTin cells that enter the tract in one minute
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double lTinInputRate=0;
	
	/**
	 * <a name = "lTiInputRate"></a>
	 * <b>Description:<br></b> 
	 * Rate at which LTi cells enter the tract per step.  Adjusted as the simulation progresses.  Having this working rate, rather
	 * than adjusting the initial rate, is useful later for stats etc
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Internal only, Must be numeric and above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Number of LTi cells that enter the tract in one minute
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double lTiInputRate=0;
	
	/**
	 * <a name = "trackingStartHours"></a>
	 * <b>Description:<br></b> 
	 * ArrayList of just the tracking start hours.  Each is removed once that track is complete
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Internal only
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public ArrayList<Integer> trackingStartHours = new ArrayList<Integer>();
	
	/**
	 * <a name = "trackingEndHours"></a>
	 * <b>Description:<br></b> 
	 * ArrayList of just the tracking end hours.  Each is removed once that track is complete
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Internal only
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public ArrayList<Integer> trackingEndHours = new ArrayList<Integer>();
	
	/**
	 * <a name = "trackingSnapStartHr"></a>
	 * <b>Description:<br></b> 
	 * Hour at which the next set of tracking is due to start. Changes if more than one range is used
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Internal Only
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double trackingSnapStartHr;;
	
	/**
	 * <a name = "trackingSnapStartHr"></a>
	 * <b>Description:<br></b> 
	 * Hour at which the next set of tracking is due to end. Changes if more than one range is used
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Internal Only
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * n/a
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double trackingSnapEndHr;
	
	
	/***************************************************************************************************
	 * ***************************************************************************************************
	 * ***************************************************************************************************
	 * 								CONSTANT PARAMETERS
	 * 							VALUES WHICH DO NOT CHANGE WITH EACH RUN
	 */
	
	
	/***************************************************************************************************
	 * 									CELL RELATED CONSTANT PARAMETERS
	 * 											 
	 */
	
	/**
	 * <a name = "lToDivisionTime"></a>
	 * <b>Description:<br></b> 
	 * Number of hours taken for an LTo cell to divide
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Set to 12 a an assumption
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int lToDivisionTime = 12;		
	
	/**
	 * <a name = "LTO_DIAMETER"></a>
	 * <b>Description:<br></b> 
	 * Diameter of LTo Cell (in pixels - 1 pixel = 4 micrometres)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Set as has been verified experimentally
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public static final double LTO_DIAMETER = 6;
	
	/**
	 * <a name = "HCELL_DIAMETER"></a>
	 * <b>Description:<br></b> 
	 * Diameter of LTin and LTi Cells (in pixels - 1 pixel = 4 micrometres)
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Set as has been verified experimentally
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public static double HCELL_DIAMETER = 2;
	
	/**
	 * <a name = "cellSpeedMinLowBound"></a>
	 * <b>Description:<br></b> 
	 * Lower bound to use to generate cell speed. Can be changed on the console before running the simulation.
	 * As 1 pixel is 4micrometres, initial coding represents a lower bound move of 4micrometres a minute
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Set as has been verified experimentally (in speed per minute)
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels per minute
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double cellSpeedMinLowBound = 0.95;
	
	/**
	 * <a name = "cellSpeedMinUpBound"></a>
	 * <b>Description:<br></b> 
	 * Upper bound to use to generate cell speed. Can be changed on the console before running the simulation
	 * As 1 pixel is 4micrometres, initial coding represents an upper bound move of 16micrometres a minute
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Set as has been verified experimentally (in speed per minute)
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels per minute
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double cellSpeedMinUpBound = 2.2;
	
	/**
	 * <a name = "cellSpeedLowBound"></a>
	 * <b>Description:<br></b> 
	 * Used to calculate the cell speed lower bound used in the simulation - as the known lower bound is in speed per 
	 * minute, and the simulation steps may not be in minutes, this may need calculating
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Lower Cell Speed per amount of time set in secondsPerStep
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double cellSpeedLowBound;
	
	/**
	 * <a name = "cellSpeedUpBound"></a>
	 * <b>Description:<br></b> 
	 * Used to calculate the cell speed upper bound used in the simulation - as the known upper bound is in speed per 
	 * minute, and the simulation steps may not be in minutes, this may need calculating
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Lower Cell Speed per amount of time set in secondsPerStep
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double cellSpeedUpBound;
	
	/**
	 * <a name = "ltoDistanceThreshold"></a>
	 * <b>Description:<br></b> 
	 * Distance threshold from an active LTo at which an immature LTo cell can become active
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Distance in pixels
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double ltoDistanceThreshold = 1;     
	
	/**
	 * <a name = "numContactsActivateLTo"></a>
	 * <b>Description:<br></b> 
	 * Number of times an inactive cell is in contact with an LTin/LTi cell before it becomes active
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be above 0
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Count of number of contacts
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public int numContactsActivateLTo = 10;
	
	
	/***************************************************************************************************
	 * 								ENVIRONMENT RELATED CONSTANT PARAMETERS
	 * 											 
	 */
	
	/**
	 * <a name = "growthTime"></a>
	 * <b>Description:<br></b> 
	 * The number of hours over which the intestine tract grows from its initial to upper sizes
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Should be less than or equal to 72
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Hours
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public static int growthTime = 72;
	
	
	
	/***************************************************************************************************
	 * 									ADHESION RELATED CONSTANT PARAMETERS
	 * 											 
	 */
	
	/**
	 * <a name = "vcamIncrement"></a>
	 * <b>Description:<br></b> 
	 * Value at which to increment the vcam expression on each contact
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Must be a decimal between 0 and 1
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Numerical Increase
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double vcamIncrement = 0.05;
	
		
	/***************************************************************************************************
	 * 									CHEMOKINE RELATED CONSTANT PARAMETERS
	 * 											 
	 */
	
	/**
	 * <a name = "chemoSigCurveThreshold"></a>
	 * <b>Description:<br></b> 
	 * Threshold which is used to centre the CCL13/CXCL21 sigmoid curve.
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * Set at 3 as this puts the curve spot on at the axis rather than having the axis in the middle
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Curve Function
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double chemoSigCurveThreshold = 3;
	

	/**
	 * <a name = "chemoLinearAdjustmentReducer"></a>
	 * <b>Description:<br></b> 
	 * Adjustment applied to the linear adjustment of the chemokine sigmoid curve with each contact
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Curve Adjustment Function
	 * <br><br>
	 * <b>Link to Domain Model:</b>
	 */
	public double chemoLinearAdjustmentReducer = 0.005;
	
	
	/***************************************************************************************************
	 * 								CLASS METHODS
	 */
	
	
	
	/**
	 * readSimParameters - gets the parameter group from the XML file so each parameter can be extracted by the relevant function
	 * 
	 * @param doc	The XML document to be parsed
	 * @param groupNum	The parameter group to be extracted
	 * @return	The XML nodes for this group
	 */
	public NodeList readSimParameters(Document doc,int groupNum)
	{
		// Get the Parameter Group Tags
		NodeList paramGroups = doc.getDocumentElement().getChildNodes();
		Node n;
		NodeList groupVars;
								
		n = paramGroups.item(groupNum);
		groupVars = n.getChildNodes();
				
		return groupVars;
	}
	
	
	/**
	 * Reads in the Environment Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_Environment_Params(NodeList groupVals)
	{
		/**
		 * OLD READ IN METHOD
		 
		NodeList env_group = this.readSimParameters(doc,0);
		
		Node n = env_group.item(0);
		//displayFlag = Boolean.parseBoolean(n.getTextContent());
		
		n = env_group.item(1);
		initialGridHeight = Double.parseDouble(n.getTextContent());
		
		n = env_group.item(2);
		initialGridLength = Double.parseDouble(n.getTextContent());
		
		n = env_group.item(3);
		upperGridHeight = Double.parseDouble(n.getTextContent());
		
		n = env_group.item(4);
		upperGridLength = Double.parseDouble(n.getTextContent());
		
		n = env_group.item(5);
		secondsPerStep = Integer.parseInt(n.getTextContent());
		
		n = env_group.item(6);
		simulationTime = Integer.parseInt(n.getTextContent());
		*/
	}
	
	/**
	 * Reads in the Interaction Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_Interaction_Params(NodeList groupVals)
	{
		/**
		 * OLD READ IN METHOD
		NodeList int_group = this.readSimParameters(doc,1);
		
		Node n = int_group.item(0);
		thresholdBindProbability = Double.parseDouble(n.getTextContent());
		*/
	}
	
	/**
	 * Reads in the Stroma Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_Stroma_Params(NodeList groupVals)
	{
		/**
		 * OLD READ IN METHOD
		NodeList stroma_group = this.readSimParameters(doc,2);
		
		Node n = stroma_group.item(0);
		stromalCellDensity = Integer.parseInt(n.getTextContent());
		
		n = stroma_group.item(1);
    	percentStromaRETLigands = Double.parseDouble(n.getTextContent());
    	
    	n = stroma_group.item(2);
    	percentRETLigandNonStromal = Double.parseDouble(n.getTextContent());
    	
    	n = stroma_group.item(3);
    	numHoursRETLigandLToActive = Integer.parseInt(n.getTextContent());
    	
    	n = stroma_group.item(4);
    	probabilityLTinLTiRETLigands = Double.parseDouble(n.getTextContent());
    	
    	n = stroma_group.item(5);
    	imLToActiveTime = Integer.parseInt(n.getTextContent());
		**/
	}
	
	/**
	 * Reads in the LTin Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_LTin_Params(NodeList groupVals)
	{
		/**
		 * OLD READ IN METHOD
		NodeList ltin_group = this.readSimParameters(doc,3);
		
		Node n = ltin_group.item(0);
		percentLTinfromFACStain = Double.parseDouble(n.getTextContent());
		
		n = ltin_group.item(1);
    	lTinInputTime = Integer.parseInt(n.getTextContent());
    	
    	n = ltin_group.item(2);
    	lTinInputRateGraphType = n.getTextContent();
    	
    	n = ltin_group.item(3);
    	lTinInputRateGraphConstant = Double.parseDouble(n.getTextContent());
    	**/
	}
	
	/**
	 * Reads in the LTi Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_LTi_Params(NodeList groupVals)
	{
		percentLTifromFACStain = Double.parseDouble(groupVals.item(1).getChildNodes().item(1).getTextContent());
		System.out.println(percentLTifromFACStain);
    	lTiInputDelayTime = Integer.parseInt(groupVals.item(3).getChildNodes().item(1).getTextContent());
    	lTiInputTime = Integer.parseInt(groupVals.item(5).getChildNodes().item(1).getTextContent());
    	System.out.println(lTiInputTime);
    	lTiInputRateGraphType = groupVals.item(7).getChildNodes().item(1).getTextContent();
    	lTiInputRateGraphConstant = Double.parseDouble(groupVals.item(9).getChildNodes().item(1).getTextContent());
    	System.out.println(lTiInputRateGraphConstant);
	}
	
	/**
	 * Reads in the Chemokine Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_Chemokine_Params(NodeList groupVals)
	{
		chemoThreshold = Double.parseDouble(groupVals.item(1).getChildNodes().item(1).getTextContent());
		chemoUpperLinearAdjust = Double.parseDouble(groupVals.item(3).getChildNodes().item(1).getTextContent());
		chemoLowerLinearAdjust = Double.parseDouble(groupVals.item(5).getChildNodes().item(1).getTextContent());
				
	}
	
	/**
	 * Reads in the VCAM Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_VCAM_Params(NodeList groupVals)
	{
		maxVCAMeffectProbabilityCutoff = Double.parseDouble(groupVals.item(1).getChildNodes().item(1).getTextContent());
		vcamSlope = Double.parseDouble(groupVals.item(3).getChildNodes().item(1).getTextContent());
	}
	
	/**
	 * Reads in the Knockout Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_Knockout_Params(NodeList groupVals)
	{	
		retLigandKnockOut = Boolean.parseBoolean(groupVals.item(1).getChildNodes().item(1).getTextContent());
		chemoKnockOut = Boolean.parseBoolean(groupVals.item(3).getChildNodes().item(1).getTextContent());
		vcamKnockOut = Boolean.parseBoolean(groupVals.item(5).getChildNodes().item(1).getTextContent());
		
		/**
		NodeList ko_group = this.readSimParameters(doc,7);
		
		Node n = ko_group.item(0);
		retLigandKnockOut = Boolean.parseBoolean(n.getTextContent());
		
		n = ko_group.item(1);
    	chemoKnockOut = Boolean.parseBoolean(n.getTextContent());
    	
    	n = ko_group.item(2);
    	vcamKnockOut = Boolean.parseBoolean(n.getTextContent());
    	**/
		
	}
	
	/**
	 * Reads in the Output Control Parameters from the XML file
	 * 
	 * @param doc	The XML document to be parsed
	 */
	public void read_Output_Control(Document doc)
	{
		NodeList out_group = this.readSimParameters(doc,8);
		
		Node n = out_group.item(0);
		cellTrackingEnabled = Boolean.parseBoolean(n.getTextContent());
		
		n = out_group.item(1);
    	stepBystepTrackingImages = Boolean.parseBoolean(n.getTextContent());
    	
    	n = out_group.item(2);
    	trackingHourRanges = n.getTextContent();
    	
    	// process the tracking hour ranges - they are input as a string 
    	// i.e. 12-14,15-17,19-21
    	// If tracking is not enabled, this can be input as NULL
    	
    	if(this.cellTrackingEnabled)
    	{
    		// split the ranges by the comma
    		StringTokenizer st = new StringTokenizer(trackingHourRanges,",");
    	
    		// now process the start and end for each
    		while(st.hasMoreTokens())
    		{
    			// now split the substring by the '-'
    			StringTokenizer st2 = new StringTokenizer(st.nextToken(),"-");
    			this.trackingStartHours.add(Integer.parseInt(st2.nextToken()));
    			this.trackingEndHours.add(Integer.parseInt(st2.nextToken()));
    		}
    		
    		// Set the first tracking range
    		this.trackingSnapStartHr = this.trackingStartHours.get(0);
    		this.trackingSnapEndHr = this.trackingEndHours.get(0);
    	}
    	
    	n = out_group.item(3);
    	twelveHourSnaps = Boolean.parseBoolean(n.getTextContent());
    	
    	n = out_group.item(4);
    	generateLToStats = Boolean.parseBoolean(n.getTextContent());
  	
	}
	
	/**
	 * Blank Constructor - used by the web service, which doesn't have XML file or output directory as input
	 */
	public SimParametersBackup()
	{}
	
	/**
	 * Constructor called when the simulation starts (if not using the web interface)
	 * Reads in the XML file and sets the simulation parameters accordingly
	 * 
	 * @param runDescription	The description to use of this run (normally becomes the folder name)
	 * @param paramFilePath	The full file path to the XML file
	 * @param runFilePath	The full file path to where the results should be stored
	 */
	public SimParametersBackup(String runDescription,String paramFilePath,String runFilePath)
	{
		try
		{
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc;
			
			// 6/1/12 - IF LOOP TO CHECK FOR WEB SERVICE REMOVED, AS THIS NOW GOES IN BLANK CONSTRUCTOR, NOT HERE
			// make a directory for all of the output files
			this.filePath = runFilePath;
			this.description = runDescription;
			new File(this.filePath+"/"+this.description).mkdir();
			doc = docBuilder.parse(new File(paramFilePath));
		
			/**
			 * FIRSTLY, GET ALL THE CHILD NODES - REPRESENT EACH PARAMETER GROUP
			 * 
			 * Some of the maths looks weird from this point onwards.  When the XML is read in, it is read in
			 * as #text, <NodeName>, #text, <NodeName> etc.  Therefore, we need to skip the #text parts, which
			 * is why the for loop starts at 1 and increases by 2.  This is also why a separate counter is needed,
			 * panelArrayCounter, as the panels stored in the array need to increase by 1
			 */
			NodeList allGroups = doc.getDocumentElement().getChildNodes();   // ALL CHILD NODES OF XML
			int numberOfGroups = allGroups.getLength();
			
			
			//read_Environment_Params(allGroups.item(1).getChildNodes());
			//read_Interaction_Params(allGroups.item(3).getChildNodes());
			//read_Stroma_Params(allGroups.item(5).getChildNodes());
			//read_LTin_Params(allGroups.item(7).getChildNodes());
			read_LTi_Params(allGroups.item(9).getChildNodes());
			read_Chemokine_Params(allGroups.item(11).getChildNodes());
			read_VCAM_Params(allGroups.item(13).getChildNodes());
			read_Knockout_Params(allGroups.item(15).getChildNodes());
			//read_Output_Control(allGroups.item(17).getChildNodes());
			
			//for(int i=1;i<numberOfGroups-2;i=i+2)
			//{
				// NOW COMPLETE THIS TAB WITH THE PARAMETERS
				//NodeList groupChildNodes = allGroups.item(i).getChildNodes();                  // GROUP CHILD NODES
				
				//for(int j=1;j<groupChildNodes.getLength();j=j+2)
				//{
				//	System.out.println(groupChildNodes.item(j).getNodeName());
				//}
			//}
			
		}
		catch (SAXParseException err) 
		{
			System.out.println ("** Parsing error" + ", line " 
				+ err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());

		}
		catch (SAXException e) 
		{
			Exception x = e.getException ();
			((x == null) ? e : x).printStackTrace ();
		}
		// catch all exceptions
		catch (Throwable t) 
		{
			t.printStackTrace ();
		}
				
		
		
		
		
		
		
		
		/*try
		{	
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc;

			// USE THESE THREE LINES IF READING FROM THE LOCAL PARAMETER FILE!
			//if(!this.webRun)
			if(runDescription != null && paramFilePath!=null && runFilePath!=null)
			{
				// make a directory for all of the output files
				this.filePath = runFilePath;
				this.description = runDescription;
				new File(this.filePath+"/"+this.description).mkdir();
				doc = docBuilder.parse(new File(paramFilePath));
			}
			else
			{	
				System.out.println("HERE");
				
				// USE THESE THREE IF READING FROM THE WEB SITE
				
				URL url = new URL("http://www-users.cs.york.ac.uk/~kalden/frontiers/Frontiers_Parameters.xml");
				InputStream stream = url.openStream();
				doc = docBuilder.parse(stream);
			}
		
			// normalize text representation
			doc.getDocumentElement ().normalize ();

			// NOW READ IN THE PARAMETERS IN GROUPS
			read_Output_Control(doc);
			read_Knockout_Params(doc);
			read_VCAM_Params(doc);
			read_Chemokine_Params(doc);
			read_LTi_Params(doc);
			read_LTin_Params(doc);
			read_Stroma_Params(doc);
			read_Interaction_Params(doc);
			read_Environment_Params(doc);
			
		}
		catch (SAXParseException err) 
		{
			System.out.println ("** Parsing error" + ", line " 
				+ err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());

		}
		catch (SAXException e) 
		{
			Exception x = e.getException ();
			((x == null) ? e : x).printStackTrace ();
		}
		// catch all exceptions
		catch (Throwable t) 
		{
			t.printStackTrace ();
		}*/	

	}
	
		
	/**
	 * This method is used by the web service - where the user has changed the parameters, these cannot be
	 * put back into the XML file, so these are read from the form, and stored in the variables as if these 
	 * were read from the XML file
	 * @param boxes	ArrayList containing all the TextBoxes from the form, and the respective simulation variables
	 */
	public void adjustParameters(ArrayList<JTextField> boxes, ArrayList<JRadioButton> radios)
	{
		// READ ENVIRONMENT VARIABLES
		displayFlag = true;
		initialGridHeight = Double.parseDouble(boxes.get(1).getText());
		initialGridLength = Double.parseDouble(boxes.get(2).getText());
		upperGridHeight = Double.parseDouble(boxes.get(3).getText());
        upperGridLength = Double.parseDouble(boxes.get(4).getText());
		secondsPerStep = Integer.parseInt(boxes.get(5).getText());
        simulationTime = Integer.parseInt(boxes.get(6).getText());
        
        // INTERACTION
        thresholdBindProbability = Double.parseDouble(boxes.get(7).getText());
        
        // STROMA
        stromalCellDensity = Integer.parseInt(boxes.get(8).getText());
        percentStromaRETLigands = Double.parseDouble(boxes.get(9).getText());
        percentRETLigandNonStromal = Double.parseDouble(boxes.get(10).getText());
        numHoursRETLigandLToActive = Integer.parseInt(boxes.get(11).getText());
        probabilityLTinLTiRETLigands = Double.parseDouble(boxes.get(12).getText());
        imLToActiveTime = Integer.parseInt(boxes.get(13).getText());
        
        // LTin
        percentLTinfromFACStain = Double.parseDouble(boxes.get(14).getText());
        lTinInputTime = Integer.parseInt(boxes.get(15).getText());
        lTinInputRateGraphType = boxes.get(16).getText();
        lTinInputRateGraphConstant = Double.parseDouble(boxes.get(17).getText());
        
        // LTi
        percentLTifromFACStain = Double.parseDouble(boxes.get(18).getText());
        lTiInputDelayTime = Integer.parseInt(boxes.get(19).getText());
        lTiInputTime = Integer.parseInt(boxes.get(20).getText());
        lTiInputRateGraphType = boxes.get(21).getText();
        lTiInputRateGraphConstant = Double.parseDouble(boxes.get(22).getText());
        
        // CHEMOKINE EXPRESSION PARAMETERS
        chemoThreshold = Double.parseDouble(boxes.get(23).getText());
        chemoUpperLinearAdjust = Double.parseDouble(boxes.get(24).getText());
        chemoLowerLinearAdjust = Double.parseDouble(boxes.get(25).getText());
        
        // VCAM PARAMETERS
        //vcamInitialProbability = Double.parseDouble(boxes.get(19).getText());
        maxVCAMeffectProbabilityCutoff = Double.parseDouble(boxes.get(26).getText());
        vcamSlope = Double.parseDouble(boxes.get(27).getText());
        
        // KNOCKOUTS
        retLigandKnockOut = radios.get(0).isSelected();
        chemoKnockOut = radios.get(1).isSelected();
        vcamKnockOut = radios.get(2).isSelected();
        
        // SET THE TRACKING VARIABLES TO OFF AS NOT READ IN
        cellTrackingEnabled = false;
        stepBystepTrackingImages = false;
        trackingHourRanges= null;
        twelveHourSnaps = false;
        generateLToStats = false;
        
	}
	
	
  	/**
	 * Test Function for this Class
	 * @param args
	 */
	public static void main(String[] args)
	{	
		//String filePath = args[1];
		//String runDescription = args[0];
		String filePath = "/home/kieran/Desktop/";
		String runDescription = "noDisplayTest";
		String paramFilePath = "/home/kieran/workspace/parameters-metadata_MASTER.xml";
		SimParameters sp = new SimParameters(runDescription,paramFilePath,filePath);
		
		
	}
}
