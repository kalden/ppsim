package ppatch_onepatch;

import java.awt.Color;
import java.io.File;
import java.util.ArrayList;
import java.util.StringTokenizer;

import ppatch_chemokine.ChemokineGrid;
import ppatch_environment.Setup_Stromal_Cell_Distribution;
import ppatch_environment.TractGrowth;
import ppatch_onepatch_cells.CellDivision;
import ppatch_onepatch_cells.CellInputControl;
import ppatch_stats.CellExpression;
import ppatch_stats.CellTracking;
import ppatch_stats.LToStatistics;
import ppatch_stats.PatchStatistics;
import ppatch_stats.TrackImaging;
import sim.util.*;
import ec.util.MersenneTwisterFast;
import sim.display.Display2D;
import sim.engine.*;
import sim.field.continuous.*;
import sim.field.grid.ObjectGrid2D;
import ycil_XMLUtilities.XMLFileUtilities;



/**
 * The main class for the Peyer's Patch Simulation.The simulation begins when the start() method of this class is run.
 * Contains all the attributes used throughout the simulation, as well as the state of the simulation
 * at any given time.
 * 
 * @author Kieran Alden
 *
 */
public class PPatchSim extends SimState
{	
	
	/***************************************************************************************************
	 * ***************************************************************************************************
	 * ***************************************************************************************************
	 * 								PLATFORM PARAMETERS - FOR RUNNING & DATA COLLECTION 
	 */
	
	/***************************************************************************************************
	 * 								THE PARAMETERS FROM THE XML FILE & PROGRAM PARAMS
	 */
	
	/**
	 * <a name = "simParams"></a>
	 * <b>Description:<br></b> 
	 * Object of the class which reads the simulation parameters from the XML file, making these available in the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Simulation Parameters
	 * <br><br>
	 */
	public static SimParameters2 simParams;

	
	/**
	 * <a name = "display"></a>
	 * <b>Description:<br></b> 
	 * If run with a GUI, this forms the window used to display the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Graphical Display
	 * <br><br>
	 */
	public static Display2D display;
	
	/**
	 * <a name = "filePath"></a>
	 * <b>Description:<br></b> 
	 * The directory path where the parameter file is and output will be stored
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Full File Path
	 * <br><br>
	 */
	public static String filePath;
	
	/**
	 * <a name = "runDescription"></a>
	 * <b>Description:<br></b> 
	 * A description of this run
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Description
	 * <br><br>
	 */
	public static String runDescription;
	
	/**
	 * <a name = "paramFilePath"></a>
	 * <b>Description:<br></b> 
	 * Full file path to parameter XML file
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * File Path
	 * <br><br>
	 */
	public static String paramFilePath;
	
	
	/***************************************************************************************************
	 * 												ENVIRONMENT 
	 */
	
	/**
	 * <a name = "tract"></a>
	 * <b>Description:<br></b> 
	 * A Continuous grid representing the small intestine tract being modelled
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * The Environment 
	 * <br><br>
	 */
	public Continuous2D tract = null;
	
	/**
	 * <a name = "currentGridHeight"></a>
	 * <b>Description:<br></b> 
	 * Stores the current height of the intestine tract at that particular timestep
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels
	 * <br><br>
	 */
	public static double currentGridHeight;
	
	/**
	 * <a name = "currentGridLength"></a>
	 * <b>Description:<br></b> 
	 * Stores the current length of the intestine tract at that particular timestep
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels
	 * <br><br>
	 */
	public static double currentGridLength;  
	
	/**
	 * <a name = "heightGrowth"></a>
	 * <b>Description:<br></b> 
	 * Calculated amount by which the tract will grow in height with each step
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels
	 * <br><br>
	 */
	public double heightGrowth;
	
	/**
	 * <a name = "lengthGrowth"></a>
	 * <b>Description:<br></b> 
	 * Calculated amount by which the tract will grow in length with each step
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Pixels
	 * <br><br>
	 */
	public double lengthGrowth;
	
	/**
	 * <a name = "tractSizeControl"></a>
	 * <b>Description:<br></b> 
	 * Object used to control the growth of the tract environment over the run
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Object
	 * <br><br>
	 */
	public TractGrowth tractSizeControl;
	
	/**
	 * <a name = "stromalCellEnvironment"></a>
	 * <b>Description:<br></b> 
	 * Object used to set up the distribution of LTo and RLNonStromal Cells in the environment
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Object
	 * <br><br>
	 */
	public Setup_Stromal_Cell_Distribution stromalCellEnvironment; 
	
	/**
	 * <a name = "chemoGrid"></a>
	 * <b>Description:<br></b> 
	 * Value grid used when calculating the chemokine diffusion.  Note that no values are actually stored in the grid, the grid exists to make processing easier.
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Object
	 * <br><br>
	 */
	public ChemokineGrid chemoGrid;        // not in param file
	
	/**
	 * <a name = "cellColours"></a>
	 * <b>Description:<br></b> 
	 * Colour array be used to match a colour to the colourCode variable, representing cell state
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * ArrayList
	 * <br><br>
	 */
    public static ArrayList<Color> cellColours = new ArrayList<Color>();

    /**
	 * <a name = "lToGrid"></a>
	 * <b>Description:<br></b> 
	 * Grid of 1x1 squares which overlays the Continuous2D grid
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Grid Space
	 * <br><br>
	 */
	public ObjectGrid2D lToGrid;
	
	
	/***************************************************************************************************
	 * 												CELLS 
	 */
	
	/**
	 * <a name = "trackedCells_Close"></a>
	 * <b>Description:<br></b> 
	 * Collection which stores all the LTi/LTin Cell Objects that are being tracked close from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Data Store
	 * <br><br>
	 */
	public static Bag trackedCells_Close;
	
	/**
	 * <a name = "trackedCells_Close"></a>
	 * <b>Description:<br></b> 
	 * Collection which stores all the LTi/LTin Cell Objects that are being tracked away from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Data Store
	 * <br><br>
	 */
	public static Bag trackedCells_Away;
	
	/**
	 * <a name = "numActiveLTo"></a>
	 * <b>Description:<br></b> 
	 * Stores the number of LTo cells calculated from the percentage of stromal cells that are active in the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Number of Cells
	 * <br><br>
	 */
	public static int numActiveLTo;
	
	/**
	 * <a name = "ltoCellsBag"></a>
	 * <b>Description:<br></b> 
	 * Collection which stores all the LTo Cell Objects in order that there locations can be easily accessed
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Data Storage (Bag)
	 * <br><br>
	 */
	public static Bag ltoCellsBag;
	
	/**
	 * <a name = "activelToCellsBag"></a>
	 * <b>Description:<br></b> 
	 * Collection which stores all the stromal Cell Objects which are active in order that there locations can be easily accessed
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Data Storage (Bag)
	 * <br><br>
	 */
	public static Bag activelToCellsBag;
	
	/**
	 * <a name = "RETLigandNonStromalCellsBag"></a>
	 * <b>Description:<br></b> 
	 * Collection which stores all the Decoy Cell Objects in order that there locations can be easily accessed
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Data Storage (Bag)
	 * <br><br>
	 */
	public static Bag RETLigandNonStromalCellsBag;
	
	public ArrayList<String> cellTypeList;
	
	
	/***************************************************************************************************
	 * 												STATISTICS 
	 */
	
	/**
	 * <a name = "numPatchesMoreThan25LTi"></a>
	 * <b>Description:<br></b> 
	 * Used to count the number of patches > 25 LTi at the end of the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Count
	 * <br><br>
	 */
	public int numPatchesMoreThan25LTi = 0;
	
	/**
	 * <a name = "numPatchesGreaterThan400Microns"></a>
	 * <b>Description:<br></b> 
	 * Used to count the number of patches > 400 Microns at the end of the simulation
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Count
	 * <br><br>
	 */
	public int numPatchesGreaterThan400Microns = 0;
	
	
	/**
	 * <a name = "stats"></a>
	 * <b>Description:<br></b> 
	 * Object used to generate the patch statistics at the end of the run
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Object of PatchStatistics
	 * <br><br>
	 */
	public PatchStatistics stats;
	
	/**
	 * <a name = "ltoStats"></a>
	 * <b>Description:<br></b> 
	 * Object used to generate the LTo statistics at the end of the run
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Object of LToStatistics
	 * <br><br>
	 */
	public LToStatistics ltoStats;
	
	/**
	 * <a name = "cellTrackStats"></a>
	 * <b>Description:<br></b> 
	 * Object used to perform the cell tracking during the run 
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Object of CellTracking
	 * <br><br>
	 */
	public CellTracking cellTrackStats;
	
	public CellExpression cellexp;
	
	/**
	 * <a name = "captureTrackImage"></a>
	 * <b>Description:<br></b> 
	 * Object used to take snapshots of the tract (if required) during and at the end of the run 
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Object of TrackImaging
	 * <br><br>
	 */
	public TrackImaging captureTrackImage;
	

	/**
	 * <a name = "ltoCellDivision"></a>
	 * <b>Description:<br></b> 
	 * Object used to perform the LTo cell division where required
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Onject of CellDivision
	 * <br><br>
	 */
	public CellDivision ltoCellDivision;
	
	/**
	 * <a name = "averageLength"></a>
	 * <b>Description:<br></b> 
	 * Average length of cells tracked far from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Length Statistic
	 * <br><br>
	 */
	public double averageLength;
	
	/**
	 * <a name = "averageVelocity"></a>
	 * <b>Description:<br></b> 
	 * Average velocity of cells tracked far from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Velocity Statistic
	 * <br><br>
	 */
	public double averageVelocity;
	
	/**
	 * <a name = "averageDisplacement"></a>
	 * <b>Description:<br></b> 
	 * Average displacement of cells tracked far from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Displacement Statisitic
	 * <br><br>
	 */
	public double averageDisplacement;
	
	/**
	 * <a name = "averageDisplacementRate"></a>
	 * <b>Description:<br></b> 
	 * Average displacement rate of cells tracked far from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Displacement Statistic
	 * <br><br>
	 */
	public double averageDisplacementRate;
	
	/**
	 * <a name = "averageMeanderingIndex"></a>
	 * <b>Description:<br></b> 
	 * Average meandering index of cells tracked far from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Meandering Statistic
	 * <br><br>
	 */
	public double averageMeanderingIndex;
	
	/**
	 * <a name = "averageLengthNear"></a>
	 * <b>Description:<br></b> 
	 * Average length of cells tracked close from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Length Statistic
	 * <br><br>
	 */
	public double averageLengthNear;
	
	/**
	 * <a name = "averageVelocityNear"></a>
	 * <b>Description:<br></b> 
	 * Average velocity of cells tracked close from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Length Statistic
	 * <br><br>
	 */
	public double averageVelocityNear;
	
	/**
	 * <a name = "averageDisplacementNear"></a>
	 * <b>Description:<br></b> 
	 * Average displacement of cells tracked close from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Velocity Statistic
	 * <br><br>
	 */
	public double averageDisplacementNear;
	
	/**
	 * <a name = "averageDisplacementRateNear"></a>
	 * <b>Description:<br></b> 
	 * Average displacement rate of cells tracked close from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Displacement Statistic
	 * <br><br>
	 */
	public double averageDisplacementRateNear;
	
	/**
	 * <a name = "averageMeanderingIndexNear"></a>
	 * <b>Description:<br></b> 
	 * Average meandering index of cells tracked close from a patch
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Average Meandering Statistic
	 * <br><br>
	 */
	public double averageMeanderingIndexNear;
	
	/**
	 * <a name = "velocities"></a>
	 * <b>Description:<br></b> 
	 * Array used to hold the velocities of cells tracked away from a patch - used to calculate variance
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Array
	 * <br><br>
	 */
	public double[] velocities;
	
	/**
	 * <a name = "lengths"></a>
	 * <b>Description:<br></b> 
	 * Array used to hold the lengths of cells tracked away from a patch - used to calculate variance
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Array
	 * <br><br>
	 */
	public double[] lengths;
	
	/**
	 * <a name = "displacements"></a>
	 * <b>Description:<br></b> 
	 * Array used to hold the displacement of cells tracked away from a patch - used to calculate variance
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Array
	 * <br><br>
	 */
	public double[] displacements;
	
	/**
	 * <a name = "velocitiesNear"></a>
	 * <b>Description:<br></b> 
	 * Array used to hold the velocities of cells tracked close to a patch - used to calculate variance
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Array
	 * <br><br>
	 */
	public double[] velocitiesNear;
	
	/**
	 * <a name = "lengthsNear"></a>
	 * <b>Description:<br></b> 
	 * Array used to hold the lengths of cells tracked close to a patch - used to calculate variance
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Array
	 * <br><br>
	 */
	public double[] lengthsNear;
	
	/**
	 * <a name = "displacementsNear"></a>
	 * <b>Description:<br></b> 
	 * Array used to hold the displacements of cells tracked close to a patch - used to calculate variance
	 * <br><br>
	 * <b>Restrictions:<br></b>
	 * None
	 * <br><br>
	 * <b>Units & Representation:<br></b>
	 * Array
	 * <br><br>
	 */
	public double[] displacementsNear;
	
	public Bag allLTis = new Bag();
	
	/***************************************************************************************************
	 * 												CLASS METHODS
	 */
	
	
	/**
	 * @param seed
	 */
	public PPatchSim(long seed)
	{
		super(new MersenneTwisterFast(seed),new Schedule());
	}
	
	public PPatchSim(long seed, SimParameters2 sp,String[] args)
	{
		
		super(new MersenneTwisterFast(seed),new Schedule());
		this.simParams = sp;
		
		// START THE RUN
		doLoop(PPatchSim.class,args);
	}
	
	/**
	 * Begins the simulation by calling doLoop to run the start method until the simulation exits
	 * @param args
	 */
	public static void main(String[] args)
	{
			
		// Set up the simulation display & console
		// Check whether running from the command line or eclipse
		if(args.length > 0)
		{
			// Command line run (arguments should have been supplied!)
			runDescription = args[0];
			paramFilePath = args[1];
			filePath = args[2];
		}
		else
		{
			// Assume Eclipse Run (though will enter here if command line and no args supplied)
			// Else an exception will be generated
			runDescription = "GRAPHTEST";
			paramFilePath = "/home/kieran/Dropbox/WorkingDocs/parameters-master.xml";
			filePath = "/home/kieran/Desktop/";
		}
		
		// remove comments from try loop when turning the logger back on
		//try
		//{
			new File(filePath+"/"+runDescription).mkdir();
			
			if(new File(filePath+"/"+runDescription).isDirectory())
			{
				// Set up the logger
				// KA 020914 - highlighted logger out to test Steph's work
				//YCIL_Logger.setup(filePath+"/"+runDescription);
				//YCIL_Logger.logger.setLevel(Level.INFO);
				doLoop(PPatchSim.class,args);
			}
			else
			{
				System.out.println("Cannot Initialise Simulation or Logger: Results Directory Not Found");
			}
		//}
		//catch(IOException e)
		//{
			//e.printStackTrace();
		//}
		
		
	}
	
	/**
	 * Ends the simulation after the 72 hour period - taking the final snapshot if image output is required
	 */
	public void finish()
	{	
		super.finish();
		
		try
		{
			// draw the graphs if this is a webrun
			//if(simParams.webRun)
			//{
				//PatchStatsGraph patches = new PatchStatsGraph();
				//patches.drawStatsWindow(this);
			//}
			
			if(!simParams.webRun)
			{
				
				// Take the end of run snapshots if necessary / enabled
				if(simParams.twelveHourSnaps)
				{
					captureTrackImage.takeEndOfRunSnapshots(this);
				}
			}
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		//System.exit(0);
		
	}
	
	/**
	 * Begins the simulation run.  Calls are made to the required methods to initialise the environment, add the cells, deal with tract 
	 * growth etc
	 */
	public void start()
	{	
		if(this.simParams==null)				// launched without a GUI OR SA Wrapper
		{
			XMLFileUtilities.readSettingsFile(paramFilePath);
			this.simParams = new SimParameters2();
			//this.simParams.displayFlag = false;
		}
	
		// A - SET UP THE REQUIRED VARIABLES
		
		// Set the first tracking range
		if(!simParams.trackingHourRanges.isEmpty())
		{
			StringTokenizer st = new StringTokenizer(this.simParams.trackingHourRanges.get(0),"-");
			this.simParams.trackingSnapStartHr = Integer.parseInt(st.nextToken());
			this.simParams.trackingSnapEndHr = Integer.parseInt(st.nextToken());
			this.simParams.trackingHourRanges.remove(0);
		}
		
		// 1: Populate the colour array for use in the display class (If GUI on)
		this.popColourArray();
		
		// 2: Declare the cell storage bags
		ltoCellsBag = new Bag();
		activelToCellsBag = new Bag();
		RETLigandNonStromalCellsBag = new Bag();
		
		// 3: Set up the grid
		tract = new Continuous2D(6.0,simParams.initialGridLength,simParams.initialGridHeight);
		// Store the current grid height & length as these will change throughout the simulation
		currentGridLength = simParams.initialGridLength;
		currentGridHeight = simParams.initialGridHeight;
		
		// 4: Calculate the upper and lowe cell speed bounds for this run (from the number of seconds represented by each step)
		simParams.cellSpeedLowBound = (simParams.cellSpeedMinLowBound/60)*simParams.secondsPerStep;
		simParams.cellSpeedUpBound = (simParams.cellSpeedMinUpBound/60)*simParams.secondsPerStep;
		
		// 5. Calculate the environment growth rate per minute. 
		// As growth time is in hours (to standardise with all other variables), needs converting to seconds
		heightGrowth = (simParams.upperGridHeight-simParams.initialGridHeight)/((simParams.growthTime*60)*60);
		lengthGrowth = (simParams.upperGridLength-simParams.initialGridLength)/((simParams.growthTime*60)*60);
		
		// 6. Set up grid to use to calculate chemokine diffusion
		chemoGrid = new ChemokineGrid(simParams.initialGridLength,simParams.initialGridHeight);
		
		// B: Start the simulation back functions
		super.start();
		
		// C: Place the LTo and Decoy cells on the stroma
		stromalCellEnvironment = new Setup_Stromal_Cell_Distribution(this,simParams);
				
		// D: Initialise Cell Input Control
		CellInputControl hemCells = new CellInputControl(schedule,simParams,this);	
		hemCells.setStopper(schedule.scheduleRepeating(hemCells));
		
		// E: Initialise Cell Division Control 
		ltoCellDivision = new CellDivision(simParams);
		
		// F: Begin management of the environment (and growth over the development period)
		tractSizeControl = new TractGrowth(simParams);
	
		// KA MAR'14 - TEST OF GENERIC OUTPUT
		this.cellTypeList = new ArrayList<String>() {{ add("LTi");add("LTin"); }};
		
		
		// G: Begin cell tracking if enabled - tracks cells over a set period, captures images of the tract etc
		if(simParams.cellTrackingEnabled)
		{
			cellTrackStats = new CellTracking(simParams,this);
			cellTrackStats.setStopper(schedule.scheduleRepeating(cellTrackStats));
		}
		
		// H: Begin cell expression
		cellexp = new CellExpression(this,simParams);
		
		
			
		// H: Setup stat generation if enabled (and not a web run)
		if(!simParams.webRun)
		{		
			if(simParams.generateLToStats)
			{
				ltoStats = new LToStatistics(simParams);
				ltoStats.setStopper(schedule.scheduleRepeating(ltoStats));
				
				this.stats = new PatchStatistics(simParams);
				stats.setStopper(schedule.scheduleRepeating(stats));
			}
			
			
			if(simParams.stepBystepTrackingImages || simParams.twelveHourSnaps)
			{
				captureTrackImage = new TrackImaging(simParams);
			}
		}
		
		
		
		
	}
	
	/**
	 * Creates an array of cell colours to use for the on screen display
	 */
	public void popColourArray()
    {
    	this.cellColours.add(Color.black);
    	
		this.cellColours.add(Color.LIGHT_GRAY);   // lighter blue
    	this.cellColours.add(Color.LIGHT_GRAY);   // lighter blue
    	this.cellColours.add(Color.LIGHT_GRAY);   // lighter blue
		
    	// LTIN COLOURS
    	this.cellColours.add(new Color(255,0,0));   // red
    	this.cellColours.add(new Color(255,165,0));   // orange
    	this.cellColours.add(Color.orange);
    	    	
    	// LTI COLOURS
    	this.cellColours.add(new Color(0,255,0));   // green
    	this.cellColours.add(new Color(0,255,0));   // green
    	
    	// red in twice as LTi changes state, yet no need to demonstrate this
    	this.cellColours.add(Color.black);
    }
	
	
}
