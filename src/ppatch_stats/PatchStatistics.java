package ppatch_stats;

import java.io.FileWriter;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import ppatch_onepatch_cells.Cells;
import ppatch_onepatch_cells.LTi;
import ppatch_onepatch_cells.LTo;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.util.Bag;
import sim.util.Double2D;

/**
 * Class to produce statistics on PP location, size etc at the end of the simulation
 * @author kieran
 *
 */
public class PatchStatistics implements Steppable,Stoppable
{
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;
	
	/**
	 * Writer to output just the LTi cells that are in a patch to a CSV file
	 */
	public FileWriter patchWriter;
	
	/**
	 * Writer to output the positions of all LTi cells to a CSV file
	 */
	public FileWriter patchWriter2;
	
	/**
	 * Flag to show if this class has been stopped (when no longer needed)
	 */
	private Stoppable stopper = null;
	
	/**
	 * Method to change the value of the stopper
	 * @param stopper	Whether the class should be stopped or not
	 */
    public void setStopper(Stoppable stopper)   {this.stopper = stopper;}
    
    /**
     * Method to stop the class where necessary
     */
    public void stop(){stopper.stop();}

	/**
	 * Initialise the class and writer
	 * 
	 * @param sp	The current simulation parameters
	 */
	public PatchStatistics(SimParameters2 sp)
	{
		this.simParams = sp;
	}
	
	public void outputPatchStats(PPatchSim ppsim)
	{
		try
		{
			patchWriter = new FileWriter(PPatchSim.filePath+PPatchSim.runDescription+"/patchStats_Hr"+simParams.nextPatchOutputHour+".csv");
			// add the first column headings
			patchWriter.append("LTi_X,LTi_Y\n");
			patchWriter2 = new FileWriter(PPatchSim.filePath+PPatchSim.runDescription+"/patchStatsAll_Hr"+simParams.nextPatchOutputHour+".csv");
			patchWriter2.append("LTi_X,LTi_Y\n");
			
			this.outputLTiPositions(ppsim);
			
			patchWriter.close();
			patchWriter2.close();
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Gathers the stats for each timestep
	 */
	public void step(final SimState state)
	{
		PPatchSim ppsim = (PPatchSim)state;
		
		// while the simulation is running
		if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.simulationTime*60)*60))
		{
			if(ppsim.schedule.getSteps()*simParams.secondsPerStep == ((simParams.nextPatchOutputHour*60)*60))
			{	
				// OUTPUT THE LTO AND LTI POSITIONS AT THIS TIMEPOINT
				this.outputPatchStats(ppsim);
				
				// NOW THIS OUTPUT TIME HAS ENDED, PREPARE FOR THE NEXT HOUR (IF APPLICABLE)
				// delete this hour from those being done
				simParams.patchStatHours.remove(0);
				// set the next range (if there is one)
				if(!simParams.patchStatHours.isEmpty())
				{
					simParams.nextPatchOutputHour = simParams.patchStatHours.get(0);
					
				}
				else
				{
					// Register the end hour - thus when the simulation ends and the produce stat function is called, 
					// the file will have an hour to be labelled with
					simParams.nextPatchOutputHour = simParams.simulationTime;
				}
			}
		}
		else
		{
			// Simulation has ended - output the end stats
			this.outputPatchStats(ppsim);
			
			this.stop();
		}
	}

	
	public void outputLTiPositions(PPatchSim ppsim)
	{
		for(int i=0;i<ppsim.allLTis.size();i++)
		{
			LTi ltiCell = (LTi) ppsim.allLTis.get(i);
			try
			{
				// find out what is around the LTi cell
				Bag nearCells = ppsim.tract.getObjectsExactlyWithinDistance(ltiCell.agentLocation, simParams.HCELL_DIAMETER*2, true);
				Cells temp = null;
				Boolean ltiFound = false;
				
				if( nearCells != null )
				{
					for( int j = 0 ; j < nearCells.numObjs && !ltiFound; j++ )
					{
						temp = (Cells)nearCells.get(j);
						// check cell is not null, not itself, is an LTi
						if(temp!=null && temp!=ltiCell && temp instanceof LTi)
						{
							// there is an LTi near, so it is possible that it could be in a patch
							// however, now check that there is an LTo within a range
							Boolean ltoFound = false;
							Cells ltoSeek = null;
							
							Bag nearCells2 = ppsim.tract.getObjectsExactlyWithinDistance(ltiCell.agentLocation, simParams.HCELL_DIAMETER*4, true);
							
							for( int k=0;k<nearCells2.numObjs && !ltoFound;k++)
							{
								ltoSeek = (Cells)nearCells2.get(k);
							
								if(ltoSeek!=null && ltoSeek instanceof LTo && temp.cellState>0)
								{
									patchWriter.append(ltiCell.agentLocation.x+","+ltiCell.agentLocation.y+"\n");
									ltoFound=true;
									ltiFound=true;
								}
								
							}
							
						}
					}
				}
				patchWriter2.append(ltiCell.agentLocation.x+","+ltiCell.agentLocation.y+"\n");
			}
			catch(Exception e)
			{
				e.printStackTrace();
			}
		}	
	}
		
}
