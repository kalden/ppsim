package ppatch_stats;

import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.StringTokenizer;

/**
 * Class for producing SPARTAN compatible simulation data (CSV files) from a collection of objects
 * 
 * @author Kieran Alden
 *
 */
public class CSVFileOutput 
{
	/**
	 * Path to where the simulation output should be stored
	 */
	private String filePath;
	
	/**
	 * Name to give to the results file
	 */
	private String fileName;
	
	/**
	 * CSV headers, in an arraylist (simulation responses to be analysed in SPARTAN)
	 */
	private ArrayList<String> headers;
	
	/**
	 * Output stream for Java 7
	 */
	//public OutputStream fout;
	
	/**
	 * Output Stream for Java 6
	 */
	public FileWriter simOutputWriter;
	
	/**
	 * Constructor: Store path to directory where output is to be stored, file name, and the array of simulation
	 * measures being examined in SPARTAN
	 * 
	 * @param fp	Path to directory storing results
	 * @param fname	Name to give the results file
	 * @param head	Arraylist of simulation output measures that are being analysed in SPARTAN
	 */
	public CSVFileOutput(String fp, String fname, ArrayList<String> head)
	{
		this.filePath = fp;
		this.fileName = fname;
		this.headers = head;
		
		try
		{
			this.simOutputWriter = new FileWriter(filePath+"/"+fileName);
		}
		catch(IOException e)
		{
			System.out.println("I/O Error: "+e);
		}
	}
	
	/**
	 * Takes a collection of agents in the simulation and produces SPARTAN compatible output based on this and the simulation 
	 * response measures provided previously. 
	 * 
	 * @param simObjects	A collection of simulation agents
	 */
	public void processSimulationResponses(Collection<Object> simObjects)
	{
		// Firstly write the headers to the file, appended with the object type (this is output as a sanity check)
		this.writeCSVFileHeader();
		
		Iterator<Object> agents = simObjects.iterator();
		while(agents.hasNext())
		{
			// Firstly as a sanity check we output the class of object, so we know in the results what we are dealing with
			Object simAgent = agents.next();
			this.writeResponseToStream(simAgent.getClass().getName(),",");
			
			// Now we can access the required responses - taking these from the arraylist of required measures
			Iterator<String> it = this.headers.iterator();
			while(it.hasNext())
			{
				// Firstly store the measure
				String measure = it.next();
				// Now work out what the separator should be - if the last measure, this will be a new line
				String sep;
				if(it.hasNext())
					sep = ",";
				else
					sep = "\n";
				
				// Now to determine: Are we dealing with a String, double, etc parameter, or an object. If an object, there will be 
				// a period as we need an attribute of that object (consider Double2D, we may want the x coordinate. So detect
				// the dot
				if(!measure.contains("."))
				{
					this.writeResponseToStream(this.returnSimMeasure(measure, "",simAgent),sep);
				}
				else
				{
					// Now to split the string on the dot
					StringTokenizer st = new StringTokenizer(measure, ".");
					// Now we can use the function sending the name of the object and the property we require
					this.writeResponseToStream(this.returnSimMeasure(st.nextToken(),st.nextToken(),simAgent),sep);
				}
				
			}				
		}
		
		this.closeStream();
	}
	
	/**
	 * For a given object, runs the 'getter' in that class to return the object value. Can also deal with compound objects where
	 * an attribute of that object is required (e.g. consider a Double2D, and you want just the X coordinate). Uses reflection to
	 * achieve this. Note that as reflection only looks at runtime classes, and the getter may well be in an object super class, 
	 * the exception that the getter was not found is caught, and the method then searches through the class hierarchy to see if it 
	 * can be recovered
	 * 
	 * @param simMeasureName	The name of the attribute (simulation response) of the object to write to file
	 * @param simMeasureProperty	Where applicable, the name of the property of that measure. Blank if no properties (such as a double, string, int etc)
	 * @param simAgent	The agent for which the measure is being recovered
	 * @return	The value of this object - will be written to the simulation response CSV file
	 */
	public Object returnSimMeasure(String simMeasureName, String simMeasureProperty, Object simAgent)
	{	
		try 
		{
			if(simMeasureProperty.isEmpty())
			{
				// What we're going to do is call the getter of the response name
				Method method = simAgent.getClass().getDeclaredMethod("get"+simMeasureName);
				return(method.invoke(simAgent, null));
			}
			else
			{
				// This time we need the object of the variable for which we are after a subproperty
				// E.g. - if we wanted the X coordinate of a Double2D, this would be the Double2D object
				// Call the method to get the object for which we have to return the property
				// The getters are different in this case as they return a PROPERTY, not the object
				Method method = simAgent.getClass().getDeclaredMethod("get"+simMeasureName, simMeasureProperty.getClass());
				return(method.invoke(simAgent, simMeasureProperty));
					
			}
		}
		catch(NoSuchMethodException e)
		{
			// Now try the superclasses to determine if the method can be found higher up the hierarchy
			boolean found = false;
			Class extendedClass = simAgent.getClass().getSuperclass();
			Object simMeasureToReturn = null;
			while(!found && extendedClass!=null)
			{
				try
				{
					if(simMeasureProperty.isEmpty())
					{
						// NO METHOD FOUND HERE, SO TRY THE SUPER CLASS		
						Method method = simAgent.getClass().getSuperclass().getDeclaredMethod("get"+simMeasureName);
						simMeasureToReturn = method.invoke(simAgent, null);
						found = true;
					}
					else
					{
						Method method = simAgent.getClass().getSuperclass().getDeclaredMethod("get"+simMeasureName, simMeasureProperty.getClass());
						simMeasureToReturn = method.invoke(simAgent, simMeasureProperty);
						found = true;
					}
				}
				catch(NoSuchMethodException ex)
				{
					// Move on to the next class
					extendedClass = extendedClass.getSuperclass();
				}
				catch(InvocationTargetException ex)
				{
					ex.printStackTrace();
					return "Invocation Target Exception";
				}
				catch(IllegalAccessException ex)
				{
					ex.printStackTrace();
					return "Illegal Access Exception";
				}			
			}
			
			if(found)
				return(simMeasureToReturn);
			else
				return("Sim Measure Not Found");
			
		}
		catch(InvocationTargetException e)
		{
			e.printStackTrace();
			return "Problem";
		}
		catch(IllegalAccessException e)
		{
			e.printStackTrace();
			return "Problem";
		}
	}
	

	/**
	 * Write the CSV file header, containing output measures to be analysed in SPARTAN.
	 */
	public void writeCSVFileHeader()
	{
		try
		{
			// Firstly, the type of object, put in as a sanity check that the write output has been found
			this.simOutputWriter.append("AgentType,");
			// Java 7:
			//this.fout.write(("AgentType,").getBytes());
			
			// For loop used to iterate through until one is left in array (see -1), such that divider is not placed after last element in header
			for(int i=0;i<headers.size()-1;i++)
			{
				this.simOutputWriter.append(headers.get(i)+",");
				// Java 7:
				// this.fout.write((headers.get(i)+",").getBytes());
			}
						
			// Now write the last element and new line
			this.simOutputWriter.append((headers.get(headers.size()-1))+"\n");
			// Java 7
			// this.fout.write((headers.get(headers.size()-1)+"\n").getBytes());
		}
		catch(IOException e)
		{
			System.out.println("I/O Error: "+e);
		}
	}
	
	/**
	 * Writes a simulation response to file, with the desired CSV separator ("," or "\n")
	 * 
	 * @param response	The simulation response to be written to the file
	 * @param sep	The separator between fields in the CSV file (comma or new line)
	 */
	public void writeResponseToStream(Object response, String sep)
	{
		try
		{
			this.simOutputWriter.append(response.toString()+sep);
			// Java 7:
			// fout.write((response.toString()+sep).getBytes());
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Closes the output stream
	 */
	public void closeStream()
	{
		try
		{
			this.simOutputWriter.close();
			//this.fout.close();
		}
		catch(IOException e)
		{
			e.printStackTrace();
		}
		
	}
}
