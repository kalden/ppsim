package ppatch_stats;

import java.io.FileWriter;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import ppatch_onepatch_cells.Cells;
import ppatch_onepatch_cells.LTin;
import ppatch_onepatch_cells.LTo;
import ppatch_onepatch_cells.RLNonStromal;
import sim.util.Bag;

public class CellExpression 
{
	/**
	 * File writer for cells close to a forming patch
	 */
	public FileWriter cellExpressionWriter;
	
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;
	
	public CellExpression(PPatchSim ppsim, SimParameters2 sp)
	{
		this.simParams = sp;
		
	}
	
	
	public void generateExpressionStats(PPatchSim ppsim)
	{
		try
		{
			cellExpressionWriter = new FileWriter(ppsim.filePath+"/"+ppsim.runDescription+"/cellExpression_"+simParams.trackingSnapStartHr+"to"+simParams.trackingSnapEndHr+".csv");
			cellExpressionWriter.append("Cell Type,VCAM Expression, Chemokine Expression\n");
			
			
			// DO THE LTO CELLS FIRST
			
			for(int i=0;i<ppsim.activelToCellsBag.size();i++)
			{
				LTo ltoCell = (LTo) ppsim.activelToCellsBag.get(i);
				
				if(ltoCell.cellState>0)
				{
					cellExpressionWriter.append("LTo,");
					
					// VCAM Expression Level
					cellExpressionWriter.append(Integer.toString((int)(ltoCell.vcamAdhesionEffect.returnVCAMExpressionLevel()*100000))+",");
					
					// Chemokine Level
					cellExpressionWriter.append(Integer.toString((int)(ltoCell.chemoLinearAdjust*100000))+",");
				}
								
				cellExpressionWriter.append("\n");
			}
			
			// RET-LIGAND NON STROMAL - EXPRESSES VCAM BUT NOT CHEMOKINE
			
			for(int i=0;i<PPatchSim.RETLigandNonStromalCellsBag.size();i++)
			{
				RLNonStromal rlnsCell = (RLNonStromal) PPatchSim.RETLigandNonStromalCellsBag.get(i);
				
				cellExpressionWriter.append("RLNS,");
				
				// VCAM Expression Level
				cellExpressionWriter.append(Integer.toString((int)(rlnsCell.vcamAdhesionEffect.returnVCAMExpressionLevel()*100000))+",");
				
				// Chemokine Level - not expressed by RLNS
				cellExpressionWriter.append("0");
				
				cellExpressionWriter.append("\n");
			}
			
			
			
			
			// NOW DO THE LTIN AND LTI CELLS - THESE ARE IN TWO BAGS
			this.writeHematopoieticCellStats(PPatchSim.trackedCells_Close);
			this.writeHematopoieticCellStats(PPatchSim.trackedCells_Away);
			
			cellExpressionWriter.close();
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	
	
	
	public void writeHematopoieticCellStats(Bag cellBag)
	{
		try
		{
			for(int k=0;k<cellBag.size();k++)
			{
				Cells trackedCell = (Cells) cellBag.get(k);
				
				if(trackedCell instanceof LTin)
				{
					cellExpressionWriter.append("LTin,");
				}
				else
				{
					cellExpressionWriter.append("LTi,");
				}
				
				// LTIN AND LTI DO NOT EXPRESS EITHER VCAM OR CHEMOKINE, AND THUS THESE ARE OUTPUT AS ZERO
				cellExpressionWriter.append("0,");
				cellExpressionWriter.append("0");	
				
				cellExpressionWriter.append("\n");
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	

}
