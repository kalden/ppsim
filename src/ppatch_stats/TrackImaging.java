package ppatch_stats;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import ppatch_onepatch_cells.LTo;

/**
 * Takes snapshots of the environment for output and further analysis - at both the end of the simulation, at 12 hour timepoints if required,
 * and every step if timelapse imaging is required
 * 
 * @author kieran
 *
 */
public class TrackImaging 
{
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;
	
	/**
	 * Store a local copy of the simulation parameters
	 * 
	 * @param sp	The simulation parameters
	 */
	public TrackImaging(SimParameters2 sp)
	{
		this.simParams = sp;
	}

	/**
	 * Take a snap of the display at 12 hour intervals
	 * @param ppsim	The current simulation state
	 */
	public void takeDisplaySnaps(PPatchSim ppsim)
	{
		/*
		if(ppsim.schedule.getSteps()==1)
		{
			ppsim.display.takeTractSnap(simParams.filePath+simParams.description+"/Initial");
		}
		else if(ppsim.schedule.getSteps()*simParams.secondsPerStep==((12*60)*60))
		{
			ppsim.display.takeTractSnap(simParams.filePath+simParams.description+"/12Hours");
		}
		else if(ppsim.schedule.getSteps()*simParams.secondsPerStep==((24*60)*60))
		{
			ppsim.display.takeTractSnap(simParams.filePath+simParams.description+"/24Hours");
		}
		else if(ppsim.schedule.getSteps()*simParams.secondsPerStep==((36*60)*60))
		{
			ppsim.display.takeTractSnap(simParams.filePath+simParams.description+"/36Hours");
		}
		else if(ppsim.schedule.getSteps()*simParams.secondsPerStep==((48*60)*60))
		{
			ppsim.display.takeTractSnap(simParams.filePath+simParams.description+"/48Hours");
		}*/
	}
	
	/**
	 * Take a snapshot of the environment at the end of the run
	 * 
	 * @param ppsim	The current simulation state
	 */
	public void takeEndOfRunSnapshots(PPatchSim ppsim)
	{
		// SNAPSHOT OF TRACT AT END OF SIMULATION
		// take an end of simulation snapshot - if required by the tracking parameter
		/*
		ppsim.display.takeTractSnap(simParams.filePath+"/"+simParams.description+"/End Snapshot");
		*/
	}
	
	/**
	 * Images can also be taken every step if required - great for timelapse imaging
	 * 
	 * @param ppsim	The current simulation state
	 */
	public void timePeriodImaging(PPatchSim ppsim)
	{
		// IMAGING EVERY STEP FOR A SET PERIOD (IF ENABLED)
		// take images of the tract at every step from a start hour to finish hour for analysis in Volocity if required
		// start and end hours set as parameters in the xml file
		
		/*
		if(ppsim.schedule.getSteps()*simParams.secondsPerStep >= ((simParams.trackingSnapStartHr*60)*60) 
				&& ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.trackingSnapEndHr*60)*60))
		{
			ppsim.display.takeTractSnap(simParams.filePath+"/"+simParams.description+"/"+ppsim.schedule.getSteps());
		}
		*/
		
		
	}
	
	
}
