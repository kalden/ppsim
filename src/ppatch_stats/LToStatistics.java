package ppatch_stats;

import java.io.FileWriter;
import java.io.IOException;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import ppatch_onepatch_cells.LTo;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;

/**
 * Class to generate some statistics for each LTo in the simulation
 * 
 * @author kieran
 *
 */
public class LToStatistics implements Steppable,Stoppable
{
	
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;

	
		/**
	 * Writes the LTo statistics from the simulation to a csv results file
	 */
	public FileWriter lToEndStatsWriter;
	
	public FileWriter ltoCurrentWriter;
	
	/**
	 * Flag to show if this class has been stopped (when no longer needed)
	 */
	private Stoppable stopper = null;
	
	/**
	 * Method to change the value of the stopper
	 * @param stopper	Whether the class should be stopped or not
	 */
    public void setStopper(Stoppable stopper)   {this.stopper = stopper;}
    
    /**
     * Method to stop the class where necessary
     */
    public void stop(){stopper.stop();}
	
		
    /**
     * Initialise the class and set the required writers to produce the stats
     * 
     * @param sp	The current simulation parameters
     */
	public LToStatistics(SimParameters2 sp)
	{
		this.simParams = sp;
	}
	
	/**
	 * Generates the stats file - writing the key parameters to the file at each step
	 * @param ppsim	The current simulation state
	 */
	public void generateLToStats(PPatchSim ppsim)
	{
		try
		{
			for(int i=0;i<PPatchSim.ltoCellsBag.size();i++)
			{	
				LTo ltoCell = (LTo) PPatchSim.ltoCellsBag.get(i);
				
				if(ltoCell.cellState>0)
				{	
					// Firstly output the current timestep
					lToEndStatsWriter.append(Long.toString(ppsim.schedule.getSteps())+",");
				
					// Cell Number
					lToEndStatsWriter.append(Integer.toString(i)+",");
				
					// Position X
					lToEndStatsWriter.append(Double.toString(ltoCell.agentLocation.x)+",");
				
					// Position Y
					lToEndStatsWriter.append(Double.toString(ltoCell.agentLocation.y)+",");
				
					// now the state the cell is in
					lToEndStatsWriter.append(Integer.toString(ltoCell.cellState)+",");
				
					// VCAM Level
					lToEndStatsWriter.append(Double.toString(ltoCell.vcamAdhesionEffect.returnVCAMExpressionLevel())+",");
				
					// Chemokine Level
					lToEndStatsWriter.append(Double.toString(ltoCell.chemoLinearAdjust));
				
					lToEndStatsWriter.append("\n");
					
					
				}
				
			}
			lToEndStatsWriter.close();
			
		}
		catch(IOException e)
		{
			e.printStackTrace();
		} 	
	}
	
	public void addFinalStatValues(PPatchSim ppsim)
	{
		// go through each LTo in turn
		for(int i=0;i<PPatchSim.ltoCellsBag.size();i++)
		{	
			LTo ltoCell = (LTo) PPatchSim.ltoCellsBag.get(i);
			
			// update the final VCAM expression level stat
			ltoCell.endingVCAMExpressionLevel = ltoCell.vcamAdhesionEffect.returnVCAMExpressionLevel();
			
			// update the final chemokine level
			ltoCell.endingChemoLinearAdjust = ltoCell.chemoLinearAdjust; 
		}
		
		
		
	}
	
	public void generateStatsForExperimentalLToAtRunEnd(PPatchSim ppsim)
	{
		try
		{
			lToEndStatsWriter = new FileWriter(ppsim.filePath+"/"+ppsim.runDescription+"/ltoStats_End.csv");
			lToEndStatsWriter.append("LTo,ChemoStartVal,ChemoEndVal,VCAMStartVal,VCAMEndVal,TimePointLTinChange,TimePointLTiChange,TimePointMature\n");
			
			LTo ltoCell = (LTo) PPatchSim.ltoCellsBag.get(0);
			
			// Cell Number
			lToEndStatsWriter.append(Integer.toString(0)+",");
			
			// Starting Chemo Value
			lToEndStatsWriter.append(Double.toString(ltoCell.startingChemoLinearAdjust)+",");
			
			// Ending Chemo Value
			lToEndStatsWriter.append(Double.toString(ltoCell.endingChemoLinearAdjust)+",");
			
			// Starting VCAM Value
			if(ltoCell.startingVCAMExpressionLevel>simParams.maxVCAMeffectProbabilityCutoff)
				lToEndStatsWriter.append(Double.toString(simParams.maxVCAMeffectProbabilityCutoff)+",");
			else
				lToEndStatsWriter.append(Double.toString(ltoCell.startingVCAMExpressionLevel)+",");
			
			// Ending VCAM Value
			if(ltoCell.endingVCAMExpressionLevel>simParams.maxVCAMeffectProbabilityCutoff || ltoCell.startingVCAMExpressionLevel>= simParams.maxVCAMeffectProbabilityCutoff)
				lToEndStatsWriter.append(Double.toString(simParams.maxVCAMeffectProbabilityCutoff)+",");
			else
				lToEndStatsWriter.append(Double.toString(ltoCell.endingVCAMExpressionLevel)+",");
			
			//Timepoint LTin State Change
			lToEndStatsWriter.append(Double.toString(ltoCell.lTinContactStateChangeTimePoint)+",");
			
			//Timepoint LTi State Change
			lToEndStatsWriter.append(Double.toString(ltoCell.lTiContactStateChangeTimePoint)+",");
			
			// Timepoint Mature LTo
			lToEndStatsWriter.append(Double.toString(ltoCell.matureLToStateChangeTimePoint));
			
			lToEndStatsWriter.append("\n");
			
			this.lToEndStatsWriter.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void generateLToStatsAtRunEnd(PPatchSim ppsim)
	{
		try
		{
			for(int i=0;i<PPatchSim.ltoCellsBag.size();i++)
			{	
				LTo ltoCell = (LTo) PPatchSim.ltoCellsBag.get(0);
				
				// Cell Number
				lToEndStatsWriter.append(Integer.toString(0)+",");
				
				// Starting Chemo Value
				lToEndStatsWriter.append(Double.toString(ltoCell.startingChemoLinearAdjust)+",");
				
				// Ending Chemo Value
				lToEndStatsWriter.append(Double.toString(ltoCell.endingChemoLinearAdjust)+",");
				
				// Starting VCAM Value
				if(ltoCell.startingVCAMExpressionLevel>simParams.maxVCAMeffectProbabilityCutoff)
					lToEndStatsWriter.append(Double.toString(simParams.maxVCAMeffectProbabilityCutoff)+",");
				else
					lToEndStatsWriter.append(Double.toString(ltoCell.startingVCAMExpressionLevel)+",");
				
				// Ending VCAM Value
				if(ltoCell.endingVCAMExpressionLevel>simParams.maxVCAMeffectProbabilityCutoff || ltoCell.startingVCAMExpressionLevel>= simParams.maxVCAMeffectProbabilityCutoff)
					lToEndStatsWriter.append(Double.toString(simParams.maxVCAMeffectProbabilityCutoff)+",");
				else
					lToEndStatsWriter.append(Double.toString(ltoCell.endingVCAMExpressionLevel)+",");
				
				//Timepoint LTin State Change
				lToEndStatsWriter.append(Double.toString(ltoCell.lTinContactStateChangeTimePoint)+",");
				
				//Timepoint LTi State Change
				lToEndStatsWriter.append(Double.toString(ltoCell.lTiContactStateChangeTimePoint)+",");
				
				// Timepoint Mature LTo
				lToEndStatsWriter.append(Double.toString(ltoCell.matureLToStateChangeTimePoint));
				
				lToEndStatsWriter.append("\n");
				
				lToEndStatsWriter.close();
			}
		}
		catch(IOException e)
		{
			e.printStackTrace();
		} 	
		
		
		
	}
	
	public void outputCurrentLToStats(PPatchSim ppsim)
	{
		try
		{
			ltoCurrentWriter = new FileWriter(ppsim.filePath+ppsim.runDescription+"/ltoStats_Hr"+simParams.nextLToStatOutputHour+".csv");
			ltoCurrentWriter.append("LToNum,State,Chemo_Value,VCAM_Value\n");
			
			LTo ltoCell = (LTo) PPatchSim.ltoCellsBag.get(0);
				
			// Cell Number
			ltoCurrentWriter.append(Integer.toString(0)+",");
			
			// State
			ltoCurrentWriter.append(Integer.toString(ltoCell.cellState)+",");
				
			// Chemo Value
			ltoCurrentWriter.append(Double.toString(ltoCell.chemoLinearAdjust)+",");
			
			// VCAM Value	
			ltoCurrentWriter.append(Double.toString(ltoCell.vcamAdhesionEffect.returnVCAMExpressionLevel()));
			
			ltoCurrentWriter.append("\n");
				
			this.ltoCurrentWriter.close();	
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Gathers the stats for each timestep
	 */
	public void step(final SimState state)
	{
		PPatchSim ppsim = (PPatchSim)state;
		
		// while the simulation is running
		if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.simulationTime*60)*60))
		{	
			if(ppsim.schedule.getSteps()*simParams.secondsPerStep == ((simParams.nextLToStatOutputHour*60)*60))
			{	
				// OUTPUT THE CURRENT LTO STATS
				this.outputCurrentLToStats(ppsim);
				
				// NOW THIS OUTPUT TIME HAS ENDED, PREPARE FOR THE NEXT HOUR (IF APPLICABLE)
				// delete this hour from those being done
				simParams.ltoStatHours.remove(0);
				// set the next range (if there is one)
				if(!simParams.ltoStatHours.isEmpty())
				{
					simParams.nextLToStatOutputHour = simParams.ltoStatHours.get(0);
					
				}
			}
		}
		else		// simulation finished - update & output the stats
		{
			// update the finishing LTo stats where necessary
			this.addFinalStatValues(ppsim);
			this.generateStatsForExperimentalLToAtRunEnd(ppsim);
			
			this.stop();
		
		}
	}
}
