package ppatch_stats;

import java.io.*;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import ppatch_onepatch_cells.Cells;
import ppatch_onepatch_cells.LTin;
import ppatch_onepatch_cells.LTo;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.util.Bag;
import sim.util.Double2D;
import ycil_output.CSVFileWriter;

/**
 * Class to track cells for a set period in the simulation.  This can be done for a number of periods
 * @author kieran
 *
 */
public class CellTracking implements Steppable,Stoppable
{
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;
	
	/**
	 * File writer for cells close to a forming patch
	 */
	public FileWriter trackedCells_Close_Writer;
	
	/**
	 * File writer for cells away from a forming patch
	 */
	public FileWriter trackedCells_Away_Writer;
	
	/**
	 * File writer for the summary file, used in batch run situations, where the average of the displacement, length & velocity can then be calculated over
	 * multiple runs
	 */
	//public FileWriter summaryWriter;
	
	public FileWriter tableWriter; 
	
	
	/**
	 * Initilise the class and the bags in which the tracked cells are stored
	 * 
	 * @param sp	The simulation parameters used in this run
	 * @param ppsim	The current simulation state
	 */
	public CellTracking(SimParameters2 sp,PPatchSim ppsim)
	{
		this.simParams = sp;
		
		// Set up the bag that keeps track of the cells being tracked
		ppsim.trackedCells_Close = new Bag();
		ppsim.trackedCells_Away = new Bag();

	}
	
	
	/**
	 * Begin tracking at the required timepoint.  Where this occurs, cells that are in the far bag, yet are close enough to an LTo to be in the near bag,
	 * are moved as required
	 * 
	 * @param ppsim	The current simulation state
	 */
	public void beginTracking(PPatchSim ppsim)
	{		
		for(int k=0;k<ppsim.trackedCells_Away.size();k++)
		{
			Cells trackedCell = (Cells) ppsim.trackedCells_Away.get(k);
			
			if(findNearestLTo(trackedCell,ppsim)*4<=50)
			{
				ppsim.trackedCells_Away.remove(trackedCell);
				ppsim.trackedCells_Close.add(trackedCell);
				
			}
		}
	}
	
	/**
	 * Output cell tracking results at the end of the tracking period.  These are output as two CSV files - one for close and one for far
	 * 
	 * @param ppsim	The current simulation state
	 * @param cellsTracked	The cells tracked (either far or away)
	 * @param outputTrackStatsWriter	The writer which writes the file
	 */
	public void outputTrackCellsResults(PPatchSim ppsim,Bag cellsTracked,FileWriter outputTrackStatsWriter)
	{
		// Initialise the averages
		ppsim.averageDisplacement = 0;
		ppsim.averageLength = 0;
		ppsim.averageDisplacementRate = 0;
		ppsim.averageVelocity = 0;
		ppsim.averageMeanderingIndex = 0;
		
		
		// TURNED OFF THE 20 FOR THE MOMENT WHILE LOOKING AT STATS OVER TIME
		//if(cellsTracked.size()<20)
		//{
			ppsim.velocities = new double[cellsTracked.size()];
			ppsim.displacements = new double[cellsTracked.size()];
			ppsim.lengths = new double[cellsTracked.size()];
		//}
		//else
		//{
		//	ppsim.velocities = new double[20];
		//	ppsim.displacements = new double[20];
		//	ppsim.lengths = new double[20];
		//}
		
		// Now write out the tracked cells to the file
		
		try
		{
			// stop the output when k=20 so samples can be compared easily on U table
			// TURNED OFF ON 241111 WHILE CELLS TRACKED AWAY BEING EXAMINED IN METHOD OTHER THAN U TEST
			//for(int k=0;k<cellsTracked.size() && k<20;k++)
			for(int k=0;k<cellsTracked.size();k++)
			//for(int k=0;k<cellsTracked.size();k++)
			{
				Cells trackedCell = (Cells) cellsTracked.get(k);
				
				// check that the cell was tracked for an hour
				if(trackedCell.timeTracked == (3600/simParams.secondsPerStep))
				{
					// CALCULATE CELL DISPLACEMENT (TAKING INTO ACCOUNT THE CELL MAY HAVE ROLLED AROUND THE SCREEN)
					double adjuster = 0;
					double trackDisplacement = distanceBetweenTwoPoints(trackedCell.agentTrackEndLocation,trackedCell.agentTrackStartLocation,0);
				
					if(trackDisplacement>200)		// must have rolled around the screen, and is therefore incorrect
					{
						if(trackedCell.agentTrackEndLocation.y<trackedCell.agentTrackStartLocation.y)
							trackDisplacement = distanceBetweenTwoPoints(trackedCell.agentTrackEndLocation,trackedCell.agentTrackStartLocation,254);
						else
							trackDisplacement = distanceBetweenTwoPoints(trackedCell.agentTrackEndLocation,trackedCell.agentTrackStartLocation,-254);
					}
					
					// NOW THIS FUNCTION IS USED BOTH BY THE FILE WRITING AND WEB VERSIONS OF THE SIMULATION
					// SO ONLY WRITE TO FILE IF NOT ON THE WEB
					if(!simParams.webRun)
					{					
						if(trackedCell instanceof LTin)
						{
							outputTrackStatsWriter.append("LTin,");
						}
						else
						{
							outputTrackStatsWriter.append("LTi,");
						}
				
						outputTrackStatsWriter.append(Integer.toString(trackedCell.timeTracked)+",");
			
						// append cell state
						outputTrackStatsWriter.append(Integer.toString(trackedCell.cellState)+",");
				
						// append cell speed
						outputTrackStatsWriter.append(Double.toString(trackedCell.cellSpeed*4)+",");
			
						// append the locations of where the cell started
						outputTrackStatsWriter.append(Double.toString(trackedCell.agentTrackStartLocation.x)+",");
						outputTrackStatsWriter.append(Double.toString(trackedCell.agentTrackStartLocation.y)+",");
				
						// append the locations of where the cell ended up
						outputTrackStatsWriter.append(Double.toString(trackedCell.agentTrackEndLocation.x)+",");
						outputTrackStatsWriter.append(Double.toString(trackedCell.agentTrackEndLocation.y)+",");
				
						// TRACK LENGTH
						outputTrackStatsWriter.append(Double.toString((trackedCell.trackLength)*4)+",");

					
						// TRACK VELOCITY
						outputTrackStatsWriter.append(Double.toString(trackedCell.trackLength*4/60)+",");
						
							
						// OUTPUT DISPLACEMENT
						outputTrackStatsWriter.append(Double.toString(trackDisplacement*4)+",");
				
					
						// DISPLACEMENT RATE
						outputTrackStatsWriter.append(Double.toString(trackDisplacement*4/60)+",");
						// note always divided by 60 as after the displacement rate per minute
						
						// MEANDERING INDEX
						outputTrackStatsWriter.append(Double.toString(trackDisplacement/trackedCell.trackLength)+",");
					
						// CLOSEST LTO
						outputTrackStatsWriter.append(Double.toString(findNearestLTo(trackedCell,ppsim)*4)+",");
						outputTrackStatsWriter.append("\n");
					}
					
					// ADD TO THE ARRAYS USED TO DO MANN-WHITNEY
					ppsim.displacements[k] = trackDisplacement*4;
					ppsim.velocities[k] = trackedCell.trackLength*4/60;
					ppsim.lengths[k] = trackedCell.trackLength*4;
					
					// ADD TO THE STATS TO CALCULATE AVERAGES
					ppsim.averageLength += (trackedCell.trackLength*4);
					ppsim.averageVelocity += (trackedCell.trackLength*4)/60;
					ppsim.averageDisplacement += (trackDisplacement*4);
					ppsim.averageDisplacementRate += (trackDisplacement*4/60);
					ppsim.averageMeanderingIndex += (trackDisplacement/trackedCell.trackLength);
				}
			}
			
			// AGAIN SWITCHED OFF TO EXAMINE ALL CELLS TRACKED, NOT FIRST 20
			int divider = 0;
			//if(cellsTracked.size()>20)
			//	divider = 20;
			//else
				divider = cellsTracked.size();
			
			
			ppsim.averageDisplacement = ppsim.averageDisplacement/divider;
			ppsim.averageLength = ppsim.averageLength/divider;
			ppsim.averageDisplacementRate = ppsim.averageDisplacementRate/divider;
			ppsim.averageVelocity = ppsim.averageVelocity/divider;
			ppsim.averageMeanderingIndex = ppsim.averageMeanderingIndex/divider;
			
			
		}	
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	/**
	 * Calculates the distance between two coordinates
	 * 
	 * @param endloc	Where the cell has moved to
	 * @param startloc	Where the cell started
	 * @param adjuster	Adjuster applied to the calculation where the cell has rolled around the screen
	 * @return
	 */
	public double distanceBetweenTwoPoints(Double2D endloc, Double2D startloc, double adjuster)
	{
		return Math.sqrt(
				Math.pow(endloc.x - startloc.x,2)
				+
				Math.pow((endloc.y - startloc.y)+adjuster,2)
				);
	}
	
	/**
	 * Finds nearest LTo cell to the LTin/LTi
	 * 
	 * @param trackedCell	The cell being examined
	 * @param ppsim	The current simulation state
	 * @return	The distance to the nearest LTo
	 */
	public double findNearestLTo(Cells trackedCell,PPatchSim ppsim)
	{
		// now go through each LTo and work out where the nearest active LTo to a tracked cell is
		double closestLTo= Double.POSITIVE_INFINITY;
		double distance;
		
		for(int l=0;l<ppsim.ltoCellsBag.size();l++)
		{
			LTo ltoCell = (LTo)ppsim.ltoCellsBag.get(l);
	
			if(ltoCell.cellState>=1)
			{
				// calculate the distance from this cell to the LTo
				distance = (Math.sqrt((Math.pow(trackedCell.agentLocation.x-ltoCell.agentLocation.x,2)+Math.pow(trackedCell.agentLocation.y-ltoCell.agentLocation.y,2))));
				if(distance<closestLTo)
				{
					closestLTo = distance;
				}
			}
	
		}
		return closestLTo;
		
	}
	
	/**
	 * JAY's FUNCTION FOR CHECKING THE INPUT RATE SOLUTION IS WORKING
	 * @param ppsim
	 */
	public void generateDataTable(PPatchSim ppsim)
	{
		try
		{
			
			if(!simParams.webRun)
			{ 
				File f = new File(ppsim.filePath+"/PointsTable.csv");
				if(!f.exists())
				{
					tableWriter = new FileWriter(ppsim.filePath+"/PointsTable.csv",true);
					tableWriter.append("Time,LTin,LTi\n");
					tableWriter.append(ppsim.schedule.getSteps()*simParams.secondsPerStep+","+simParams.lTinCellularity+","+simParams.lTiCellularity+" \n");
				}
				else
				{
					tableWriter = new FileWriter(ppsim.filePath+"/PointsTable.csv",true);
				}
				tableWriter.append(ppsim.schedule.getSteps()*simParams.secondsPerStep+","+simParams.lTinCellularity+","+simParams.lTiCellularity+" \n");
				tableWriter.close();
			}
			
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
	}
	
	
	
	
	/**
	 * Generates a summary file at the run end showing averages of cell measures.  Not really used and may be removed
	 * @param ppsim
	 */
	public void generateStatsAtRunEnd(PPatchSim ppsim)
	{
		//try 
		//{	
			if(!simParams.webRun)
			{
				//File f = new File(simParams.filePath+"/summary.csv");
				//if(!f.exists())
				//{
					//summaryWriter = new FileWriter(simParams.filePath+"/summary_"+simParams.trackingSnapStartHr+"to"+simParams.trackingSnapEndHr+".csv",true);
					//summaryWriter.append("Run Description,Number Patches >400 Microns,Number Patches > 25 LTi,Average Length Close,Average Velocity Close,Average Displacement Close,Average Displacement Rate Close,Average Meandering Index Close,Average Length Away,Average Velocity Away,Average Displacement Away,Average Displacement Rate Away,Average Meandering Index Away\n");
				//}
				//else
				//{
					//summaryWriter = new FileWriter(simParams.filePath+"/summary.csv",true);
				//}
			
				//this.summaryWriter.append(simParams.description+","+ppsim.numPatchesGreaterThan400Microns+","+ppsim.numPatchesMoreThan25LTi+",");
			
			
				// OUTPUT THE CELL TRACKING STATS TO FILE
				//trackedCells_Close_Writer = new FileWriter(simParams.filePath+"/"+simParams.description+"/trackedCells_Close_"+simParams.trackingSnapStartHr+"to"+simParams.trackingSnapEndHr+".csv");
				//trackedCells_Close_Writer.append("Cell Type,Time Span,Cell State,Cell Speed,Cell Start Position X,Cell Start Position Y,Cell End Position X,Cell End Position Y,Length,Velocity,Displacement,Displacement Rate,Meandering Index,Nearest LTo Cell (microns)\n");
			
				//trackedCells_Away_Writer = new FileWriter(simParams.filePath+"/"+simParams.description+"/trackedCells_Away_"+simParams.trackingSnapStartHr+"to"+simParams.trackingSnapEndHr+".csv");
				//trackedCells_Away_Writer.append("Cell Type,Time Span,Cell State,Cell Speed,Cell Start Position X,Cell Start Position Y,Cell End Position X,Cell End Position Y,Length,Velocity,Displacement,Displacement Rate,Meandering Index,Nearest LTo Cell (microns)\n");
			}
			
			// Now use the utility class to create the output files and process the cell objects													
			CSVFileWriter.initialiseOutputFile(ppsim.filePath+"/"+ppsim.runDescription+"/",
					"/trackedCells_Close_"+simParams.trackingSnapStartHr+"to"+simParams.trackingSnapEndHr+"New.csv",
					simParams.cellResponseMeasures);
			CSVFileWriter.processSimulationResponses(ppsim.trackedCells_Close);
			
			CSVFileWriter.initialiseOutputFile(ppsim.filePath+"/"+ppsim.runDescription+"/",
					"/trackedCells_Away_"+simParams.trackingSnapStartHr+"to"+simParams.trackingSnapEndHr+"New.csv",
					simParams.cellResponseMeasures);
			CSVFileWriter.processSimulationResponses(ppsim.trackedCells_Away);

			
			
			// RUN THE OUTPUT CELL TRACKS WHETHER WRITING TO FILE OR NOT - USED BY BOTH THE WEB AND NON WEB VERSIONS
			// write the tracks of cells close to the stromal cells
			/*
			this.outputTrackCellsResults(ppsim,ppsim.trackedCells_Close,trackedCells_Close_Writer);
			ppsim.averageLengthNear = ppsim.averageLength;
			ppsim.averageVelocityNear = ppsim.averageVelocity;
			ppsim.averageDisplacementNear = ppsim.averageDisplacement;
			ppsim.averageDisplacementRateNear = ppsim.averageDisplacementRate;
			ppsim.averageMeanderingIndexNear = ppsim.averageMeanderingIndex;
			
			ppsim.velocitiesNear = ppsim.velocities;
			ppsim.displacementsNear = ppsim.displacements;
			ppsim.lengthsNear = ppsim.lengths;*/
			
			//if(!simParams.webRun)
			//{
				//this.summaryWriter.append(ppsim.averageLength+","+ppsim.averageVelocity+","+ppsim.averageDisplacement+","+ppsim.averageDisplacementRate+","+ppsim.averageMeanderingIndex+",");
			//}
			
			// write the tracks of the cells away from the stromal cell
			//this.outputTrackCellsResults(ppsim,ppsim.trackedCells_Away,trackedCells_Away_Writer);
			
			/*
			if(!simParams.webRun)
			{
				//this.summaryWriter.append(ppsim.averageLength+","+ppsim.averageVelocity+","+ppsim.averageDisplacement+","+ppsim.averageDisplacementRate+","+ppsim.averageMeanderingIndex+"\n");
			
				trackedCells_Close_Writer.close();
				trackedCells_Away_Writer.close();
				//summaryWriter.close();
			}*/
			
			
		//} 
		//catch (IOException e) 
		//{
			//e.printStackTrace();
		//}
		
	}
	

	/**
	 * Performed each step in the simulation - monitors the two tracked bags during cell tracking, moving cells between them as necessary
	 */
	public void step(final SimState state)
	{
		PPatchSim ppsim = (PPatchSim)state;
		
		// while the simulation is running
		if(ppsim.schedule.getSteps()*simParams.secondsPerStep <= ((simParams.simulationTime*60)*60))
		{
			if(simParams.cellTrackingEnabled)
			{
				// THERE WILL BE CELLS IN THE BAG BEING TRACKED, THOUGH TRACKING IS YET TO START
				// SET THE TRACK START LOCATION OF ALL OF THESE CELLS NOW THE TIME HAS COMMENCED
				// IF MULTIPLE TRACKING RANGES ARE USED, BOTH BAGS WILL NEED TO BE RESET
				if(ppsim.schedule.getSteps()*simParams.secondsPerStep == ((simParams.trackingSnapStartHr*60)*60))
				{	
					for(int k=0;k<ppsim.trackedCells_Away.size();k++)
					{
						Cells trackedCell = (Cells) ppsim.trackedCells_Away.get(k);
						trackedCell.agentTrackStartLocation = trackedCell.agentLocation;
						trackedCell.agentTrackEndLocation = null;
						trackedCell.timeTracked = 0;
						trackedCell.trackLength = 0;
					}
					
					for(int k=0;k<ppsim.trackedCells_Close.size();k++)
					{
						Cells trackedCell = (Cells) ppsim.trackedCells_Close.get(k);
						trackedCell.agentTrackStartLocation = trackedCell.agentLocation;
						trackedCell.agentTrackEndLocation = null;
						trackedCell.timeTracked = 0;
						trackedCell.trackLength = 0;
					}
				}
				
				
				// TRACKING HAS NOW ENDED - ADD THE END LOCATION FOR ALL CELLS THAT ARE STILL BEING TRACKED
				else if(ppsim.schedule.getSteps()*simParams.secondsPerStep == ((simParams.trackingSnapEndHr*60)*60))
				{
					for(int k=0;k<ppsim.trackedCells_Away.size();k++)
					{
						Cells trackedCell = (Cells) ppsim.trackedCells_Away.get(k);
						
						if(trackedCell.agentTrackEndLocation == null)
						{
							trackedCell.agentTrackEndLocation = trackedCell.agentLocation;
						}
					}
					
					for(int k=0;k<ppsim.trackedCells_Close.size();k++)
					{
						Cells trackedCell = (Cells) ppsim.trackedCells_Close.get(k);
						if(trackedCell.agentTrackEndLocation == null)
						{
							trackedCell.agentTrackEndLocation = trackedCell.agentLocation;
						}
					}
					
					// NOW TRACKING HAS ENDED, PREPARE FOR THE NEXT TRACKING RANGE
					// FIRSTLY OUTPUT THE CURRENT TRACKED RANGE
					this.generateStatsAtRunEnd(ppsim);
					
					// NOW GENERATE EXPRESSION
					ppsim.cellexp.generateExpressionStats(ppsim);
					
					// set the next range (if there is one)
					if(!simParams.trackingHourRanges.isEmpty())
					{
						StringTokenizer st = new StringTokenizer(this.simParams.trackingHourRanges.get(0),"-");
						this.simParams.trackingSnapStartHr = Integer.parseInt(st.nextToken());
						this.simParams.trackingSnapEndHr = Integer.parseInt(st.nextToken());
						this.simParams.trackingHourRanges.remove(0);
					}
					
					
					//if(!simParams.trackingStartHours.isEmpty())
					//{
						//simParams.trackingSnapStartHr = simParams.trackingStartHours.get(0);
						//simParams.trackingSnapEndHr = simParams.trackingEndHours.get(0);
					//}
				}
			}
			
			// CALL TO JAY'S METHOD TO CHECK THE CELLULARITY IN THE SYSTEM
			//if (ppsim.schedule.getSteps()%60 == 0)
			//{
				//generateDataTable(ppsim);
			//}
			
		}
		else
		{
			this.stop();
		}
		
	}
	
	/**
	 * Flag to show if this class has been stopped (when no longer needed)
	 */
	private Stoppable stopper = null;
	
	/**
	 * Method to change the value of the stopper
	 * @param stopper	Whether the class should be stopped or not
	 */
    public void setStopper(Stoppable stopper)   {this.stopper = stopper;}
    
    /**
     * Method to stop the class where necessary
     */
    public void stop(){stopper.stop();}
}
