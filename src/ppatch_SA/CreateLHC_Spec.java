package ppatch_SA;

import java.io.File;
import java.text.DecimalFormat;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class CreateLHC_Spec 
{
	
	

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		String metaDataFile = args[0];
		String outputFilePath = args[1];
		double variance = Double.parseDouble(args[2]);
		
		
		try
		{	
			// READING
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc;
			
			doc = docBuilder.parse(new File(metaDataFile));
			
			// normalize text representation
			doc.getDocumentElement ().normalize ();
			
			// WRITE THE ROOT
			//root elements
			Document docWriting = docBuilder.newDocument();
			Element rootElement = docWriting.createElement(doc.getDocumentElement().getNodeName());
			docWriting.appendChild(rootElement);
			
			// Get the Parameter Group Tags
			NodeList paramGroups = doc.getDocumentElement().getChildNodes();
						
			// Now go through each group in turn
			for(int i=0;i<paramGroups.getLength();i++)
			{
				if(!paramGroups.item(i).getNodeName().equals("#text"))
				{
					// WRITE THIS NODE TO THE FILE
					Element staff = docWriting.createElement(paramGroups.item(i).getNodeName());
					rootElement.appendChild(staff);
					
					// Get the nodes for this child - these will be the parameters in each group
					Node n = paramGroups.item(i);
					NodeList groupVars = n.getChildNodes();
					
					// Now go through this subset in turn
					for(int j=0;j<groupVars.getLength();j++)
					{
						if(!groupVars.item(j).getNodeName().equals("#text"))
						{	
							// Get the nodes for this child - these will be the parameter attributes
							Node attrs = groupVars.item(j);
							NodeList attrVars = attrs.getChildNodes();
							
							// 1 - the default value for the parameter
							//System.out.println(attrVars.item(1).getTextContent());
							// 3 - the variable type
							//System.out.println(attrVars.item(3).getTextContent());
							// 5 - the units
							//System.out.println(attrVars.item(5).getTextContent());
							// 7 - the parameter type (simulation / experimental etc)
							//System.out.println(attrVars.item(7).getTextContent());
							
							if(attrVars.item(7).getTextContent().equals("simulation"))
							{
								// WRITE THIS TO FILE
								Element firstname = docWriting.createElement(groupVars.item(j).getNodeName());
								staff.appendChild(firstname);
																
								Element defaultVal = docWriting.createElement(attrVars.item(1).getNodeName());
								defaultVal.appendChild(docWriting.createTextNode(attrVars.item(1).getTextContent()));
								firstname.appendChild(defaultVal);
								
								Element varType = docWriting.createElement(attrVars.item(3).getNodeName());
								varType.appendChild(docWriting.createTextNode(attrVars.item(3).getTextContent()));
								firstname.appendChild(varType);
								
								DecimalFormat dForm = new DecimalFormat("#.###");

								
								// Now write the SA Max & Min for this variable
								if(attrVars.item(3).getTextContent().equals("double"))
								{
									double rangeOfVariance = Double.parseDouble(attrVars.item(1).getTextContent()) * variance;
									double lhc_SA_min = Double.parseDouble(attrVars.item(1).getTextContent()) - rangeOfVariance;
									double lhc_SA_max = Double.parseDouble(attrVars.item(1).getTextContent()) + rangeOfVariance;
									
									Element saMax = docWriting.createElement("lhc_SA_max");
									saMax.appendChild(docWriting.createTextNode(String.format("%.3f",lhc_SA_max)));
									firstname.appendChild(saMax);
									
									Element saMin = docWriting.createElement("lhc_SA_min");
									saMin.appendChild(docWriting.createTextNode(String.format("%.3f",lhc_SA_min)));
									firstname.appendChild(saMin);
							      
								} 
								else if(attrVars.item(3).getTextContent().equals("integer"))
								{
									double rangeOfVariance = Double.parseDouble(attrVars.item(1).getTextContent()) * variance;
									int lhc_SA_min = (int)Math.floor(Integer.parseInt(attrVars.item(1).getTextContent()) - rangeOfVariance);
									int lhc_SA_max = (int)Math.ceil(Integer.parseInt(attrVars.item(1).getTextContent()) + rangeOfVariance);
									
									Element saMax = docWriting.createElement("lhc_SA_max");
									saMax.appendChild(docWriting.createTextNode(Integer.toString(lhc_SA_max)));
									firstname.appendChild(saMax);
									
									Element saMin = docWriting.createElement("lhc_SA_min");
									saMin.appendChild(docWriting.createTextNode(Integer.toString(lhc_SA_min)));
									firstname.appendChild(saMin);
								}
								
								
								
								
							}
							else  // not an SA variable, so just need the default value
							{
								// AS THIS IS A VARIABLE WHICH CAN CHANGE, NEED TO WRITE MORE DETAILS
								Element firstname = docWriting.createElement(groupVars.item(j).getNodeName());
								firstname.appendChild(docWriting.createTextNode(attrVars.item(1).getTextContent()));
								staff.appendChild(firstname);
								
							}
						}
					}
				}
				
			}
			//write the content into xml file
			  TransformerFactory transformerFactory = TransformerFactory.newInstance();
			  Transformer transformer = transformerFactory.newTransformer();
			  DOMSource source = new DOMSource(docWriting);
			  StreamResult result =  new StreamResult(new File(outputFilePath));
			  transformer.transform(source, result);
		}
		catch (SAXParseException err) 
		{
			System.out.println ("** Parsing error" + ", line " 
				+ err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());

		}
		catch (SAXException e) 
		{
			Exception x = e.getException ();
			((x == null) ? e : x).printStackTrace ();
		}
		// catch all exceptions
		catch (Throwable t) 
		{
			t.printStackTrace ();
		}	

	}

}
