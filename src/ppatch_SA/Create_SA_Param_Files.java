package ppatch_SA;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Create_SA_Param_Files 
{
	
	public static boolean checkArrayList(ArrayList<String> parameters, String paramToFind)
	{
		Iterator<String> agents = parameters.iterator();
		boolean found=false;
		while(agents.hasNext())
		{
			String paramInArray = agents.next();
			//System.out.println(paramInArray+" "+paramToFind);
			
			if(paramInArray.equals(paramToFind))
			{
				System.out.println(paramInArray+" "+paramToFind);
				found=true;
			}
		}
		
		return found;
		
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TAKES A DIRECTORY STATING WHERE THE METADATA, PARAMETERS FOR RUNS CSV FILE IS, AND WHERE OUTPUT SHOULD GO
		// THIS IS RUN AS ITS OWN JAR
		
		//String lhcParamsForRunsFilePath = args[0];
		//String paramsMetaDataFilePath = args[1];
		//String paramFileOutputFolder = args[2];
		//int numSamples = Integer.parseInt(args[3]);
		
		
		
		String lhcParamsForRunsFilePath = "/home/kieran/Desktop/JC_LHC/LHC_Parameters_for_Runs.csv"; 
		String paramsMetaDataFilePath = "/home/kieran/Desktop/GCSimParameters.xml";
		String paramFileOutputFolder = "/home/kieran/Desktop/JC_LHC/";
		int numSamples = 500;
		
		ArrayList<String> simMeasures = new ArrayList<String>();
		simMeasures.add("maxNonInteractiveTime");
		simMeasures.add("maxAge");
		simMeasures.add("timeToProliferate");
		simMeasures.add("differentiationProbability");
		simMeasures.add("centroblastLifeSpan");
		simMeasures.add("maxNonInteractingThreshold");
		simMeasures.add("dedifferentiationProbability");
		simMeasures.add("centrocyteLifeSpan");
				
		try
		{
			
			String lhcDesignFile = lhcParamsForRunsFilePath;
			BufferedReader br = new BufferedReader( new FileReader(lhcDesignFile));
			
			String strLine = "";
			StringTokenizer st = null;
			
			// FIRSTLY, SKIP OVER THE HEADINGS LINE IN THE CSV FILE
			strLine = br.readLine();
			
			// NOW GENERATE THE PARAMETER FILE FOR ALL 500 PERMUTATIONS FROM THE LATIN HYPERCUBE FILE
			for(int i=0;i<numSamples;i++)
			{
				//read comma separated file line by line & make a new parameter file for each line
				// READING
				DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
				DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
				Document doc;
			
				doc = docBuilder.parse(new File(paramsMetaDataFilePath));
				
				// normalize text representation
				doc.getDocumentElement ().normalize ();
				
				// WRITE THE ROOT
				//root elements
				Document docWriting = docBuilder.newDocument();
				Element rootElement = docWriting.createElement(doc.getDocumentElement().getNodeName());
				docWriting.appendChild(rootElement);
				
			
				// READ IN THE NEXT LINE OF THE LHC DESIGN (A PARAMETER SET)
				
				strLine = br.readLine();
				
				// BREAK THE LINE DOWN
				st = new StringTokenizer(strLine, ",");
				
					
				// IGNORE THE FIRST TOKEN - LINE NUMBER
				//st.nextToken();
							
				// Get the Parameter Group Tags
				NodeList paramGroups = doc.getDocumentElement().getChildNodes();
									
				// Now go through each group in turn
				for(int j=0;j<paramGroups.getLength();j++)
				{
					if(!paramGroups.item(j).getNodeName().equals("#text"))
					{
						// WRITE THIS NODE TO THE DOC THAT WILL GO TO FILE
						Element staff = docWriting.createElement(paramGroups.item(j).getNodeName());
						//System.out.println(staff.getNodeName());
						rootElement.appendChild(staff);
						
						
						// Get the nodes for this child - these will be the parameters in each group
						Node n = paramGroups.item(j);
						NodeList groupVars = n.getChildNodes();
								
						// Now go through this subset in turn
						//System.out.println(groupVars.getLength());
						for(int k=0;k<groupVars.getLength();k++)
						{
							if(!groupVars.item(k).getNodeName().equals("#text"))
							{	
								Element paramName = docWriting.createElement(groupVars.item(k).getNodeName());
								//System.out.println(groupVars.item(k).getTextContent());
															
								staff.appendChild(paramName);
								
								if(!checkArrayList(simMeasures,groupVars.item(k).getNodeName()))
								{
									// Write the value of the parameter
									paramName.appendChild(docWriting.createTextNode(groupVars.item(k).getTextContent()));
								}
								else
								{
									if(groupVars.item(k).getNodeName().equals("differentiationProbability") || 
											groupVars.item(k).getNodeName().equals("dedifferentiationProbability"))
									{
										paramName.appendChild(docWriting.createTextNode(st.nextToken()));
									}
									else
									{
										int val = (int)Math.ceil(Double.parseDouble(st.nextToken()));
										
										paramName.appendChild(docWriting.createTextNode(Integer.toString(val)));
									}
								}
								
								
								
																// NOW GET THE CHILDREN OF THIS PARAMETER
								//Node p = groupVars.item(k);
								//NodeList paramAttrs = p.getChildNodes();
								
								//System.out.println(paramAttrs.getLength());
								
								/*
								// Now go through each tag of the parameter
								Element paramValue = docWriting.createElement(paramAttrs.item(1).getNodeName());
									
								if(paramAttrs.item(7).getTextContent().equals("simulation"))
								{
									if(paramAttrs.item(3).getTextContent().equals("double"))
									{
										paramValue.appendChild(docWriting.createTextNode(String.format("%.3f",Double.parseDouble(st.nextToken()))));
									}
									else		// DEALING WITH INTS OR STRINGS
									{
											paramValue.appendChild(docWriting.createTextNode(st.nextToken()));
									}
								}
								else
								{
									// WRITE THE DEFAULT VALUE
									paramValue.appendChild(docWriting.createTextNode(paramAttrs.item(1).getTextContent()));		
								}
								
								paramName.appendChild(paramValue);
										
								// NOW WRITE THE REST OF THE INFO FOR THIS PARAMETER
											
								Element paramType = docWriting.createElement(paramAttrs.item(3).getNodeName());
								paramType.appendChild(docWriting.createTextNode(paramAttrs.item(3).getTextContent()));
								paramName.appendChild(paramType);
											
								Element paramUnits = docWriting.createElement(paramAttrs.item(5).getNodeName());
								paramUnits.appendChild(docWriting.createTextNode(paramAttrs.item(5).getTextContent()));
								paramName.appendChild(paramUnits);
											
								Element paramLogic = docWriting.createElement(paramAttrs.item(7).getNodeName());
								paramLogic.appendChild(docWriting.createTextNode(paramAttrs.item(7).getTextContent()));
								paramName.appendChild(paramLogic);
									
								Element paramTooltip = docWriting.createElement(paramAttrs.item(9).getNodeName());
								paramTooltip.appendChild(docWriting.createTextNode(paramAttrs.item(9).getTextContent()));
								paramName.appendChild(paramTooltip);
							*/			
							}
						}
						
					}
				}
				// NOW WRITE THIS PARAMETER FILE
				//System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.xsltc.trax.TransformerFactoryImpl");
				TransformerFactory transformerFactory = TransformerFactory.newInstance();
				Transformer transformer = transformerFactory.newTransformer();
				transformerFactory.setAttribute("indent-number", 2);
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");

				docWriting.normalizeDocument();
				DOMSource source = new DOMSource(docWriting);
				StreamResult result =  new StreamResult(new File(paramFileOutputFolder+"/paramFile_"+(i+1)+".xml"));
				transformer.transform(source, result);
				
			}
			
			
			
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}


}
