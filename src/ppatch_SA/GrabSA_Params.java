package ppatch_SA;

import java.io.*;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import java.util.ArrayList;


public class GrabSA_Params 
{
	/**
	 * File writer for cells close to a forming patch
	 */
	public FileWriter param_Writer;
	
	/**
	 * Small test function to print the formed array to screen for debugging
	 */
	public void printout_array(ArrayList sa_params)
	{
		for(int g=0;g<sa_params.size();g++)
		{
			ArrayList temp = (ArrayList)sa_params.get(g);
			// Name of Parameter
			System.out.println(temp.get(0));
			// Default Value
			System.out.println(temp.get(1));
			// Type of Parameter
			System.out.println(temp.get(2));
			// Max SA Val
			System.out.println(temp.get(3));
			// Min SA Val
			System.out.println(temp.get(4));
		}
	}
	
	public ArrayList generate_SA_Parameter_Array()
	{
		ArrayList<ArrayList> sa_params = new ArrayList<ArrayList>();
		
		
		try
		{
			param_Writer = new FileWriter("saParamSummary.csv");
			param_Writer.append("Parameter,Default,Type,Max,Min\n");
			
			// READING
			DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
			Document doc;
		
			doc = docBuilder.parse(new File("lhcDesign.xml"));
			
			// normalize text representation
			doc.getDocumentElement ().normalize ();
			
			// Get the Parameter Group Tags
			NodeList paramGroups = doc.getDocumentElement().getChildNodes();
									
			// Now go through each group in turn
			for(int i=0;i<paramGroups.getLength();i++)
			{
				if(!paramGroups.item(i).getNodeName().equals("#text"))
				{
					// Get the nodes for this child - these will be the parameters in each group
					Node n = paramGroups.item(i);
					NodeList groupVars = n.getChildNodes();
					
					// Now go through this subset in turn
					for(int j=0;j<groupVars.getLength();j++)
					{
						if(!groupVars.item(j).getNodeName().equals("#text"))
						{	
							// Get the nodes for this child - these will be the parameter attributes
							Node attrs = groupVars.item(j);
							NodeList attrVars = attrs.getChildNodes();
							
							if(attrVars.getLength()>1)		// This will be a node which has child nodes (and SA info)
							{
								// THIS WILL BE A PARAMETER THAT WILL BE CHANGED DURING SA ANALYSIS
								// SO STORE IN THE ARRAY
								ArrayList<String> paramDetails = new ArrayList<String>();
								param_Writer.append(groupVars.item(j).getNodeName()+",");
								//System.out.println(groupVars.item(j).getNodeName());
								param_Writer.append(attrVars.item(0).getTextContent()+",");
								//System.out.println(attrVars.item(0).getTextContent());
								param_Writer.append(attrVars.item(1).getTextContent()+",");
								//System.out.println(attrVars.item(1).getTextContent());
								param_Writer.append(attrVars.item(2).getTextContent()+",");
								//System.out.println(attrVars.item(2).getTextContent());
								param_Writer.append(attrVars.item(3).getTextContent()+"\n");
								//System.out.println(attrVars.item(3).getTextContent());
							}
						}
					}
				}				
			}
			param_Writer.close();
			
			
		}
		catch (SAXParseException err) 
		{
			System.out.println ("** Parsing error" + ", line " 
				+ err.getLineNumber () + ", uri " + err.getSystemId ());
			System.out.println(" " + err.getMessage ());

		}
		catch (SAXException e) 
		{
			Exception x = e.getException ();
			((x == null) ? e : x).printStackTrace ();
		}
		// catch all exceptions
		catch (Throwable t) 
		{
			t.printStackTrace ();
		}	

		return sa_params;
		
	}
	

	public static void main(String[] args) 
	{
		GrabSA_Params test = new GrabSA_Params();
		ArrayList sa_params = test.generate_SA_Parameter_Array();
		//test.printout_array(sa_params);
		
	
	}
	
	
	
}
