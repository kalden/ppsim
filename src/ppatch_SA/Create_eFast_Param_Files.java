package ppatch_SA;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.StringTokenizer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Create_eFast_Param_Files 
{

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		// TAKES A DIRECTORY STATING WHERE THE METADATA, PARAMETERS FOR RUNS CSV FILE IS, AND WHERE OUTPUT SHOULD GO
		// THIS IS RUN AS ITS OWN JAR
		
		String fileAddress = "/media/MyBook/Patch_SA/eFAST/";
		String paramsMetaDataFilePath = "/media/MyBook/Patch_SA/eFAST/parameters-master.xml";
		String paramFileOutputFolder = "/media/MyBook/Patch_SA/eFAST/";
		int numReSamples = 3;
		int numParams = 7;
		int numSamples = 65;
		
		//String fileAddress = args[0];
		//int numReSamples = Integer.parseInt(args[1]);
		//int numParams = Integer.parseInt(args[2]);
		//int numSamples = Integer.parseInt(args[3]);
		
		
		try
		{
			new File(fileAddress+"/paramFiles/").mkdir();
			
			for(int q=1;q<=numReSamples;q++)
			{
				new File(fileAddress+"/paramFiles/"+q).mkdir();
				
				// Now look at each parameter
				for(int p=1;p<=numParams;p++)
				{
					new File(fileAddress+"/paramFiles/"+q+"/"+p).mkdir();
					
					String lhcDesignFile = fileAddress+"/Curve"+q+"_Param"+p+".csv";
					BufferedReader br = new BufferedReader( new FileReader(lhcDesignFile));
			
					String strLine = "";
					StringTokenizer st = null;
			
					// FIRSTLY, SKIP OVER THE HEADINGS LINE IN THE CSV FILE
					strLine = br.readLine();
			
					// NOW GENERATE THE PARAMETER FILE FOR ALL 500 PERMUTATIONS FROM THE LATIN HYPERCUBE FILE
					for(int i=0;i<numSamples;i++)
					{
						//read comma separated file line by line & make a new parameter file for each line
						// READING
						DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
						DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
						Document doc;
					
						doc = docBuilder.parse(new File(paramsMetaDataFilePath));
						
						// normalize text representation
						doc.getDocumentElement ().normalize ();
						
						// WRITE THE ROOT
						//root elements
						Document docWriting = docBuilder.newDocument();
						Element rootElement = docWriting.createElement(doc.getDocumentElement().getNodeName());
						docWriting.appendChild(rootElement);
						
					
						// READ IN THE NEXT LINE OF THE LHC DESIGN (A PARAMETER SET)
						strLine = br.readLine();
						
						// BREAK THE LINE DOWN
						st = new StringTokenizer(strLine, ",");
							
						// IGNORE THE FIRST TOKEN - LINE NUMBER
						//st.nextToken();
									
						// Get the Parameter Group Tags
						NodeList paramGroups = doc.getDocumentElement().getChildNodes();
											
						// Now go through each group in turn
						for(int j=0;j<paramGroups.getLength();j++)
						{
							if(!paramGroups.item(j).getNodeName().equals("#text"))
							{
								// WRITE THIS NODE TO THE DOC THAT WILL GO TO FILE
								Element staff = docWriting.createElement(paramGroups.item(j).getNodeName());
								//System.out.println(staff.getNodeName());
								rootElement.appendChild(staff);
								
										
								// Get the nodes for this child - these will be the parameters in each group
								Node n = paramGroups.item(j);
								NodeList groupVars = n.getChildNodes();
										
								// Now go through this subset in turn
								for(int k=0;k<groupVars.getLength();k++)
								{
									if(!groupVars.item(k).getNodeName().equals("#text"))
									{	
										Element paramName = docWriting.createElement(groupVars.item(k).getNodeName());
										//System.out.println(paramName.getNodeName());
										staff.appendChild(paramName);
										
										// NOW GET THE CHILDREN OF THIS PARAMETER
										Node param = groupVars.item(k);
										NodeList paramAttrs = param.getChildNodes();
										
										// Now go through each tag of the parameter
										Element paramValue = docWriting.createElement(paramAttrs.item(1).getNodeName());
											
										if(paramAttrs.item(7).getTextContent().equals("simulation"))
										{
											if(paramAttrs.item(3).getTextContent().equals("double"))
											{
												paramValue.appendChild(docWriting.createTextNode(String.format("%.3f",Double.parseDouble(st.nextToken()))));
											}
											else		// DEALING WITH INTS OR STRINGS
											{
													paramValue.appendChild(docWriting.createTextNode(st.nextToken()));
											}
										}
										else
										{
											// WRITE THE DEFAULT VALUE
											paramValue.appendChild(docWriting.createTextNode(paramAttrs.item(1).getTextContent()));		
										}
										
										paramName.appendChild(paramValue);
												
										// NOW WRITE THE REST OF THE INFO FOR THIS PARAMETER
													
										Element paramType = docWriting.createElement(paramAttrs.item(3).getNodeName());
										paramType.appendChild(docWriting.createTextNode(paramAttrs.item(3).getTextContent()));
										paramName.appendChild(paramType);
													
										Element paramUnits = docWriting.createElement(paramAttrs.item(5).getNodeName());
										paramUnits.appendChild(docWriting.createTextNode(paramAttrs.item(5).getTextContent()));
										paramName.appendChild(paramUnits);
													
										Element paramLogic = docWriting.createElement(paramAttrs.item(7).getNodeName());
										paramLogic.appendChild(docWriting.createTextNode(paramAttrs.item(7).getTextContent()));
										paramName.appendChild(paramLogic);
											
										Element paramTooltip = docWriting.createElement(paramAttrs.item(9).getNodeName());
										paramTooltip.appendChild(docWriting.createTextNode(paramAttrs.item(9).getTextContent()));
										paramName.appendChild(paramTooltip);
												
									}
								}
							}
						}
						// NOW WRITE THIS PARAMETER FILE
						//System.setProperty("javax.xml.transform.TransformerFactory", "org.apache.xalan.xsltc.trax.TransformerFactoryImpl");
						TransformerFactory transformerFactory = TransformerFactory.newInstance();
						Transformer transformer = transformerFactory.newTransformer();
						transformerFactory.setAttribute("indent-number", 2);
						transformer.setOutputProperty(OutputKeys.INDENT, "yes");

						docWriting.normalizeDocument();
						DOMSource source = new DOMSource(docWriting);
						new File(fileAddress+"/paramFiles/"+q+"/"+p).mkdir();
						String xmlOutputAddress = paramFileOutputFolder+"paramFiles/"+q+"/"+p+"/";
						StreamResult result =  new StreamResult(new File(xmlOutputAddress +"paramFile_Curve"+q+"_Param"+p+"_"+(i+1)+".xml"));
						transformer.transform(source, result);
						
					}
				}
			}			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}


}
