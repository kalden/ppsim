package ppatch_onepatch_cells;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import sim.util.Bag;
import sim.util.Double2D;
import sim.util.Int2D;
import sim.util.IntBag;

/**
 * Class to deal with the feature that LTo cells divide every twelve hours once the first state change has occurred
 * 
 * @author kieran
 *
 */
public class CellDivision
{
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;
	
	/**
	 * Stores the simulation parameters in this class for ease of access later
	 * 
	 * @param sp	Simulation parameters used in this run
	 */
	public CellDivision(SimParameters2 sp)
	{
		this.simParams = sp;
	}
	
	/**
	 * Creates the duplicate of an existing LTo cell (i.e. simulating cell division)
	 * @param temp	The cell that is dividing
	 * @param ppsim	The current simulation state
	 * @return LTo ltoCell	The new LTo cell, sharing all properties of the original
	 */
	public void makeNewLTo(LTo dividingCell, LTo newCell, PPatchSim ppsim)
	{
		
		// The new LTo cell needs to have all the properties of the original, so copy these over
		newCell.cellState = dividingCell.cellState;
		newCell.chemoLinearAdjust = dividingCell.chemoLinearAdjust;
		newCell.chemoSigThreshold = dividingCell.chemoSigThreshold;
		newCell.vcamAdhesionEffect.vcamExpressionLevel = dividingCell.vcamAdhesionEffect.vcamExpressionLevel;
		
		// add the stats for the start of this cells life
		newCell.startingChemoLinearAdjust = dividingCell.chemoLinearAdjust;
		newCell.startingVCAMExpressionLevel = dividingCell.vcamAdhesionEffect.vcamExpressionLevel;
		
		if(newCell.cellState>0)  // an activeexpressing LTo
		{
			// add to the bag used to do patch size
			ppsim.activelToCellsBag.add(newCell);
		}
		
	}
	
	/**
	 * As the tract has been modelled as 2D, with cells allowed to roll round top and bottom, the cells can potentially find a divide location 
	 * on the opposite side of the tract (left and right)- this checks that this does not happen
	 * 
	 * @param ppsim	The current simulation state
	 * @param dividingCell	The LTo cell object that is dividing
	 * @param xPosInNeighbourhood	The x position that has been found by the Moore's Neighborhood algorithm which needs checking to ensure it is not on opposite side
	 * @param distance	The distance away from the dividing cell that the cell is looking to divide to
	 * @return a boolean stating whether the location is valid or not
	 */
	public boolean checkRightLeftRollAround(PPatchSim ppsim,LTo dividingCell, Integer xPosInNeighbourhood,int distance)
	{
		// GET NEIGHBOURS IS TOROIDAL AS THE CELL MAY DIVIDE OVER THE TOP/BOTTOM OF SCREEN
		// HOWEVER THIS MEANS THE CELL WILL DIVIDE LEFT & RIGHT
		// THIS CANNOT HAPPEN AND NEEDS TO BE DETECTED
		
		// gather whether the cell is on the left hand or right hand edge of the tract (if either)
		if(dividingCell.gridLoc.x < ((int)(simParams.initialGridLength/simParams.LTO_DIAMETER))/2)	// on the left
		{
			if((xPosInNeighbourhood-dividingCell.gridLoc.x)+distance < (int)(simParams.initialGridLength/simParams.LTO_DIAMETER))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else	// on the right
		{
			if((dividingCell.gridLoc.x - xPosInNeighbourhood)+distance < (int)(simParams.initialGridLength/simParams.LTO_DIAMETER))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
			
	}
	
	/**
	 * Used to simulate cell division.  This method deals with the creation of the divided cell, while ensuring
	 * this is not placed over an LTo cell that already exists
	 * 
	 * @param cellDividing	The existing LTo cell that is dividing
	 * @param ppsim	The current simulation state
	 * @return LTo ltoCell	New LTo cell representing the divided cell
	 */
	public void divideSingleLTo(LTo cellDividing,PPatchSim ppsim,int distance)
	{
		// convertDone - flag to show if an inactive cell has been found to be converted or not
		boolean convertDone = false;
		
		Int2D blankSpace = null;
		
		// Need to get the LTo Cells that are around itd
		IntBag xPos = new IntBag();
		IntBag yPos = new IntBag();
		
		// get the cells that are in the lto grid squares around this
		ppsim.lToGrid.getNeighborsMaxDistance(cellDividing.gridLoc.x, cellDividing.gridLoc.y, distance, true, xPos, yPos);
		
		// Now got through attempting to find an immature LTo that can be converted(or blank square if necessary for later!)
		
		// Now go through each grid in the neighbourhood, excluding the centre where the cell is
		for(int k=0;k<xPos.size() && !convertDone;k++)
		{	
			// FIX TO STOP DIVISION OVER RIGHT/LEFT OF SCREEN
			// CHECK THE LOCATION IS NOT ON THE OTHER SIDE OF THE TRACT
			// To check this, take the x grid location of where the cell wants to move, subtract the location of where the cell is (if on left,
			// if on right side swap them over), add the distance the cell wants to move, and check this is greater than the upper limit of the tract
			boolean moveOk = checkRightLeftRollAround(ppsim,cellDividing,xPos.get(k),distance);
				
			if(moveOk)
			{
				Cells stromalCell = (Cells)ppsim.lToGrid.get(xPos.get(k),yPos.get(k));
			
				if(stromalCell!=null && stromalCell.cellState==0)			// this is an LTo cell can be converted
				{
					// need to copy properties of dividing cell to this cell
					this.makeNewLTo(cellDividing, (LTo)stromalCell, ppsim);
					convertDone = true;
				}
			
				if(stromalCell==null && blankSpace==null)			// this is a free space and can therefore be filled with an LTo
				{
					blankSpace = new Int2D(xPos.get(k),yPos.get(k));
				}
			}
			
		}
		
		// Now need to check a cell has been created - if not (there were no inactive cells around to convert), need to create a new one
		if(!convertDone)
		{
			if(blankSpace!=null)
			{
				// take the first empty grid space that was found & place an LTo there
				// work out where this square represents on the grid (as the intestine itself will have grown, so the squares will be a different
				// size to originally
				
				// work out the size of each grid square
				double xAdj = PPatchSim.currentGridLength / ((int)(simParams.initialGridLength/simParams.LTO_DIAMETER));
				double yAdj = PPatchSim.currentGridHeight / ((int)(simParams.initialGridHeight/simParams.LTO_DIAMETER));
			
				// store the location
				Double2D location = new Double2D((blankSpace.x*xAdj)+simParams.LTO_DIAMETER/2,(blankSpace.y*yAdj)+simParams.LTO_DIAMETER/2);
				
				// set the ltoCell to be at this location
				LTo ltoCell = new LTo(location,simParams,blankSpace,ppsim.ltoCellsBag.size());
				
				// copy the properties of the dividing LTo cell to the new cell
				this.makeNewLTo(cellDividing, ltoCell, ppsim);

				// add to the storage and display
				ppsim.ltoCellsBag.add(ltoCell);
				
				ppsim.lToGrid.set(blankSpace.x,blankSpace.y, ltoCell);
				ppsim.tract.setObjectLocation(ltoCell,location);
				ltoCell.setStopper(ppsim.schedule.scheduleRepeating(ltoCell));		
				
			}
			else
			{
				// there were no blank squares around, need to look further afield for a free location
				this.divideSingleLTo(cellDividing, ppsim, distance+1);
			}
		}
	}
	
	/**
	 * Monitors when LTo cell division needs to occur - and if this step has been reached, the division occurs
	 * 
	 * @param ppsim	The current simulation state
	 */
	public void lToCellDivision(PPatchSim ppsim)
	{
		// Now deal with case where the time has been reached where the LTo Cells should be dividing
		// Check this time has come
		//if(ppsim.schedule.getSteps()>0 && ppsim.schedule.getSteps()<((60/simParams.secondsPerStep)*(simParams.numHoursRETLigandLToActive*60)))
		if((ppsim.schedule.getSteps()*simParams.secondsPerStep) > 0 && (ppsim.schedule.getSteps()*simParams.secondsPerStep) < ((simParams.numHoursRETLigandLToActive*60)*60))
		{
			//if((ppsim.schedule.getSteps()%((60/simParams.secondsPerStep)*(simParams.lToDivisionTime*60))==0))
			if(((ppsim.schedule.getSteps()*simParams.secondsPerStep)%((simParams.lToDivisionTime*60)*60)==0))
			{
				// Firstly take a copy of the LTo object bag
				// A copy is taken to iterate through as the new 'divided' cells are added to the original
				Bag ltoCellsCopy = cloneLToBag(ppsim);
			
				// need to divide each current LTo cell into two, so go through each
				
				for(int i=0;i<ltoCellsCopy.size();i++)
				{
					// get the LTo Cell
					LTo dividingCell = (LTo)ltoCellsCopy.get(i);
					
					// cells will only divide if ARTN/RET signal has occurred and VCAM/Chemokine is being expressed
				
					if(!dividingCell.stopped && dividingCell.cellState>1)
					{		
						// call the function to make the new (divided) cell - which ensures not placed over another
						this.divideSingleLTo(dividingCell,ppsim,1);
				
					}
				}
			}
		}
	}
	
	/**
	 * Creates a temporary copy of the LTo cell location bag for iteration purposes (as new LTo cells are added to the bag on division, can't iterate through when dividing)
	 * 
	 * @return Bag ltoCellCopy	Clone of original Bag
	 */
	public Bag cloneLToBag(PPatchSim ppsim)
	{
		Bag ltoCellCopy = new Bag();
		
		try
		{
			ltoCellCopy = (Bag)ppsim.ltoCellsBag.clone();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
				
		return ltoCellCopy;
	}
	
}
