package ppatch_onepatch_cells;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.Random;
import java.util.SortedMap;
import java.util.TreeMap;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import sim.engine.SimState;
import sim.util.Bag;
import sim.util.Double2D;
import sim.util.IntBag;
import sim.portrayal.*;

/**
 * Class which defines the behaviour and attributes of the LTin Cell.
 * With some common attributes and behaviour shared with the other cell types, this
 * class extends the Cells superclass
 * 
 * @author Kieran Alden
 *
 */
public class LTin extends Cells
{	
    /**
     * Creates a new LTin Agent (or Cell), and assigns the cell a speed.
     * This speed is a random gaussian number between a lower and upper bound set when the simulation is run
     * 
     * @param cellId	Tag used to keep track of the cell by the simulation, will be 'LTin' plus a number	
     * @param location	Double2D Coordinate location of the LTin Cell on the grid
     * @param cellSpeedLowBound	Lower bound used to generate cell speed
     * @param cellSpeedUpBound	Upper bound used to generate cell speed
     */
    public LTin(Double2D location,SimParameters2 sp)
	{
    	// Constructor for superclass takes the location & cell tag
		super(location,sp);
		
		this.agentPreviousLocation = location;
		
		// generate speed of this cell
		Random rnd = new Random();
		this.simParams = sp;
		
		// while loop is used to ensure the cellspeed falls within the set range
        while(cellSpeed<simParams.cellSpeedLowBound || cellSpeed>simParams.cellSpeedUpBound)
		{
			cellSpeed = rnd.nextGaussian();
		}
        
        cellSpeedSecond = this.cellSpeed / sp.secondsPerStep;
        cellSpeedScaled = this.cellSpeed*4;
        
        // set cell colour - all LTin cells in initial state start by being coloured yellow.  On change
        // of state, the colour will change
        this.cellState=4;
	}	
	
	
	/* (non-Javadoc)
	 * @see sim.app.ppatch.Cells#getType()
	 */
	public String getCellType()
    {
        return "LTin";
    }
	
	//public int getTimeTracked() {
		//return timeTracked;
	//}
	 
	/* (non-Javadoc)
	 * @see sim.engine.Steppable#step(sim.engine.SimState)
	 */
	public void step(final SimState state)
	{
		PPatchSim ppsim = (PPatchSim)state;
		
		if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.simulationTime*60)*60))
		{
			this.agentPreviousLocation = this.agentLocation;
		
			// check that the cell is still active (has not left the screen)
			if(!this.stopped)
			{
				if(simParams.cellTrackingEnabled)
				{
					//if(ppsim.schedule.getSteps()==((60/simParams.secondsPerStep)*(simParams.trackingSnapStartHr*60)))
					if(ppsim.schedule.getSteps()*simParams.secondsPerStep == ((simParams.trackingSnapStartHr*60)*60))
					{
						this.agentTrackStartLocation = this.agentLocation;
					}
					//if(ppsim.schedule.getSteps()==((60/simParams.secondsPerStep)*(simParams.trackingSnapEndHr*60)))
					if(ppsim.schedule.getSteps()*simParams.secondsPerStep ==((simParams.trackingSnapEndHr*60)*60))
					{
						this.agentTrackEndLocation = this.agentLocation;
					}
				}
				// no attractive force for LTin Cells - need to implement random angle movement
				// If the cell is in a position where it would be held by adhesion factors, this is calculated in the performMove3 method
				
				double angle = Math.toRadians(ppsim.random.nextInt(360)+1);
				// send to function which deals with the move
				this.performMove3(ppsim,angle);					

			}
			else		// the cell has left the right or left of the screen/tract and will be stopped and removed from the simulation
			{
				ppsim.tract.remove(this);
				this.stop();
				
				// decrease the cellularity
				simParams.lTinCellularity--;
			
				// Remove from all tracking
				if(simParams.cellTrackingEnabled)
				{
					if(ppsim.trackedCells_Away.contains(this))
						ppsim.trackedCells_Away.remove(this);
					if(ppsim.trackedCells_Close.contains(this))
						ppsim.trackedCells_Close.remove(this);
				}
			}
		}
		else
		{
			// simulation is over so stop the cell
			this.stop();
		}
	}	
			
	/**
	 * Used by the graphic console to draw the cell to the screen
	 */
	public final void draw(Object object, Graphics2D graphics, DrawInfo2D info)
    {
		// Note multiplied by info - this ensures the diameter grows as the zoom function is used
		double diamx = info.draw.width*simParams.HCELL_DIAMETER;
		double diamy = info.draw.height*simParams.HCELL_DIAMETER;
	
		graphics.setColor(PPatchSim.cellColours.get(cellState));
		Ellipse2D.Double cell = new Ellipse2D.Double((int)info.draw.x-diamx/2,(int)info.draw.y-diamy/2,(int)diamx,(int)diamy);
        graphics.fill(cell);
    }
}
