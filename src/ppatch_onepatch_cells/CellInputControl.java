package ppatch_onepatch_cells;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import sim.display.Display2D;
import sim.engine.Schedule;
import sim.engine.SimState;
import sim.engine.Steppable;
import sim.engine.Stoppable;
import sim.field.continuous.Continuous2D;
import sim.util.Bag;
import sim.util.Double2D;
import sim.util.Int2D;
import sim.util.IntBag;

/**
 * Class to control the functionality at each step of the simulation.  
 * (a) migration of LTin and LTi cells to the tract.  As a number of these cells enter per step, 
 * the best way to control migration was to have a class which is Steppable and manages this migration. From the start, LTin cells will migrate at a given rate per step and for a certain number of steps. 
 * LTi cells will then follow after a set delay, at a set rate, for a set time period
 * (b) generation of statistics per step (all saved to a csv file)
 * (c) division of LTo cells when this becomes necessary
 * (d) growth of the intestine tract as becomes necessary per step
 * 
 * Done this way to limit the need for a large number of steppable classes
 * 
 * @author Kieran Alden
 *
 */
public class CellInputControl implements Steppable,Stoppable
{	
	/**
	 * The current state of the simulation
	 */
	PPatchSim ppsim;
	
	/**
	 * The simulation schedule for which this class needs adding to
	 */
	Schedule sch;
	
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;
	
	/**
	 * Flag which counts the 'parts' of LTi cells created by the input rate at each step, when 1 is reached that cell is also created
	 */
	double ltiInFlag = 0;
	
	/**
	 * Flag which counts the 'parts' of LTin cells created by the input rate at each step, when 1 is reached that cell is also created
	 */
	double ltinInFlag = 0;
	
	/**
	 * Creates the class and schedules this to run at each step
	 * @param schedule	The current simulation schedule
	 */
	public CellInputControl(Schedule schedule,SimParameters2 sp,PPatchSim ppsim)
	{
		this.sch = schedule;
		this.simParams = sp;
		
		this.calculateCellPopulationFigures();

	}
	
	/**
	 * Calculates the number of LTin and LTi cells on the tract at E15.5 (from percentages gathered experimentally), and from this generates an input
	 * rate for both cell types
	 * 
	 */
	public void calculateCellPopulationFigures()
	{
		// Firstly, need to calculate the cell input rates from the E15.5 FACS data
		// Work out how many cells would fill all of the available area:
		double totalHCells = ((((int)(simParams.initialGridLength/simParams.HCELL_DIAMETER)*(int)(simParams.initialGridHeight/simParams.HCELL_DIAMETER))));
		
		// Now work out what the required number of LTin cells at E15.5 would be 
		double numLTin15pt5 = (totalHCells / 100)*simParams.percentLTinfromFACStain;
		
		// Now work out the required number of LTi Cells at E15.5
		double numLTi15pt5 = (totalHCells / 100)*simParams.percentLTifromFACStain;
		
		// Now calculate the input rates to achieve this number over a 24 hour period (E14.5-E15.5)
		simParams.lTinInputRate = numLTin15pt5/(((24*60)*60)/simParams.secondsPerStep);
		simParams.lTiInputRate = numLTi15pt5/(((24*60)*60)/simParams.secondsPerStep);
	}
	
	
	
		
	/**
	 * Adds the required number of LTin or LTi cells to the simulation in this step as set by the input rate
	 * 
	 * @param ppsim	The current simulation
	 * @param cellType	Type of cell being created (LTin or LTi)
	 * @param inputRate	The input rate per step for this cell type at this time point
	 */
	public void createCell(PPatchSim ppsim,String cellType)
	{
		Cells agent = null;
		boolean collision = true;
		Double2D loc = null;
			
		while(collision)
		{
			// CALCULATE WHERE THE CELL WILL ENTER
			// THE PARAMETER inputCircumferencePercentage CAN RESTRICT WHERE CELLS ENTER
			// THIS TESTS WHETHER THERE IS A SET RANGE WHERE LTIN/LTI CELLS MIGRATE INTO THE TRACT
			Double x,y;
				
			if(simParams.inputCircumferencePercentage != 100)
			{
				// Calculate the available circumference area
				Double rangeHeight = (ppsim.currentGridHeight/100)*simParams.inputCircumferencePercentage;
				// Get a random y variable somewhere in this shortened area
				double rangeY = ppsim.random.nextDouble()*rangeHeight;
					
				// Now need to adjust so this range is centered around the centre of the simulation space.
				// Using the middle ensures no cells roll around the screen straight away when a range is used
				// Calculate the lower lim y coord
				// divided by 2 as half of the input range goes above midpoint in environment, half below
				Double midpoint = ppsim.currentGridHeight/2;
				Double lowlim = midpoint - ((ppsim.currentGridHeight/100)*(simParams.inputCircumferencePercentage/2));
					
				// Now adjust the y parameter to base it around the middle
				y = lowlim + rangeY;
					
				// Calculate a random x
				x = ppsim.random.nextDouble()*ppsim.currentGridLength;
			}
			else
			{
				// Just work out random position in whole environment
				
				x = ppsim.random.nextDouble()*ppsim.currentGridLength;
				y = ppsim.random.nextDouble()*ppsim.currentGridHeight;
				
			}
				
			// Set the location of the cell to these coordinates
			loc = new Double2D(x,y);
				
			// Make the cell at this location 
			agent = this.makeCell(ppsim,loc, cellType);
			collision = agent.ltiltinCollision(ppsim.tract);
		}
				
		// determine if this cell should be expressing RET Ligand (based on a probability)
		// choose a random number between 1 and 100
			
		if(simParams.probabilityLTinLTiRETLigands>0)
		{
			int num = ppsim.random.nextInt(100);
				
			if(num<simParams.probabilityLTinLTiRETLigands)	// the cell will be expressing RET ligand
			{
				agent.expressingRETLigand = true;
			}
		}
			
		// Now there are guarantees as to no collisions, add this cell to the tract and to the scheduler
		ppsim.tract.setObjectLocation(agent,loc);
		agent.setStopper(sch.scheduleRepeating(agent));
			
		// add this cell to the list of those being tracked if tracking has started
		// The cell tracking class removes any which have been tracked for over than an hour
		if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.trackingSnapStartHr*60)*60) 
				&& simParams.cellTrackingEnabled)	// no point adding the cell if tracking has ended
		{
			// Set Track Start Location
			agent.agentTrackStartLocation = agent.agentLocation;
			// Add the cell to the list
			ppsim.trackedCells_Away.add(agent);
		}
	}
	
	/**
	 * Creates either an LTi or LTin cell at the given location
	 * @param loc	The location where the cell should be placed
	 * @param cellType	The type of cell being created (LTin or LTi)
	 * @return Cells agent - the LTin or LTi cell
	 */ 
	
	public Cells makeCell(PPatchSim ppsim,Double2D loc,String cellType)
	{
		Cells agent;
		if(cellType.equals("LTin"))
		{
			agent = new LTin(loc,simParams);
			simParams.lTinCellularity++;
		}
		else
		{
			agent = new LTi(loc,simParams);
			simParams.lTiCellularity++;
		
			ppsim.allLTis.add(agent);
		}

		return agent;
	}
	
	/**
	 * Adds the number of LTin cells required for a particular timestep to the simulation
	 * @param ppsim	The current simulation state
	 **/
	public void addLTinCells(PPatchSim ppsim)
	{
		double reqLTinCells =0; //required number of cells at point in time as predicted by graph
		double lTinCellsPrev = 0; //number of cells one step previous
		double steps = 0;
		steps = ppsim.schedule.getSteps();	
	
		if (simParams.lTinInputRateGraphType.equalsIgnoreCase("exp"))
		{
		//exponential graphs	
			reqLTinCells = Math.pow(simParams.lTinInputRateGraphConstant,steps); //from equation: y=1.00345^x to get 168 cells at 24 hours.
			lTinCellsPrev = Math.pow(simParams.lTinInputRateGraphConstant,(steps-1)); //work out the required number of cells by getting the number at one previous step
			simParams.lTinInputRate = reqLTinCells - lTinCellsPrev;
		}
		else if (simParams.lTinInputRateGraphType.equalsIgnoreCase("sqrt"))
		{
		// inverse exponentials using square root
			reqLTinCells = Math.pow(simParams.lTinInputRateGraphConstant*steps, 0.5); //from equation: y=sqrt(20.5x) to get 168 cells at 24 hours.
			lTinCellsPrev = Math.pow(simParams.lTinInputRateGraphConstant*(steps-1), 0.5); //work out the required number of cells by getting the number at one previous step
			simParams.lTinInputRate = reqLTinCells - lTinCellsPrev;
		}
		// if the graphType is not either of these, the lTiInputRate will remain the default constant rate.
		
	// inverse exponentials using natural log -- not used as takes too long
	/*	double logVar = 0;
		double logPrev = 0;			
		logVar = Math.pow(steps, 22.8);
		logPrev = Math.pow(steps-1, 22.8);
		reqLTiCells = Math.log(logVar); //from equation: y=ln(x^22.8) to get 168 cells at 24 hours.
		lTiCellsPrev = Math.log(logPrev);//work out the required number of cells by getting the number at one previous step
	*/	
		
		// add the cell if rate is a whole number
		for(int i=0;i<(int)simParams.lTinInputRate;i++)
		{
			this.createCell(ppsim,"LTin");
		}
		
		// deal with decimal input rates
		if(simParams.lTinInputRate%1 > 0)
		{
			// add the decimal/remainder to the flag, and when one is reached, add that cell
			ltinInFlag = ltinInFlag+(simParams.lTinInputRate%1);
			//System.out.println(ltinInFlag);
			if(ltinInFlag>=1)
			{
				this.createCell(ppsim,"LTin");
				// take the cell from the flag
				ltinInFlag = ltinInFlag-1;
			}
		}
		
	}
	
	/**
	 * Adds the number of LTi cells required for a particular timestep to the simulation
	 * @param ppsim	The current simulation state
	 */
	public void addLTiCells(PPatchSim ppsim)
	{
		double reqLTiCells =0; //required number of cells at point in time as predicted by graph
		double lTiCellsPrev = 0; //number of cells one step previous
		double steps = 0;
		steps = ppsim.schedule.getSteps();	
	
		if (simParams.lTiInputRateGraphType.equalsIgnoreCase("exp"))
		{
		//exponential graphs	
			reqLTiCells = Math.pow(simParams.lTiInputRateGraphConstant,steps); //from equation: y=1.00345^x to get 168 cells at 24 hours.
			lTiCellsPrev = Math.pow(simParams.lTiInputRateGraphConstant,(steps-1)); //work out the required number of cells by getting the number at one previous step
			simParams.lTiInputRate = reqLTiCells - lTiCellsPrev;
		}
		else if (simParams.lTiInputRateGraphType.equalsIgnoreCase("sqrt"))
		{
		// inverse exponentials using square root
			reqLTiCells = Math.pow(simParams.lTiInputRateGraphConstant*steps, 0.5); //from equation: y=sqrt(20.5x) to get 168 cells at 24 hours.
			lTiCellsPrev = Math.pow(simParams.lTiInputRateGraphConstant*(steps-1), 0.5); //work out the required number of cells by getting the number at one previous step
			simParams.lTiInputRate = reqLTiCells - lTiCellsPrev;
		}
		// if the graphType is not either of these, the lTiInputRate will remain the default constant rate.
		
	// inverse exponentials using natural log -- not used as takes too long
	/*	double logVar = 0;
		double logPrev = 0;			
		logVar = Math.pow(steps, 22.8);
		logPrev = Math.pow(steps-1, 22.8);
		reqLTiCells = Math.log(logVar); //from equation: y=ln(x^22.8) to get 168 cells at 24 hours.
		lTiCellsPrev = Math.log(logPrev);//work out the required number of cells by getting the number at one previous step
	*/	
		
		
		// add the cell if rate is a whole number
		for(int i=0;i<(int)simParams.lTiInputRate;i++)
		{
			this.createCell(ppsim,"LTi");
		}
		
		// deal with decimal input rates
		if(simParams.lTiInputRate%1 > 0)
		{
			// add the decimal/remainder to the flag, and when one is reached, add that cell
			ltiInFlag = ltiInFlag+(simParams.lTiInputRate%1);
			//System.out.println(ltiInFlag);
			if(ltiInFlag>=1)
			{
				this.createCell(ppsim,"LTi");
				// take the cell from the flag
				ltiInFlag = ltiInFlag-1;
			}
		}
		
	}

	/**
	 * Adds the required number of cells (LTin and LTi) per step, adjusts the input rates if required, takes twelve our snaps if necessary, triggers LTo cell
	 * division, and resizes the tract if necessary. All done for every simulation step
	 */
	public void step(final SimState state)
	{
		PPatchSim ppsim = (PPatchSim)state;
		
		if((ppsim.schedule.getSteps()*simParams.secondsPerStep)<((simParams.simulationTime*60)*60))
		{	
			if(ppsim.schedule.getSteps()*simParams.secondsPerStep<((simParams.lTinInputTime*60)*60))
			{
				this.addLTinCells(ppsim);
				
				// SMALL FUNCTION WHICH CAN INCREASE THE INPUT RATE OF CELLS PER STEP IF REQUIRED
				// NOT USED HERE BUT LEFT IN INCASE THIS FUNCTIONALITY IS REQUIRED IN THE FUTURE
				/*// check whether the input rate needs increasing until the upper bound is hit
				if(simParams.lTinInputRate<simParams.lTinInputRateUpperBound)
				{
					simParams.lTinInputRate = simParams.lTinInputRate+simParams.lTinInputRateAdjust;
				}*/
			}
			else
			{
				// set the input rate to zero to stop cell input
				simParams.lTinInputRate=0;
				
				// SMALL FUNCTION WHICH CAN DECREASE THE INPUT RATE OF CELLS PER STEP AFTER INPUT TIME IF REQUIRED
				// NOT USED HERE BUT LEFT IN INCASE THIS FUNCTIONALITY IS REQUIRED IN THE FUTURE
				// bring the rate down until zero
				//if(simParams.lTinInputRate>0)
				//{
					//simParams.lTinInputRate = simParams.lTinInputRate - simParams.lTinInputRateAdjust;
					//this.addLTinCells(ppsim);
				//}
			}
			
			// LTi
			// check delay
			if(ppsim.schedule.getSteps()*simParams.secondsPerStep>((simParams.lTiInputDelayTime*60)*60))
			{
				// delay has been passed - LTi cells enter
			
				if(ppsim.schedule.getSteps()*simParams.secondsPerStep<(((simParams.lTiInputDelayTime+simParams.lTiInputTime)*60)*60))
				{
					this.addLTiCells(ppsim);
					
					// SMALL FUNCTION WHICH CAN INCREASE THE INPUT RATE OF CELLS PER STEP IF REQUIRED
					// NOT USED HERE BUT LEFT IN INCASE THIS FUNCTIONALITY IS REQUIRED IN THE FUTURE
					// check whether the input rate needs increasing until the upper bound is hit
					/*if(simParams.lTiInputRate<simParams.lTiInputRateUpperBound)
					{
						simParams.lTiInputRate = simParams.lTiInputRate+simParams.lTiInputRateAdjust;
					}*/
					
				}
				else
				{
					// input time has passed - set the rate to zero
					simParams.lTiInputRate = 0;
					
					// SMALL FUNCTION WHICH CAN DECREASE THE INPUT RATE OF CELLS PER STEP AFTER INPUT TIME IF REQUIRED
					// NOT USED HERE BUT LEFT IN INCASE THIS FUNCTIONALITY IS REQUIRED IN THE FUTURE
					/*// bring the rate down until zero
					if(simParams.lTiInputRate>0)
					{
						simParams.lTiInputRate = simParams.lTiInputRate - simParams.lTiInputRateAdjust;
						this.addLTiCells(ppsim);
					}*/
				}
			}
			
			// Take a snap of the tract if required (could have been done elsewhere, but saves an extra steppable class
			if(simParams.twelveHourSnaps)
				ppsim.captureTrackImage.takeDisplaySnaps(ppsim);	
			
			// Take a snap if within a period where each step is being imaged
			if(simParams.stepBystepTrackingImages)
				ppsim.captureTrackImage.timePeriodImaging(ppsim);
			
			// Examine to see if the tract needs to be resized for this step
			ppsim.tractSizeControl.adjustTractSize(ppsim);
			
			// Determine if the LTo cells need to be divided
			ppsim.ltoCellDivision.lToCellDivision(ppsim);
			
			
		}
		else
		{	
			this.stop();
		}
	}
	
	/**
	 * Flag to show if this class has been stopped (i.e. no more cell entry)
	 */
	private Stoppable stopper = null;
	
	/**
	 * Function to set the stopper
	 * @param stopper	Whether the class has been stopped or not
	 */
    public void setStopper(Stoppable stopper)   {this.stopper = stopper;}
    
    /**
     * Function to stop the class 
     */
    public void stop(){stopper.stop();}
}
