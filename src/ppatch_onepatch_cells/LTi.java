package ppatch_onepatch_cells;

import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.util.Random;

import ppatch_chemokine.ChemokineReceptor;
import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import sim.engine.SimState;
import sim.portrayal.DrawInfo2D;
import sim.util.Bag;
import sim.util.Double2D;

/**
 * Class which defines the behaviour and attributes of the LTi Cell.
 * With some common attributes and behaviour shared with the other cell types, this
 * class extends the Cells superclass
 * 
 * @author Kieran Alden
 *
 */
public class LTi extends Cells
{
	public ChemokineReceptor chemoReceptor;
	
	/**
     * Creates a new LTi Agent (or Cell), and assigns the cell a speed.
     * This speed is a random gaussian number between a lower and upper bound set when the simulation is run
     * 	
     * @param location	Double2D Coordinate location of the LTi Cell on the grid
     * @param cellSpeedLowBound	Lower bound used to generate cell speed
     * @param cellSpeedUpBound	Upper bound used to generate cell speed
     */
    public LTi(Double2D location,SimParameters2 sp)
	{
    	// Constructor for superclass takes the location & cell tag
		super(location,sp);
		
		// Initialise the chemokine receptor
    	this.chemoReceptor = new ChemokineReceptor(sp);
		
		this.agentPreviousLocation = location;
		
		// generate speed of this cell
		Random rnd = new Random();
		
		// while loop is used to ensure the cellspeed falls within the set range
        while(cellSpeed<simParams.cellSpeedLowBound || cellSpeed>simParams.cellSpeedUpBound)
		{
			cellSpeed = rnd.nextGaussian();
		}
        
        cellSpeedSecond = this.cellSpeed / sp.secondsPerStep;
        
        cellSpeedScaled = this.cellSpeed*4;
        
        // set cell colour - all LTi cells in initial state start by being colour red.  On change
        // of state, the colour will change
        this.cellState=7;
	}	
    
    /* (non-Javadoc)
	 * @see sim.app.ppatch.Cells#getType()
	 */
	public String getCellType()
	{
		return "LTi";
	}
	
	public int getTimeTracked() {
		return timeTracked;
	}
	
	/* (non-Javadoc)
	 * @see sim.engine.Steppable#step(sim.engine.SimState)
	 */
	public void step(final SimState state)
	{
		PPatchSim ppsim = (PPatchSim)state;
		
		// Step while the simulation is running
		if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.simulationTime*60)*60))
		{
			this.agentPreviousLocation = this.agentLocation;
		
			// check that the cell is still active and has not been stopped due to leaving the screen/tract
			if(!this.stopped)
			{
				if(simParams.cellTrackingEnabled)
				{
					//if(ppsim.schedule.getSteps()==((60/simParams.secondsPerStep)*(simParams.trackingSnapStartHr*60)))
					if(ppsim.schedule.getSteps()*simParams.secondsPerStep ==((simParams.trackingSnapStartHr*60)*60))
					{
						this.agentTrackStartLocation = this.agentLocation;
					}
					//if(ppsim.schedule.getSteps()==((60/simParams.secondsPerStep)*(simParams.trackingSnapEndHr*60)))
					if(ppsim.schedule.getSteps()*simParams.secondsPerStep ==((simParams.trackingSnapEndHr*60)*60))
					{
						this.agentTrackEndLocation = this.agentLocation;
					}
				}
			
				double angle=0.0;
				
				// Unlike LTin Cells, LTi cells may respond to a chemokine attractant
				// chemoEffect method takes care of all chemokine calculations
				// If there is sufficient level of chemokine, the cell will want to move in the angle that matches that direction
				// If no level of chemokine, 99 is returned, signifying a random move

				int chosenDirection = Integer.parseInt(this.chemoReceptor.chemoEffect(ppsim, this.agentLocation));
			
				// Now we have used the grid to find the direction of the chemokine distribution & generate probabilities,
				// can use this to generate a random angle in that direction (see diagram for explanation)
				// if chosenDirection = 99, a random angle between 0 and 360 will be returned
				angle = this.chemoReceptor.calculateAngle(chosenDirection);
		
				// Now perform the move
				// Should the cell also be in a location where VCAM would have an influence, this is taken care of in performMove3
				this.performMove3(ppsim,angle);
				
			}
			else		// the cell has left the right or left of the screen/tract and will be stopped and removed from the simulation
			{
				ppsim.tract.remove(this);
				this.stop();
				// decrease the cellularity
				simParams.lTiCellularity--;
			
				// Remove from all tracking
				if(simParams.cellTrackingEnabled)
				{
					if(ppsim.trackedCells_Away.contains(this))
						ppsim.trackedCells_Away.remove(this);
					if(ppsim.trackedCells_Close.contains(this))
						ppsim.trackedCells_Close.remove(this);
				}
			
				ppsim.allLTis.remove(this);
			}
		}
		else
		{
			// Simulation is over so stop cell
			this.stop();
		}
		
	}
	
	/**
	 * Used by the graphic console to draw the cell to the screen
	 */
	public final void draw(Object object, Graphics2D graphics, DrawInfo2D info)
    {
		// Note multiplied by info - this ensures the diameter grows as the zoom function is used
		double diamx = info.draw.width*simParams.HCELL_DIAMETER;
		double diamy = info.draw.height*simParams.HCELL_DIAMETER;
	
		graphics.setColor(PPatchSim.cellColours.get(cellState));
		Ellipse2D.Double cell = new Ellipse2D.Double((int)info.draw.x-diamx/2,(int)info.draw.y-diamy/2,(int)diamx,(int)diamy);
        graphics.fill(cell);
    }

}
