package ppatch_onepatch_cells;

import java.awt.*;
import java.awt.geom.Ellipse2D;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import sim.engine.*;
import sim.portrayal.*;
import sim.util.*;
  
/**
 * Class which defines the behaviour and attributes of the LTo Cell.
 * With some common attributes and behaviour shared with the other cell types, this
 * class extends the Cells superclass
 * 
 * @author Kieran Alden
 *
 */
public class LTo extends Cells
{
	/**
	 * Stores the current Threshold used in the sigmoid function which determines chemokine diffusion from this cell - used for both CXCL13 & CCL21
	 */
	public double chemoSigThreshold;
	
	/**
	 * Stores the adjustment to the sigmoid curve which is what determines the level of expression of chemokine
	 */
	public double chemoLinearAdjust;
	
	/**
	 * For an inactive LTo, the number of times a cell has been in contact. Once a threshold is past, the cell becomes active
	 */
	public int imLToCellContactCount;
	
	/**
	 * Number of steps the LTo has been active - used to delete immature stromal cells after a set period
	 */
	public int activeTime;
	
	/**
	 * Flag to show that the LTo has been removed as this did not differentiate by a set time
	 */
	public boolean stopped;

	/**
	 * Each LTo will be considered to be in a patch - each having a number - dividing cells will be considered to be in the same patch
	 */
	public int patchNum;
	
	/**
	 * The x and y coordinates of where the LTo is on the underlying stromal grid
	 */
	public Int2D gridLoc;
	
	
	/****************
	 * PARAMETERS FOR LTO STATISTICS
	 */
	
	/**
	 * Starting value of chemokine expression
	 */
	public double startingChemoLinearAdjust;
	
	/**
	 * Chemokine expression level reached
	 */
	public double endingChemoLinearAdjust;
	
	/**
	 * Starting value of VCAM expression
	 */
	public double startingVCAMExpressionLevel;
	
	/**
	 * VCAM Expression level reached
	 */
	public double endingVCAMExpressionLevel;
	
	/**
	 * Timepoint at which first state change occurs
	 */
	public int lTinContactStateChangeTimePoint;
	
	/**
	 * Timepoint at which second state change occurs
	 */
	public int lTiContactStateChangeTimePoint;
	
	/**
	 * Timepoint at which final state change occurs
	 */
	public int matureLToStateChangeTimePoint;
	
	/**
     * Creates a new LTi Agent (or Cell), and assigns the cell a linear adjustment and CCL19 Expression level.
     * Currently these are set to be identical for each cell, but there is scope to change this
     * 	
     * @param location	Double2D Coordinate location of the LTo Cell on the grid
     */
    public LTo(Double2D location,SimParameters2 sp,Int2D gridLocation, int ltoNum) 
    {
          super(location,sp);
          Ellipse2D.Double cell = new Ellipse2D.Double(location.x,location.y,simParams.LTO_DIAMETER,simParams.LTO_DIAMETER);
          chemoSigThreshold = simParams.chemoSigCurveThreshold;
          chemoLinearAdjust = simParams.chemoUpperLinearAdjust;
        
          // Add the VCAM Adhesion Effect Receptor/Expression if not knocked out
          this.vcamAdhesionEffect = new ppatch_vcam.VCAM_Adhesion_Effect();
          
          // For stats use
          this.startingChemoLinearAdjust = chemoLinearAdjust;
          this.startingVCAMExpressionLevel = this.vcamAdhesionEffect.vcamExpressionLevel * sp.vcamSlope;
          
          // 0 used to represent LTo cells in initial state
          this.cellState = 0;
          this.activeTime = 0;
          this.stopped = false;
          this.patchNum = ltoNum;
          this.gridLoc = gridLocation;
                 
          this.imLToCellContactCount = 0;
    }
    
    /**
     * Changes the cell state to show that the cell is expressing RET Ligand
     */
    public void activateRETLigand()
    {
    	this.cellState = 1;
    }
    
    /**
     * Changes the state of the LTo cell when the time has come to remove RET ligands
     */
    public void removeRETLigandLTo()
    {
    	this.cellState = 9;
    }
    
    public double getLToVCAMExpression()
    {
    	return this.vcamAdhesionEffect.returnVCAMExpressionLevel();
    	
    }
      
    public String getType() { return "LTo"; }

    
    public void stableContact(Cells contactedCell,int timeStep, double vcamSlope,double maxVCAMeffectProbabilityCutoff)
    {
    	if(contactedCell instanceof LTin)
		{
			if(this.cellState==1)      // Still in first state
			{
				this.cellState = 2;                             // also change cell colour to rep change in state
				
				// add the timepoint this happened for stats purposes
				this.lTinContactStateChangeTimePoint = timeStep;
			}
		  		
			// increase level of VCAM expression by LTo
			this.vcamAdhesionEffect.increaseVCAMExpressionLevel(simParams.vcamIncrement);
		}
		else
		{
			if(this.cellState==2)      // state can only change after VCAM expression has commenced
			{
				this.cellState = 3;
				
				// add the timepoint this happened for stats purposes
				this.lTiContactStateChangeTimePoint = timeStep;
			}
		
			// increase level of chemokine expression by LTo
			// 	CHEMOKINES - ADJUST SIGMOID FUNCTION PARAMETERS - IN TURN INCREASING DISTANCE AFFECTED BY CHEMOKINES
			if(this.chemoLinearAdjust>simParams.chemoLowerLinearAdjust)
			{
				this.chemoLinearAdjust-=simParams.chemoLinearAdjustmentReducer;
				
				// Check whether the lower bound has now been reached - if this is the case record this for stat purposes
				if(this.chemoLinearAdjust<=simParams.chemoLowerLinearAdjust)
				{
					// before recording this state change, the VCAM expression must also be at its peak
					if((super.vcamAdhesionEffect.vcamExpressionLevel * vcamSlope)>= maxVCAMeffectProbabilityCutoff)
					{
						this.matureLToStateChangeTimePoint = timeStep;
					}
				}
			}
		}
    }
    
    /* (non-Javadoc)
     * @see sim.engine.Steppable#step(sim.engine.SimState)
     */
    public void step( final SimState state )
    {
    	PPatchSim ppsim = (PPatchSim)state;
    	
    	// step while the simulation is running
    	if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.simulationTime*60)*60))
		{
    		if(!this.stopped)
    		{
    			// Firstly, check the state of the LTo - if still in initial state after set amount of time, cell will die
    			// With standardised time variables in hours, need to convert to minutes for comparison
    			//if(this.cellState==0 && this.activeTime>((60/simParams.secondsPerStep)*(simParams.imLToActiveTime*60)))
    			if(this.cellState==0 && this.activeTime > ((simParams.imLToActiveTime*60)*60))
    			{
    				this.stopped=true;
    				System.out.println(this+" Stopped");
    				ppsim.tract.remove(this);
    				this.stop();
    			}
    			// Now check whether the time has come (if enabled) for all RET ligands to be removed
    			else if(ppsim.schedule.getSteps()>0 && ppsim.schedule.getSteps()*simParams.secondsPerStep == ((simParams.numHoursRETLigandLToActive*60)*60))
    			{
    				this.removeRETLigandLTo();
    			}
    			else
    			{
    				// carry on as cell is active - increasing steps has been active for
    				activeTime++;
   				
    				// Firstly, check if the cell needs to be 'activated' & RET Ligands have yet to be 'removed'
    				if(this.cellState==0)
    				{
    					if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.numHoursRETLigandLToActive*60)*60))
    					{
    						// check the contact count
    						if(this.imLToCellContactCount>simParams.numContactsActivateLTo)
    						{
    							// does this cell need activating?
    							// this is determined by both
    							// i: - the number of contacts the cell has had with LTi cells
    							// ii: - the distance the inactive cell is from another LTo
    							// AN ACTIVATED CELL SKIPS THE ARTN STEP AND GOES STRAIGHT TO EXPRESSION OF VCAM
    							//System.out.println("ACTIVATING AN LTO!");
    							this.cellState = 2;
    						}
    					}	
    				}
   				}
   			}
   			
   		}
    	else
    	{
    		this.stop();
    	}
    }
    
    /** 
	 * Draws the LTo cell to the screen, with the size dependent on the scale currently being represented
	 * @see sim.portrayal.SimplePortrayal2D#draw(java.lang.Object, java.awt.Graphics2D, sim.portrayal.DrawInfo2D)
	 */
    public final void draw(Object object, Graphics2D graphics, DrawInfo2D info)
    {
    	// COMMENTED OUT SO THAT LTO CELLS ARE NOT DRAWN ON THE SCREEN
    	// UNCOMMENT IF WANT TO SEE THE LTO CELLS
    	
    	// info.draw.width & height - the current level of zoom on the object (same as shown in display
    	
        /*double diamx = info.draw.width*simParams.LTO_DIAMETER;
        double diamy = info.draw.height*simParams.LTO_DIAMETER;
    	   
        graphics.setColor(PPatchSim.cellColours.get(cellState));
        
        // THE ORIGINAL COORDINATES ARE ADJUSTED FOR SCALE ON SCREEN, AND THE FACT THAT THE ELLIPSE IS ELSE DRAWN UPWARDS FROM THE
        // BOTTOM RIGHT
        Ellipse2D.Double cell = new Ellipse2D.Double((int)info.draw.x-diamx/2,(int)info.draw.y-diamy/2,(int)diamx,(int)diamy);
        
        graphics.fill(cell);*/

    }
}
