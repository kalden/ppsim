package ppatch_onepatch_cells;

import java.awt.*;
import java.awt.geom.Ellipse2D;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import sim.engine.*;
import sim.field.continuous.Continuous2D;
import sim.portrayal.*;
import sim.util.*;
  
/**
 * Class which defines the behaviour and attributes of the 'Decoy' Cell.  This is a cell which is on the 
 * stroma and expresses RET Ligand, yet is not an LTo
 * With some common attributes and behaviour shared with the other cell types, this
 * class extends the Cells superclass
 * 
 * @author Kieran Alden
 *
 */
public class RLNonStromal extends Cells
{	
	/**
	 * Number of steps the LTo has been active - used to delete immature stromal cells after a set period
	 */
	public int activeTime;
	
	/**
	 * Flag to show that the decoy has been removed (not implemented but may be necessary)
	 */
	public boolean stopped;
	
	/**
     * Creates a new Decoy Agent (or Cell)
     * 	
     * @param location	Double2D Coordinate location of the LTo Cell on the grid
     */
    public RLNonStromal(Double2D location,SimParameters2 sp,Int2D gridLocation, int decoyNum) 
    {
          super(location,sp);
          Ellipse2D.Double cell = new Ellipse2D.Double(location.x,location.y,simParams.LTO_DIAMETER,simParams.LTO_DIAMETER);
          this.vcamAdhesionEffect = new ppatch_vcam.VCAM_Adhesion_Effect();
          
          // 6 used to represent this cell is a decoy in initial state
          cellState = 6;
          activeTime = 0;
          stopped = false;
    }
    
    public String getType() { return "Decoy"; }
    
    public void removeRETLigandDecoys(PPatchSim ppsim)
    {
    	this.stopped=true;
		//System.out.println(this+" Stopped");
		ppsim.tract.remove(this);
		this.stop();
    }

    
    /* (non-Javadoc)
     * @see sim.engine.Steppable#step(sim.engine.SimState)
     */
    public void step( final SimState state )
    {
    	PPatchSim ppsim = (PPatchSim)state;
    	
    	if(ppsim.schedule.getSteps()*simParams.secondsPerStep < ((simParams.simulationTime*60)*60))
		{
    		if(!this.stopped)
    		{
    			// Determine if the cell needs to be removed from the system
    			if(this.activeTime>((simParams.imLToActiveTime*60)*60))
    			{
    				this.stopped=true;
    				System.out.println(this+" Stopped");
    				ppsim.tract.remove(this);
    				this.stop();
    			}
    			// Now check whether the time has come (if enabled) for all RET ligands to be removed
    			else if(ppsim.schedule.getSteps()*simParams.secondsPerStep > 0 && 
   					ppsim.schedule.getSteps()*simParams.secondsPerStep ==((simParams.numHoursRETLigandLToActive*60)*60))
    			{
    				this.removeRETLigandDecoys(ppsim);
    			}
    			else
    			{
    				// carry on as cell is active - increasing steps has been active for
    				activeTime++;
    			}
    		}
		}
    	else
		{
			this.stop();
		}
    }
   				
    
    public void rlStableContact()
    {
    	this.vcamAdhesionEffect.increaseVCAMExpressionLevel(simParams.vcamIncrement);
    	
    }
   				
    
    /** 
	 * Draws the Decoy cell to the screen, with the size dependent on the scale currently being represented
	 * @see sim.portrayal.SimplePortrayal2D#draw(java.lang.Object, java.awt.Graphics2D, sim.portrayal.DrawInfo2D)
	 */
    public final void draw(Object object, Graphics2D graphics, DrawInfo2D info)
    {
    	// info.draw.width & height - the current level of zoom on the object (same as shown in display
    	
    	// UNCOMMENT IF WANT TO SEE THE RLNONSTROMAL
    	
        /*double diamx = info.draw.width*simParams.LTO_DIAMETER;
        double diamy = info.draw.height*simParams.LTO_DIAMETER;
    	   
        graphics.setColor(PPatchSim.cellColours.get(cellState));
        
        // THE ORIGINAL COORDINATES ARE ADJUSTED FOR SCALE ON SCREEN, AND THE FACT THAT THE ELLIPSE IS ELSE DRAWN UPWARDS FROM THE
        // BOTTOM RIGHT
        Ellipse2D.Double cell = new Ellipse2D.Double((int)info.draw.x-diamx/2,(int)info.draw.y-diamy/2,(int)diamx,(int)diamy);
        
        graphics.fill(cell);*/

    }
}
