package ppatch_environment;

import ppatch_chemokine.ChemokineGrid;
import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import ppatch_onepatch_cells.LTo;
import sim.util.Double2D;

/**
 * Class which deals with the growth of the environment over time, adjusting the relevant cell coordinates as required
 * 
 * @author kieran
 *
 */
public class TractGrowth
{
	/**
	 * Object containing the parameters used in this simulation run
	 */
	SimParameters2 simParams;
	
	/**
	 * Constructor - stores the simulation parameters in this class for ease of access later
	 * @param sp
	 */
	public TractGrowth(SimParameters2 sp)
	{
		this.simParams = sp;
	}
	
	/**
	 * Resizes the tract at each step in order to correctly model the growth of the intestine over the 72 hour period
	 * 
	 * @param ppsim	The current simulation state
	 */
	public void adjustTractSize(PPatchSim ppsim)
	{
		// Store old measurements - needed later
		double oldGridLength = PPatchSim.currentGridLength;
		double oldGridHeight = PPatchSim.currentGridHeight;
		
		// Make the tract grow if not yet reached the upper size thresholds
		if(ppsim.currentGridHeight<simParams.upperGridHeight)
			ppsim.currentGridHeight = ppsim.currentGridHeight+ppsim.heightGrowth;
		if(PPatchSim.currentGridLength<simParams.upperGridLength)
			PPatchSim.currentGridLength = PPatchSim.currentGridLength+ppsim.lengthGrowth;
		
		// set the new display & tract sizes IF a GUI is being used
		if(simParams.displayFlag)
		{
			ppsim.display.insideDisplay.height = PPatchSim.currentGridHeight;
			ppsim.display.insideDisplay.width = PPatchSim.currentGridLength;
		
		}
		
		// Increase the size of the overlaying grid used to work out chemokine levels 
		ppsim.chemoGrid = new ChemokineGrid(ppsim.currentGridLength,ppsim.currentGridHeight);
		
		// Now reposition all stromal cells - firstly workout the midpoints of the grid (as it grows from middle)
		double midLength = ppsim.currentGridLength/2;
		double midHeight = ppsim.currentGridHeight/2;
	
		// calculate the distance from the midpoint and reposition accordingly
		for(int l=0;l<ppsim.ltoCellsBag.size();l++)
		{
			LTo ltoCell = (LTo)ppsim.ltoCellsBag.get(l);
		
			// all LTo cells are in a grid - which starts being the height & width of an LTo - to adjust the location as the tract grows,
			// work out how much the grid space has grown by, then move the cell in the grid by that amount
			double xAdj = PPatchSim.currentGridLength / ((int)(simParams.initialGridLength/simParams.LTO_DIAMETER));
			double yAdj = PPatchSim.currentGridHeight / ((int)(simParams.initialGridHeight/simParams.LTO_DIAMETER));
			
			// set the new location
			Double2D newloc = new Double2D((ltoCell.gridLoc.x*xAdj)+simParams.LTO_DIAMETER/2,(ltoCell.gridLoc.y*yAdj)+simParams.LTO_DIAMETER/2);
			
			ltoCell.agentLocation = newloc;
			ppsim.ltoCellsBag.set(l, ltoCell);
			ppsim.tract.setObjectLocation(ltoCell,ltoCell.agentLocation);
			
		}
	}
	
}
