package ppatch_environment;

import ppatch_onepatch.PPatchSim;
import ppatch_onepatch.SimParameters;
import ppatch_onepatch.SimParameters2;
import ppatch_onepatch_cells.LTo;
import ppatch_onepatch_cells.RLNonStromal;
import sim.field.grid.ObjectGrid2D;
import sim.util.Double2D;
import sim.util.Int2D;

/**
 * Class to create the stromal cell grid environment upon which interactions take place, and populate it with the 
 * required number of LTo and RLNS cells.  This number is calculated from the percentage read in from the XML file, 
 * and based on the size of the tract being modelled.
 * 
 * @author kieran
 *
 */
public class Setup_Stromal_Cell_Distribution 
{
	/**
	 * Initialises the grid environment, and calculates how many stromal cells are required
	 * 
	 * @param ppsim	The current simulation state
	 * @param simParams	The simulation parameters read in from the XML file
	 * 
	 */
	public Setup_Stromal_Cell_Distribution(PPatchSim ppsim,SimParameters2 simParams)
	{
		// Set up the grid
		ppsim.lToGrid = new ObjectGrid2D((int)(simParams.initialGridLength/simParams.LTO_DIAMETER),(int)(simParams.initialGridHeight/simParams.LTO_DIAMETER));
		
		// Now determine the experiment being done - are we looking at a normal distribution of LTo cells, or experimentally just one LTo
		
		if(simParams.ltoSetup.equals("centreLTo"))
		{
			// EXAMINING ONE IN THIS CASE
			this.insertExperimentalLTo(ppsim,simParams);
		}
		else
		{
			// Normal situation
		
			// Firstly, work out the number of immature stromal cells to create based on the density measure
			
			// Work out what 100% of the cells would be:
			double totalCells = ((((int)(simParams.initialGridLength/simParams.LTO_DIAMETER)*(int)(simParams.initialGridHeight/simParams.LTO_DIAMETER))));
			//System.out.println("Total Cells: "+totalCells);
		
			// Now work out what the required number of potentially active stromal cells will need to be placed on the stroma
			double numActiveLTo = (totalCells / 100)*simParams.stromalCellDensity;
		
			// add all the LTo cells to the tract, randomly placed
			// However, a certain percentage of these need to be artn active at the start of the simulation - so calculate this number
			double numARTNActive = (numActiveLTo * simParams.percentStromaRETLigands)/100;
		
			// Now work out the number of 'decoy' stromal cells that need to be placed on the tract (comes from decoy percentage parameter)
			double numRLNonStromal = (totalCells / 100)*simParams.percentRETLigandNonStromal;
	
			// ADD LTO CELLS
			this.distribute_LTo_Cells(ppsim, simParams, numActiveLTo);
		
			// SET A PERCENTAGE OF THE LTo CELLS TO EXPRESS RET LIGAND
			this.setCellsExpressingRETLigand(ppsim,numARTNActive,simParams);

			// ADD THE RET LIGAND NON STROMAL CELLS
			this.distribute_RLNonStromal_Cells(ppsim, simParams, numRLNonStromal);
		}
	}
	
	/**
	 * Experimental function to place one active LTo in the centre of the simulation
	 * 
	 * @param ppsim	The current simulation state
	 * @param simParams	The current simulation parameters
	 */
	public void insertExperimentalLTo(PPatchSim ppsim, SimParameters2 simParams)
	{
		int xLoc = (int)(350/simParams.LTO_DIAMETER);
		int yLoc = (int)(127/simParams.LTO_DIAMETER);
		
		Int2D gridLocation = new Int2D(xLoc,yLoc);
		// put the object at that location in the grid
		// needs adjusting as boxes are the diameter of the LTo cell - plus add half to correct drawing function
		// else draws on edge of screen
		Double2D location = new Double2D((xLoc*simParams.LTO_DIAMETER)+simParams.LTO_DIAMETER/2,(yLoc*simParams.LTO_DIAMETER)+simParams.LTO_DIAMETER/2);
		LTo ltoCell = new LTo(location,simParams,gridLocation,1);
		
		// add to the bag storing the LTo objects
		ppsim.ltoCellsBag.add(ltoCell);
		
		// set the grid to show the cell has been placed here
		ppsim.lToGrid.set(xLoc, yLoc, ltoCell);
		
		ppsim.tract.setObjectLocation(ltoCell,location);
		
		ltoCell.setStopper(ppsim.schedule.scheduleRepeating(ltoCell));
		
		// activate the RET ligand on this cell
		ltoCell.activateRETLigand();
		ppsim.activelToCellsBag.add(ltoCell);
	
	}
	
	/**
	 * Adds the required number of LTo cells at random locations in the environment
	 * 
	 * @param ppsim	The current simulation state
	 * @param simParams	The simulation parameters read in from the XML file
	 * @param numActiveLTo	The number of LTo cells that need placing in the environment
	 */
	public void distribute_LTo_Cells(PPatchSim ppsim, SimParameters2 simParams, double numActiveLTo)
	{	
		// ADD THE LTo CELLS
		// Each will be placed randomly in the grid
		// So as the length and width have been divided by 6, we can find random grid squares, and fill if not yet chosen
		// Also avoids collision problems
		// Pattern may be box like but will be adjusted as the tract grows
		
		for(int k=0;k<numActiveLTo;k++)   // if the percentage is a decimal, this will be rounded up
		{	
			boolean locationFree = false;
			
			while(!locationFree)
			{
				int xLoc = ppsim.random.nextInt((int)(simParams.initialGridLength/simParams.LTO_DIAMETER));
				int yLoc = ppsim.random.nextInt((int)(simParams.initialGridHeight/simParams.LTO_DIAMETER));
			
				if(ppsim.lToGrid.get(xLoc, yLoc)==null)
				{
					locationFree = true;
					Int2D gridLocation = new Int2D(xLoc,yLoc);
					// put the object at that location in the grid
					// needs adjusting as boxes are the diameter of the LTo cell - plus add half to correct drawing function
					// else draws on edge of screen
					Double2D location = new Double2D((xLoc*simParams.LTO_DIAMETER)+simParams.LTO_DIAMETER/2,(yLoc*simParams.LTO_DIAMETER)+simParams.LTO_DIAMETER/2);
					LTo ltoCell = new LTo(location,simParams,gridLocation,k);
					
					// add to the bag storing the LTo objects
					ppsim.ltoCellsBag.add(ltoCell);
					
					// set the grid to show the cell has been placed here
					ppsim.lToGrid.set(xLoc, yLoc, ltoCell);
					
					ppsim.tract.setObjectLocation(ltoCell,location);
					
					ltoCell.setStopper(ppsim.schedule.scheduleRepeating(ltoCell));
					
				}
			}
		}
	}
	
	/**
	 * Adds the required number of RLNS cells at random locations in the environment
	 * 
	 * @param ppsim	The current simulation state
	 * @param simParams	The simulation parameters read in from the XML file
	 * @param numDecoys	The number of RLNS cells that need placing in the environment
	 */
	public void distribute_RLNonStromal_Cells(PPatchSim ppsim, SimParameters2 simParams, double numDecoys)
	{		
		// Now put the decoy cells on the tract
		for(int k=0;k<numDecoys;k++)   // if the percentage is a decimal, this will be rounded up
		{	
			boolean locationFree = false;
		
			while(!locationFree)
			{
				int xLoc = ppsim.random.nextInt((int)(simParams.initialGridLength/simParams.LTO_DIAMETER));
				int yLoc = ppsim.random.nextInt((int)(simParams.initialGridHeight/simParams.LTO_DIAMETER));
			
				if(ppsim.lToGrid.get(xLoc, yLoc)==null)
				{
					locationFree = true;
					Int2D gridLocation = new Int2D(xLoc,yLoc);
					// put the object at that location in the grid
					// needs adjusting as boxes are the diameter of the LTo cell - plus add half to correct drawing function
					// else draws on edge of screen
					Double2D location = new Double2D((xLoc*simParams.LTO_DIAMETER)+simParams.LTO_DIAMETER/2,(yLoc*simParams.LTO_DIAMETER)+simParams.LTO_DIAMETER/2);
					RLNonStromal decoyCell = new RLNonStromal(location,simParams,gridLocation,k);
					
					// add the cell to a collection storing all the decoys
					ppsim.RETLigandNonStromalCellsBag.add(decoyCell);
					
					ppsim.lToGrid.set(xLoc, yLoc, decoyCell);
					
					ppsim.tract.setObjectLocation(decoyCell,location);
					
					decoyCell.setStopper(ppsim.schedule.scheduleRepeating(decoyCell));
				}
			}
		}
	}
	
	/**
	 * Sets a percentage of the LTo cells on the stromal to be expressing RET Ligand, and thus have the
	 * potential to become PP
	 * 
	 * @param numARTNActive	Number of LTo cells to set to be expressing RET Ligand
	 */
	public void setCellsExpressingRETLigand(PPatchSim ppsim,double numARTNActive,SimParameters2 simParams)
	{
		if(simParams.stromalCellRETLigandPlacement.equals("random"))
		{
			// A random selection of LTo cells are set to be expressing RET ligand.  In this case, they can be anywhere on the tract 
			for(int j=0;j<numARTNActive;j++)
			{
				// Pick a random LTo
				int randomLTo = ppsim.random.nextInt(ppsim.ltoCellsBag.size());
			
				// get the LTo from the bag
				LTo ltoCell = (LTo)ppsim.ltoCellsBag.get(randomLTo);
			
				// change the state of the LTo to make this express ARTN if not already expressing ARTN
				if(ltoCell.cellState!=1)
				{
					ltoCell.activateRETLigand();
					ppsim.activelToCellsBag.add(ltoCell);
				}
			}
		}
		else
		{
			// RET Ligand expression is restricted to LTo cells in a band.
			// If no LTo cells in a band, then no RET ligand expression.  Means a high enough percentage of the stromal cells must be set to express RET Ligand
			
			// Calculate the available circumference area
			Double rangeHeight = (simParams.initialGridHeight/100)*simParams.stromalCellCircumferencePercentage;
			
			// Now need to adjust so this range is centered around the centre of the simulation space.
			// Using the middle ensures no cells roll around the screen straight away when a range is used
			Double midpoint = simParams.initialGridHeight/2;
			
			for(int j=0;j<numARTNActive;j++)
			{
				int randomLTo = ppsim.random.nextInt(ppsim.ltoCellsBag.size());
				
				// get the LTo from the bag
				LTo ltoCell = (LTo)ppsim.ltoCellsBag.get(randomLTo);
				
				if(ltoCell.cellState!=1)
				{
					if((ltoCell.agentLocation.y >= midpoint-(rangeHeight/2)) && (ltoCell.agentLocation.y <= midpoint+(rangeHeight/2)))
					{
						ltoCell.activateRETLigand();
						ppsim.activelToCellsBag.add(ltoCell);
					}
				}
			}
			
		}
	}
}
